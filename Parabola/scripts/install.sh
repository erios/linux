#!/bin/bash
#Temporal is a file that contains parameters to use when access to chroot

# pacman -Sy parabola-keyring --noconfirm
# pacman-key --populate parabola
# pacman-key --refresh-keys

partition(){
	
	#Search and show the Hard Disks to select
	aux=$(ls /dev/sd?)
	index=0
	for i in $aux; do
		hdds[$index]="${i} ${i#/*/} off"
		index=$((index+1))	
	done

	hdd=$(dialog --stdout --radiolist "Select Hard Disk" 20 70 50 ${hdds[@]})
        
	#If exist the line delete
	if (cat ~/.scriptsInstallation/temporal | grep "hdd=") &>/dev/null
	then
 		sed -i -e '/hdd=*/d' ~/.scriptsInstallation/temporal
	fi

	#And add the new line with new parameter
	echo "hdd=$hdd" >> ~/.scriptsInstallation/temporal
	
	selection=$(dialog --stdout --menu "Partitioning method"  20 70 50 \
			1 "Use all disk with swap (1GB) and /" \
			2 "Use gparted to customize" )

	case $selection in
		1)
			umount /mnt &> /dev/null
			#Create msdos partition table
			parted -s $hdd -- mklabel msdos

			#Create partition swap and /
			parted -s $hdd -- mkpart primary 1MiB 1000MiB mkpart primary 1000MiB -1s
                        
                        #Boot option partition /
                        parted -s $hdd -- set 2 boot on

			#Format and partitions and mount /
			(echo t; echo 1; echo 82; echo w) | fdisk $hdd
			mkswap ${hdd}1
			mkfs.ext4 ${hdd}2
			mount ${hdd}2 /mnt
			;;
		2)	
			# gparted $hdd

			#Search and show the partitions that select before.
			aux=$(ls $hdd?)
			index=0;
			for i in $aux; do
				partitions[$index]="${i} ${i#/*/} off"
				index=$((index+1))
			done

			partition=$(dialog --stdout --radiolist "Mount / Partition" 20 70 50 ${partitions[@]})
	
			#Mount partition /
			umount /mnt &> /dev/null
			mount $partition /mnt

			#Ask if you want mount other partitions
			other=0
			while [ $other != 3 ]; do
				other=$(dialog --stdout --menu "Mount other partition?" 20 70 50 1 "/home" 2 "/boot" 3 "No")
	
				case $other in
				1)
					umount /mnt/home &> /dev/null
					mkdir /mnt/home &> /dev/null
					mount $(dialog --stdout --radiolist "Mount /home Partition" 20 70 50 ${partitions[@]}) /mnt/home
					;;
				2)
					umount /mnt/boot &> /dev/null	
					mkdir /mnt/boot &> /dev/null
					mount $(dialog --stdout --radiolist "Mount /boot Partition" 20 70 50 ${partitions[@]}) /mnt/boot
					;;
				*)	
					other=3
					;;
				esac
			done
			;;
		esac	
}


option=0
while [ option != 7 ]; do
	option=$(dialog --stdout --menu "Parabola Installation CLI"  20 70 50 \
			1 "Format and Mount Partitions" \
			2 "Install Base System" \
			3 "Install GRUB" \
			4 "System Configure" \
			5 "(Optional) Install Live DVD Desktop/Applications" \
			6 "Create User Account" \
			7 "Exit" )

	case $option in
		1)
			partition
			;;
		2)	
			#Install base system
			pacstrap /mnt
			pacstrap /mnt dialog
			;;
		3)
			#Install grub
			pacstrap /mnt grub grub2-theme-gnuaxiom
			#Enable Parabola theme for grub
			sed -i 's|[#]GRUB_THEME=["]/path/to/gfxtheme["]|GRUB_THEME="/boot/grub/themes/GNUAxiom/theme.txt"|' /mnt/etc/default/grub
			;;
		4)
			#Generate fstab and acces to chroot to do System Config
			# genfstab -p /mnt >> /mnt/etc/fstab
			cp ~/.scriptsInstallation/temporal /mnt
			cp ~/.scriptsInstallation/systemConfig.sh /mnt
			chmod +x /mnt/systemConfig.sh
			arch-chroot /mnt /systemConfig.sh
			rm -r /mnt/systemConfig.sh  
			;;
		5)
			#Packages to the X11 live
			packages=(	"xorg-server"
					"xf86-input-evdev"
					"xf86-input-synaptics"
					"xf86-video-vesa"
					"xf86-video-intel"
					"xf86-video-nouveau"
					"xf86-video-ati"
					"xf86-video-apm"
					"xf86-video-ark"
					"xf86-video-chips"
					"xf86-video-dummy"
					"xf86-video-fbdev"
					"xf86-video-glint"
					"xf86-video-i128"
					"xf86-video-i740"
					"xf86-video-intel"
					"xf86-video-mach64"
					"xf86-video-neomagic"
					"xf86-video-nv"
					"xf86-video-openchrome"
					"xf86-video-r128"
					"xf86-video-rendition"
					"xf86-video-s3"
					"xf86-video-s3virge"
					"xf86-video-savage"
					"xf86-video-siliconmotion"
					"xf86-video-sis"
					"xf86-video-sisusb"
					"xf86-video-tdfx"
					"xf86-video-trident"
					"xf86-video-tseng"
					"xf86-video-vmware"
					"xf86-video-voodoo"
					"xorg-xinit"
					"sudo"
					"gst-plugins-good"
					"gst-libav"
					"mate"
					"mate-extra"
					"caja"
					"volumeicon"
					"zenity"
					"octopi"
					"octopi-cachecleaner"
					"octopi-notifier"
					"octopi-repoeditor"
					"ambiance-radiance-colors-suite"
					"rave-x-colors"
					"pulseaudio-alsa"
					"alsa-utils"
					"networkmanager"
					"network-manager-applet"
					"ath9k-htc-firmware"
					"gnome-keyring"
					"iceweasel"
					"icedove"
					"pidgin"
					"linphone"
					"qtox"
					"gparted"
					"smplayer"
					"epdfview"
					"gpicview"
					"abiword"
					"gnumeric"
					"leafpad"
					"galculator-gtk2"
					"xarchiver"
					"dmidecode"
					"lightdm-gtk-greeter-settings"
					"grub2-theme-gnuaxiom"
					"p7zip"
					"unar"
					"unzip"
					"zip"
					"ttf-dejavu"
					)
			
			#Install packages			
			pacstrap /mnt ${packages[@]}
			user=$(cat ~/.scriptsInstallation/temporal | grep "userName" )
			
			#Copy skel in the new system and desktop's background
			cp -a /etc/skel/ /mnt/etc/
			cp -a /etc/wallpaper.png /mnt/etc/wallpaper.png

			#Uncomment to allow members of group wheel to execute any command
			sed -i 's|[#] [%]wheel ALL=[(]ALL[)] ALL|%wheel ALL=(ALL) ALL|' /mnt/etc/sudoers

			#Enable services
			cp -a ~/.scriptsInstallation/x11.sh /mnt

			#Enable services with systemd
			chmod +x /mnt/x11.sh
			arch-chroot /mnt /x11.sh 
			rm /mnt/x11.sh
			;;
		6)	
			#Create a new username and save in temporal, to use after
			if (cat ~/.scriptsInstallation/temporal | grep "userName=") &>/dev/null
			then	
				sed -i -e '/userName=*/d' ~/.scriptsInstallation/temporal
			fi

			echo "userName=$(dialog --stdout --inputbox "Enter an User Name" 8 40)" >> ~/.scriptsInstallation/temporal
			cp ~/.codecheck /mnt
			cp ~/.scriptsInstallation/temporal /mnt
			cp ~/.scriptsInstallation/userAccount.sh /mnt
			chmod +x /mnt/userAccount.sh
			arch-chroot /mnt /userAccount.sh
			rm -r /mnt/userAccount.sh 
			;;
		*)
			#Delete temporal file and umount partitions
			rm -r /mnt/.codecheck
			rm -r /mnt/temporal
			umount /mnt/boot &> /dev/null
			umount /mnt/home &> /dev/null
			umount /mnt &> /dev/null		
			exit
			;;
	esac
done
