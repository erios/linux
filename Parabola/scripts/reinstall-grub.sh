#!/bin/bash
hdd=/dev/sdb$1
    mount $hdd /mnt
#Generate fstab and acces to chroot to do System Config
#			genfstab -p /mnt >> /mnt/etc/fstab
sed "s-hdd=.*-hdd=$hdd-g" grubConfig.sh > /mnt/grubConfig.sh
chmod +x /mnt/grubConfig.sh
arch-chroot /mnt /grubConfig.sh
rm -r /mnt/grubConfig.sh
#Delete temporal file and umount partitions
umount /mnt/boot &> /dev/null
umount /mnt/home &> /dev/null
umount /mnt &> /dev/null		
exit
