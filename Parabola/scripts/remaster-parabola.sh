#!/usr/bin/env bash
# https://wiki.parabola.nu/Remastering_the_Install_ISO
# https://git.parabola.nu/packages/parabolaiso.git/tree/parabolaiso
# https://wiki.archlinux.org/index.php/Remastering_the_Install_ISO
# https://wiki.archlinux.org/index.php/Archiso#Documentation_and_tutorials
# https://wiki.archlinux.org/index.php/Change_root
# https://git.archlinux.org/arch-install-scripts.git/tree/

set -x
setpaths (){
    isopath="/home/edgar/Downloads/parabola-mate-2016.11.03-dual.iso/parabola-mate-2016.11.03-dual.iso"
    rootpath="/usr/local/play/customiso/parabola"
    mntisopath="$rootpath/parabolaiso"
    workpath="$rootpath/customiso"
    mntpath="$rootpath/mnt"
    editpath="$rootpath/edit"

    distro="parabola"
    arch="x86_64"
}

unsquash() {
    setpaths
    mkdir "$mntisopath"
    mount -t iso9660 -o loop "$isopath" "$mntisopath"
    rsync -a "$mntisopath"/ "$workpath"/
    cd "$workpath"/"$distro"/"$arch"
    unsquashfs root-image.fs.sfs
}

mount_img () {
    setpaths
    [[ -d "$mntpath" ]] || mkdir "$mntpath"
    mount "$workpath"/"$distro"/"$arch"/squashfs-root/root-image.fs "$mntpath"
}

copy_img (){
    setpaths
    unsquash
    mount_img
    rsync -a "$mntpath"/ "$editpath"/
}
# copy_img

# mntpath="/mnt/"
# umount /mnt
# mount /dev/sdb2 "$mntpath"
mount_sys() {
    setpaths
    mount -t proc "$editpath"/proc "$editpath"/proc/
    mount -t sysfs "$editpath"/sys "$editpath"/sys/
    mount -o bind /dev "$editpath"/dev/
    mount -t devpts "$editpath"/dev/pts/ "$editpath"/dev/pts/
    mount -t tmpfs "$editpath"/dev/shm "$editpath"/dev/shm
    [[ -d "$editpath"/run/shm ]] || mkdir "$editpath"/run/shm
    mount -t tmpfs -o bind /run/shm/ "$editpath"/run/shm
}
# mount_sys

unmount_sys(){
    for i in proc sys dev/pts run/shm dev/shm dev run var/cache/pacman/pkg; do umount "$editpath"/"$i"; done && rm -fr "$editpath/run/shm/"
}
unmount_sys

chroot_netcfg(){
    setpaths
    [[ -d "$rootpath"/backup ]] || mkdir "$rootpath"/backup
    [[ -f "$rootpath"/backup/resolv.conf ]] ||
        rsync -a "$editpath"/etc/resolv.conf "$rootpath"/backup
    cp -L /etc/resolv.conf "$editpath"/etc/resolv.conf
}
# chroot_netcfg
# chroot "$editpath" /bin/bash
exit

# Wihin chroot

mv /etc/pacman.d/gnupg/ /tmp    # https://forum.antergos.com/topic/1389/how-to-pacman-keyring-errors/9
pacman-key --init
pacman -Syy
pacman -S parabola-keyring
# https://www.parabola.nu
pacman-key --refresh-keys
pacman -Syuw
rm /etc/ssl/certs/ca-certificates.crt # Beware (outdated?)
pacman -Su

# Install packages
dir="$PWD"
cd /tmp
git clone https://aur.archlinux.org/scalapack.git
cd scalapack
su parabola
makepkg -s
exit
sudo pacman -U scalapa*.pkg.tar.xz

cd /tmp
git clone https://aur.archlinux.org/mumps.git
cd mumps
su parabola
makepkg -s
exit
sudo pacman -U mumps*.pkg.tar.xz

# From AUR
# --asdeps untex
# mupdf-nojs #customize
# netgen-git PKGBUILD
# engauge
# textext-hg

packages_mateISO=(
    "alsa-utils"
    "xf86-video-vesa"
    "xf86-video-ati"
    "xf86-video-intel"
    "xf86-video-nouveau"
    "xf86-input-synaptics"
    "ambiance-radiance-colors-suite"
    "ath9k-htc-firmware"
    "caja"
    "dmidecode"
    "galculator-gtk2"
    "gnome-keyring"
    "gparted"
    "gpicview"
    "grub2-theme-gnuaxiom"
    "gst-libav"
    "gst-plugins-good"
    "icedove"
    "lightdm-gtk-greeter-settings"
    "mate"
    "mate-extra"
    "octopi"
    "octopi-cachecleaner"
    "octopi-notifier"
    "octopi-repoeditor"
    "p7zip"
    "pidgin"
    "pulseaudio-alsa"
    "rave-x-colors"
    "smplayer"
    "sudo"
    "ttf-dejavu"
    "unar"
    "unzip"
    "volumeicon"
    "xarchiver"
    "xorg-server"
    "xorg-xinit"
    "zenity"
    "zip"
)
# optional dependencies
packages_opt=(
    "libxslt" # vvv recoll vvv
    "unzip"
    "poppler"
    "pstotext"
    "antiword"
    "catdoc"
    "unrtf"
    "djvulibre"
    "id3lib"
    "python2"
    "mutagen"
    "python2-pychm"
    "perl-image-exiftool"
    "python-lxml"
    "aspell-en" # ^^^ recoll ^^^
    "gd"
    "python"
    "python-setuptools"
    "python-pip"
    "mpdecimal"
    "tk"
    "texlive-core" # latex
    "pstoedit" # vvv inkscape vvv
    "python2-scour"
    "python2-numpy"
    "python2-lxml"
    "uniconvertor"
    "pdf2svg" # ^^^ inkscape ^^^
    "python-gobject" # vvv matplotlib vvv
    "python-cairocffi"
    "python-pyqt4"
    "python-tornado"
    "python-pillow"
    "python-opengl"
    "python-nose" # ^^^ matplotlib ^^^
    "opencascade" # vvv gmsh, mupdf and friends vvv
    "gnuplot"
    "openmpi"
    "lapack"
    "swig"
    "desktop-file-utils"
    "cmake"
    "metis"
    "scotch"
    "openmp"
    "gcc-fortran"
    "glu"
    "libxmu"
    "tk"
    "openjpeg2"
    "libjpeg-turbo"
    "boost"
    "hdf5-openmpi"
    "fftw" # ^^^ gmsh, mupdf and friends ^^^
    "glfw-x11" # vvv mupdf-nojs vvv
    "harfbuzz"
    "freetype2"
    "jbig2dec"
    "libjpeg-turbo"
    "openjpeg2" # ^^^ mupdf-nojs ^^^

)
# extra
packages_extra=(
    ${packages_mateISO}
    "inkscape" # drawing
    "gimp" # drawing
    "python-numpy" # science
    "python-scipy" # science
    "python-matplotlib" # science
    "gst-plugins-bad" # codecs
    "gst-plugins-ugly" # codecs
    "emacs" # swiss-knife
    "texlive-core" # documents
    "libreoffice-fresh" # documents
    "recoll" # documents
    "maxima" # science
    "base-devel" # development
    "git" # development
    "ufw" # newtwork
    "ffmpeg" # multimedia
    "mplayer" # multimedia
    "texlive-latexextra" # latex
    "texlive-science" # latex
    "texlive-humanities" # latex
    "biber" # latex
    "texlive-formatsextra" # latex
    "texlive-pstricks" # latex
    "python-rope" # emacs-python
    "python-jedi" # emacs-python
    "flake8" # emacs-python
    "autopep8" # emacs-python
    "yapf" # emacs-python
    "poppler" # emacs-pdf-tools
    "libpng" # emacs-pdf-tools
    "zlib" # emacs-pdf-tools
    "poppler" # emacs-pdf-tools
    "poppler-glib" # emacs-pdf-tools
    "imagemagick" # emacs-pdf-tools
    "testdisk" # util
    "rsync" # util
    "icecat" # internet (iceweasel no-add-ons)
    "fortune-mod" # games
)
pacman -S --needed --asdeps ${packages_opt[@]}
pacman -S ${packages_mateISO[@]} ${packages_extra[@]}
pacman -Rs iceweasel iceweasel-l10n-es-es-1:54.0-1  iceweasel-l10n-gl-1:54.0-1  iceweasel-l10n-pt-br-1:54.0-1

ufw enable
systemctl enable wicd
pacman -S grub grub2-theme-gnuaxiom
sed -i 's|[#]GRUB_THEME=["]/path/to/gfxtheme["]|GRUB_THEME="/boot/grub/themes/GNUAxiom/theme.txt"|' /mnt/etc/default/grub

wget https://raw.githubusercontent.com/snmishra/emacs-netlist-modes/master/spice-mode/spice-mode.el
wget https://tiagoweber.github.io/blog/attachments/ob-spice.el

/etc/hostname
/usr/share/zoneinfo/
/etc/vconsole.conf #
printf "LANG=en_US.UTF-8\n" > /etc/locale.conf
printf "en_US.UTF-8 UTF-8\n" >> /etc/locale.gen
cd /usr/share/kbd/keymaps/i386/qwerty # 29 = Caps_Lock 58 = Control
cp us-acentos{*,-ctrlswapcaps}.map.gz
gunzip us-acentos-ctrlswapcaps.map.gz
# https://wiki.parabola.nu/KEYMAP
sed 's-29[[:space:]]=[[:space:]]Control-29 = Caps_Lock-g'
sed 's-58[[:space:]]=[[:space:]]Caps_Lock-29 = Control-g'


[[ -d /mnt/home/live/.emacs.d/ ]] || mkdir /mnt/home/live/.emacs.d
mv spice-mode.el ob-spice.el /mnt/home/live/.emacs.d

# https://www.garron.me/en/linux/install-aur-abs-arch-linux.html
# https://wiki.archlinux.org/index.php/Arch_User_Repository#Installing_packages
# https://raspberrypi.stackexchange.com/a/511
su parabola
mkdir /tmp/build
cd /tmp/build
wget https://aur.archlinux.org/cgit/aur.git/snapshot/python-importmagic.tar.gz
tar xzf python-importmagic.tar.gz
cd python-importmagic
makepkg -s
cp python-importmagic*.pkg.tar.xz ..
cd ..
wget https://aur.archlinux.org/cgit/aur.git/snapshot/python-sexpdata.tar.gz
tar xzf python-sexpdata.tar.gz
cd python-sexpdata
makepkg -s
sudo pacman -U python-sexpdata*.pkg.tar.xz
wget https://aur.archlinux.org/cgit/aur.git/snapshot/python-epc.tar.gz
tar xzf python-epc.tar.gz
cd python-epc
makepkg -s
sudo pacman -U python-epc*.pkg.tar.xz

exit


pacman -Syu
pacman-key --populate archlinux parabola

# Make changes

# Update
pacman -Syu --force parabolaiso linux-libre


# mntpath=/mnt
for i in proc sys dev/pts run/shm dev/shm dev; do umount "$mntpath"/"$i"; done
sudo rm -fr "$mntpath"/var/db/sudo/lectured/*

https://askubuntu.com/questions/168957/is-there-a-way-to-install-ubuntu-on-usb-pendrive-as-normal-instalation-not-live
https://askubuntu.com/questions/16988/how-do-i-install-ubuntu-to-a-usb-key-without-using-startup-disk-creator
https://wiki.archlinux.org/index.php/Installing_Arch_Linux_on_a_USB_key

# Fortunes
# http://forums.blagblagblag.org/viewtopic.php?f=2&t=5619&p=30609&hilit=fortune#p30609
# torsocks git clone https://notabug.org/PangolinTurtle/BLAG-fortune.git
# # Then add parabola into distro.sh
# elif [ $distro = "parabola" ]; then
#    echo "/usr/share/fortune/"
# http://bencane.com/2013/09/16/understanding-a-little-more-about-etcprofile-and-etcbashrc/


# RAID
# https://www.centos.org/docs/5/html/5.1/Installation_Guide/s2-s390info-raid.html
# https://linuxconfig.org/linux-software-raid-1-setup

# sudo qemu-system-x86_64 -enable-kvm -hda /dev/sdb -hdb /dev/sdd -m 6G -boot menu=on
# sudo qemu-system-x86_64 -enable-kvm -hda /dev/sdb -hdb /dev/sdd -cdrom ~/Downloads/parabola-mate-2016.11.03-dual.iso/parabola-mate-2016.11.03-dual.iso -boot d -m 6G
