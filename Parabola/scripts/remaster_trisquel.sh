#! /bin/sh

#
##
## Script to remaster a Trisquel live CD
##
#
#
# Overview:
# =========
#
# This script allows to customize an Ubuntu live CD in 3 simple steps:
#
#  1. extract the contents of an Trisquel CD ISO image to your hard drive
#
#  2. chroot to the working area containing the extracted CD
#     contents. This allows to do all the /etc configs, aptitude
#     purge/install, etc. to prepare the future ISO image
#
#  3. generate a new ISO image containing the (hopefully modified)
#     working area you just prepared
#
#
# Tested configurations:
# ======================
#
# - Tested with toutatis/x86_64 (6.0) live CD image on an
#   toutatis/x86_64 (6.0) host
#
#
# Prerequisites:
# ==============
#
#  - around 3GB available on the hard drive (plus what you may add)
#  - aptitude install squashfs-tools genisoimage
#  - to install on a bootable USB stick:
#     aptitude install usb-creator-gtk
#
#
# Usage:
# ======
#
#  All the following commands MUST be run as root (sudo).
#
#  - this_script.sh extract /path/to/original/trisquel.iso /path/to/working/area
#    => This will extract the contents of the ISO into 2 subdirectories of
#       /path/to/working/area/ (should be empty initially):
#         extract-cd/: the files needed to boot (isolinux config,
#                      kernel, initramfs, etc.), but not the root FS
#         edit/:       the root file system (complete with /etc, /usr, etc.)
#
#  - this_script.sh chroot /path/to/working/area
#    => This will start a root bash shell inside the root file
#       system. Takes care of mounting /dev, /proc, /sys. In that
#       shell, you can aptitude purge/install, edit /etc files, create
#       users, even start X applications (xauth is configured),
#       etc. But do NOT edit anything in /dev, as this will affect
#       your host (the host's /dev is mounted inside the chroot:
#       anything changed in the chroot's /dev is also changed in the
#       host's /dev !): avoid doing rm in /dev at this point :) !
#       Beware also that issuing some aptitude install commands might
#       start /etc/init.d daemons which will run inside the chroot,
#       even after you exited from it: you might have to kill these
#       manually so that they don't interfere with the host
#       Note: This operation will also copy the host's apt config into
#       the working area, so that the same repositories can be
#       used. The original Trisquel apt config will be restored when the
#       regen command is issued
#
#  - this_script.sh regen /path/to/working/area SOME_NAME /path/to/dest/my.iso
#    => Generate a new ISO file from the extract-cd/ and edit/ dirs of
#       the working area. The SOME_NAME param is just a dummy tag for
#       the ISO image. The original apt configuration will be restored
#       before the image is generated. You will still be able to use
#       the chroot operation after a regen
#
#  - this_script.sh release /path/to/working/area
#    => You will probably never need to use this operation
#       manually. It is essentially an internal command used by the
#       regen operation. But it might be important to use manually if
#       you plan to remove /path/to/working/area (eg. rm -rf) after
#       you chroot'ed to it at least once. This command makes sure the
#       host will not be impacted by the removal of the working area
#       (eg. it will unmount the /dev mount in the chroot). After this
#       command, it is safe to remove completely the working area. You
#       will still be able to use the chroot operation after a release
#
# The 'extract' and 'regen' operations will be verbose and stop at the
# first error that occurs. You can of course run the 'extract'
# operation just once, and run the 'chroot' or 'regen' operations
# thousands of times on the same working area. You can also run
# several 'chroot' sessions in parallel. But be sure NOT to run a
# 'chroot' operation while a 'regen' is in progress.
#
#
# Advanced customization:
# =======================
#
# If you're planning to update the kernel, or to change the live user
# configuration, follow the instructions in [1] (see references
# below).
#
#
# To test:
# ========
#
# Try qemu on the resulting iso file:
#  qemu-system-x86_64 -cdrom tmp/trisquel-custom.iso -boot d -m 512 -net none
#  qemu-system-i386 -cdrom tmp/trisquel-custom.iso -boot d -m 512 -net none
#
# Or burn a ISO and reboot on the CD you just created. Or use
# usb-creator-gtk to create a # bootable USB memory stick.
#
#
# References:
# ===========
#
#  [1] https://help.ubuntu.com/community/LiveCDCustomization
#  [2] http://brainstorms.in/?p=356
#  [3] http://devel.trisquel.info/makeiso/makeiso.sh
#
#
# Credits:
# ========
#   This script was modified from its original version by Amir Parsanei
#
#   https://trisquel.info/en/wiki/customizing-trisquel-iso
#
#   It can be found in its original form at:
#   d2 (david /at/ decotigny point fr): http://david.decotigny.fr
#   http://david.decotigny.fr/wiki/wakka.php?wiki=RemasterUbuntu
#
# Licensing:
# ==========
#
# GPL v3.0 or above (Amir Parsanei) 2013
#
# Copyright (c) 2008,2009,2010, David Decotigny (http://david.decotigny.fr)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * The name of its contributors may not be used to endorse or
#      promote products derived from this software without specific
#      prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
# <COPYRIGHT HOLDER> BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
# USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
# OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#

SVN_REVISION='$Id: remaster-trisquel.sh 1485 2013-09-01 18:41:48Z anonymous $'

_usage () {
    [ -n "$1" ] && /bin/echo -e "\n\033[31;1m$1\033[m\n" >&2
    cat >&2 <<EOF
Usage: $0 command options...

Commands & options:
   extract /path/to/trisquel.iso /path/to/working_area : create the working area
   chroot  /path/to/working_area                     : start a shell in the WA
   release /path/to/working_area                     : umount special WA mounts
   regen   /path/to/working_area IMGNAME /path/to/new.iso: create new iso image
EOF
    exit 1
}


# Check that we're the root user
[ x`id -u`-`id -g`y = x0-0y ] || _usage "This script requires root privileges"


_finalize_extract () {
    umount "$d/squashfs" >/dev/null 2>&1 && rmdir "$d/squashfs"
    umount "$d/mnt"      >/dev/null 2>&1 && rmdir "$d/mnt"

    trap "" KILL EXIT QUIT TERM INT
}


#disable_scripts () {
#CHROOT="$1"
## Disable the service management scripts (makeiso.sh)
    #SCRIPTS="sbin/start-stop-daemon usr/sbin/invoke-rc.d usr/sbin/service"
    #for i in $SCRIPTS
    #do
        #mv "$CHROOT/$i" "$CHROOT/$i.REAL"
        #cat <<EOF > "$CHROOT/$i"
##!/bin/sh
#echo
#echo "Warning: Fake /$i called, doing nothing"
#EOF
        #chmod 755 "$CHROOT/$i"
    #done
#
    #cat <<EOF > "$CHROOT/usr/sbin/start"
##!/bin/sh
#echo
#echo "Warning: Fake start called, doing nothing"
#EOF
#
    #cat <<EOF > "$CHROOT/usr/sbin/stop"
##!/bin/sh
#echo
#echo "Warning: Fake start called, doing nothing"
#EOF
    #chmod 755 $CHROOT/usr/sbin/start $CHROOT/usr/sbin/stop
#}

backup_file () {
    ii=$(echo "$1" | sed 's-/-__-g')
    [ -e "$d/edit/$1" ] && cp -pL "$d/edit/$1" "$d/orig/$ii" > \
      /dev/null 2>&1 || true
}

restore_file () {
    ii=$(echo "$1" | sed 's-/-__-g')
    [ -e "$d/orig/$ii" ] && cp -p "$d/orig/$ii" "$d/edit/$1" > \
      /dev/null 2>&1 || true
}

bogus_script () {
# Replace the contents of a file with a warning message
    cat <<EOF > "$d/edit/$1"
#!/bin/sh
echo
echo "Warning: Fake /$1 called, doing nothing"
EOF
    # Make the file executable
    chmod 755 "$d/edit/$1"
}

extract_img () {
    [ $# != 2 ] && _usage "Invalid number of arguments for extract command"

    # Set name of original ISO file
    imgfile="$1"
    d="$2" # This may not have a problem in sh
    ## Establish working directory (remove last slash)
    #d="$(echo "$2" | sed 's-/$--g')"

    # Make sure that options are correct
    [ -f "$imgfile" ] || _usage "Invalid ISO file $imgfile"
    [ -d "$d/edit" ] && _usage "Destination working area $d already exists"

    [ ! -d "$d" ] && mkdir "$d"

    echo "# Preparing working area $d for $imgfile"

    # Execute _finilize_extract when done extracting
    # trap: Execute a command if a signal is received (catch a signal)
    trap "_finalize_extract" KILL EXIT QUIT TERM INT
    # Print commands and their arguments as they are executed
    set -x

    # Make directory to mount ISO
    mkdir "$d/mnt"
    mount -o loop "$imgfile" "$d/mnt"

    # Make directory to extract ISO contents (without the squashfs)
    mkdir "$d/extract-cd"
    rsync --exclude=/casper/filesystem.squashfs -a \
    "$d/mnt/" "$d/extract-cd"

    # Make directory to mount the original squashfs
    mkdir "$d/squashfs"
    mount -t squashfs -o loop "$d/mnt/casper/filesystem.squashfs" \
                                    "$d/squashfs"

    # Prepare directory to play around
    mkdir "$d/edit"
    rsync -a "$d/squashfs/" "$d/edit"

    # Make directory to backup important stuff
    mkdir "$d/orig"
    # Backup and disable scripts (makeiso.sh)
    files="sbin/start-stop-daemon usr/sbin/invoke-rc.d usr/sbin/service"
    for i in $files
    do
        backup_file "$i"
        # Change the contents of the script and make it executable
        bogus_script "$i"
    done
    # Disable init start and stop scripts (they don't exist and will
    # be removed after the release) (makeiso.sh)
    bogus_script "usr/sbin/start"
    bogus_script "usr/sbin/stop"
    # Backup important files (etc/mtab doesn't exist in Trisquel LiveCD)
    files="etc/hosts etc/resolv.conf etc/mtab"
    for i in $files;
    do
        backup_file "$i"
    done
    tar cf "$d/orig/etc-apt-conf.tar" -C "$d/edit/etc" apt
}


chroot_live () {
    [ $# != 1 ] && _usage "Invalid number of arguments for chroot command"

    d="$1" # This may not have a problem in sh
    ## Establish working directory (remove last slash)
    #d="$(echo "$2" | sed 's-/$--g')"
    [ -d "$d/edit" ] || _usage "Invalid working area $d"

    # Print commands and their arguments as they are executed
    set -x

    # Copy Internet configuration to edit directory
    cp /etc/resolv.conf /etc/hosts "$d/edit/etc/"
    # Copy apt configuration to edit directory
    cp -fa /etc/apt "$d/edit/etc/"

    ## ---- Optional ----
    ## Do you want to copy your deb files to the chroot?
    #rsync /var/cache/apt/archives/*.deb "$d/edit/var/cache/apt/archives"
    ## Do you want to make a list of installed packages?
    # aptitude search --disable-columns -F '%p=%V %M' '~i' > \
    #  installed.list
    # aptitude search --disable-columns -F '%p=%V %M' '~M' > \
    #  automatically-installed.list
    # aptitude search --disable-columns -F '%p=%V %M' '~i !~M' > \
    #  manually-installed.list
    # aptitude search --disable-columns -F '%p=%V %c' '!~i !~v' > \
    #  purged.list

    # Prepare directory to backup keys for apt
    mkdir -p "$d/edit/tmp/remaster-xchg"

    # Make font bold (for announcement)
    /bin/echo -e "\033[1m"
    cat >&2 <<EOF
IMPORTANT: do not ever try to remove the $d (eg. rm -r) area
           unless you ran either the 'release' or 'regen' command beforehand !
EOF
    # Put font back to normal (actually back to white)
    /bin/echo -e "\033[m"

    # Mount dev (and dev/pts)
    [ -d "$d/edit/dev/usb" ] || mount --rbind /dev/ "$d/edit/dev"
#    [ -f /proc/cpuinfo ]     || mount -t proc none "$d/edit/proc"
#    [ -d /sys/class ]        || mount -t sysfs none "$d/edit/sys"
#    LANG="en_US.UTF-8"
#    LANGUAGE="en_US.UTF-8"
#    locale-gen en_US.UTF-8
#    export LANG LANGUAGE
#    chroot "$d"/edit env LANG="en_US.UTF-8" DISPLAY="$DISPLAY" \
#      LANGUAGE="en_US.UTF-8"

    env_key=`uuidgen`

    main_sh="/tmp/remaster-xchg/main-$env_key.sh"
    cp "$0" "$d/edit/$main_sh" # This doesn't work, right?  Yes, it does
    #cp $(which "$0") "$d/edit/$main_sh" # This may not have a problem
    #cp $(which "$0") "$d/edit$main_sh" # Does this work?
    cat > "$d/edit/tmp/remaster-xchg/startenv-$env_key.sh" <<EOF
echo `xauth nlist $DISPLAY` | xauth nmerge -
EOF
#    # Configure locale (I don't know if this is the solution. See below)
#    #wiki.blender.org/index.php/Dev:2.4/Doc/Building_Blender/Linux/Chroot
#    chroot "$d/edit" aptitude update
#    chroot "$d/edit" aptitude install locales
#    chroot "$d/edit" aptitude reinstall locales
#    chroot "$d/edit" aptitude upgrade locales
#    grep en_US "$d/edit"/usr/share/i18n/SUPPORTED | tee -a "$d/edit/etc/locale.gen"
#    chroot "$d/edit" locale-gen


    LANG="en_US.UTF-8" LANGUAGE="en_US.UTF-8" LC_ALL="en_US.UTF-8" exec chroot "$d"/edit env ENV_KEY="$env_key" DISPLAY="$DISPLAY" \
    /bin/sh "$main_sh" _actual_chroot_
    mount -t proc /proc "$d/edit/proc"
    mount -t sysfs /sys "$d/edit/proc"
}


_actual_chroot_ () {
    [ -f /proc/cpuinfo ] || mount -t proc none /proc
    [ -d /sys/class ]    || mount -t sysfs none /sys

    HOME=/root
    LC_ALL=C
    export HOME LC_ALL
    unset XAUTHORITY

    # Configure locale (if this doesn't work, see above, and try in a normal chroot)
    #www.backdrift.org/fixing-no-such-file-or-directory-locale-errors
    locale-gen en_IE.UTF-8
    dpkg-reconfigure locales
    LC_ALL="en_IE.UTF-8"
    LANG="en_IE.UTF-8"
    LANGUAGE="en_IE.UTF-8"
    export LC_ALL LANG LANGUAGE

    . /tmp/remaster-xchg/startenv-$ENV_KEY.sh
    rm -f /tmp/remaster-xchg/startenv-$ENV_KEY.sh
    rm -f "$0"

    cd /root

    exec env debian_chroot="remaster" /bin/bash -l
}


reenable_scripts () {
    # We can enable the init scripts now
    CHROOT="$1";
    SCRIPTS="sbin/start-stop-daemon usr/sbin/invoke-rc.d usr/sbin/service"
    for i in $SCRIPTS
    do
        restore_file "$i"
    done
    # There was no backup for start and stop, just bogus files
    [ -e $CHROOT/usr/sbin/start ] && rm -f $CHROOT/usr/sbin/start > \
      /dev/null 2>&1 || true
    [ -e $CHROOT/usr/sbin/stop ] && rm -f $CHROOT/usr/sbin/stop > \
      /dev/null 2>&1 || true
}


release_live () {
    [ $# != 1 ] && _usage "Invalid number of arguments for release command"

    d="$1"
    [ -d "$d/edit" ] || _usage "Invalid working area $d"

    while [ -d "$d/edit/dev/usb" ] ; do umount -fl "$d/edit/dev" ; done
    while [ -d "$d/edit/sys/class" ] ; do umount -fl "$d/edit/sys" ; done
    while [ -f "$d/edit/proc/cpuinfo" ] ; do umount -fl "$d/edit/proc" ; done
}


regenerate_img () {
    [ $# != 3 ] && _usage "Invalid number of arguments for regen command"

    d="$1"
    IMAGE_NAME="$2"

    # Determine absolute path to output ISO file (we'll do a chdir later)
    od=`dirname "$3"`
    od=`cd "$od" && /bin/pwd`
    outfile="$od"/`basename "$3"`

    [ -d "$d/edit" ] || _usage "Invalid working area $d"
    [ -f "$outfile" ] && _usage "Destination iso file $outfile already exists"


    ## Update hardware information from Internet database
    #chroot "$d/edit" torsocks update-pciids && \
     #chroot "$d/edit" torsocks update-usbids || \
     #echo "Warning: Could not update-pciids (unimportant)"
    chroot "$d/edit" dpkg-divert --rename --remove /sbin/initctl || true

    echo "# Creating image $IMAGE_NAME as $outfile from $d"
    set -x

    # Re-enable services within the chroot
    reenable_scripts "$d/edit"

    ## Detach the chroot from the system
    release_live "$d"

    # Update gconf and kernel
    chroot "$d/edit" update-gconf-defaults
    chroot "$d/edit" update-initramfs -u

    # Cleaning
    chroot "$d/edit" apt-get clean
    chroot "$d/edit" apt-get autoclean
    #chroot "$d/edit" apt-get update
    [ -f  "$d/edit"/usr/lib/locale/locale-archive ] && rm -v "$d/edit"/usr/lib/locale/locale-archive

    find "$d/edit" -regextype sed -regex '$d/edit/[.-]old$\|[.-]bak$'\
      | xargs -r rm -v

    for dir in "$d/edit"/var/lib/update-notifier/user.d/ "$d/edit"/var/lib/apt-xapian-index/
    do
        [ -d $dir ] || continue
        find $dir -type f |xargs -r rm
    done

    rm -f "$d/edit/etc/hosts" "$d/edit/etc/resolv.conf" "$d/edit/etc/mtab" \
    "$d/edit/root/.bash_history" "$d/edit/root/.Xauthority" \
    "$d/edit/etc/resolvconf/resolv.conf.d/original" \
    "$d/edit/etc/resolvconf/resolv.conf.d/tail"
    rm -rf "$d/edit/tmp/*"
    # Restore important files (mtab seems not to exist in Trisquel LiveCD)
    files="etc/hosts etc/resolv.conf etc/mtab"
    for i in $files;
    do
        restore_file "$i"
    done
    ### ---- Optional ----
    ### Do you want to put your sources directory back as it was?
    ## rm -rf "$d/edit/etc/apt"
    ##tar xf "$d/orig/etc-apt-conf.tar" -C "$d/edit/etc/"

    # Update links to kernel ISO
    KERNEL="$(chroot "$d/edit" echo `uname -r`)"
    cp "$d/edit/boot/vmlinuz-$KERNEL" \
     "$d/extract-cd/casper/vmlinuz"
    #cp "$d/edit/boot/vmlinuz-$KERNEL" \
    # $d/extract-cd/isolinux/vmlinuz
    cp "$d/edit/boot/initrd.img-$KERNEL" \
     "$d/extract-cd/casper/initrd"
    #cp "$d/edit/boot/initrd.img-$KERNEL" \
    # $d/extract-cd/isolinux/initrd

    # Update list of packages
    chmod +w "$d/extract-cd"/casper/filesystem.manifest
    chroot "$d/edit" dpkg-query -W --showformat='${Package} ${Version}\n'\
    > "$d/filesystem.manifest"
    cp -f "$d/filesystem.manifest" \
    "$d/extract-cd"/casper/filesystem.manifest
    ## This is not needed in Trisquel
    #cp -f "$d/filesystem.manifest" \
    #"$d/extract-cd/casper/filesystem.manifest-desktop"

    # Make sure that there is no previous squashfs
    rm -f "$d/extract-cd/casper/filesystem.squashfs"

    # Make really compact squashfs
    mksquashfs "$d/edit" \
    "$d/extract-cd/casper/filesystem.squashfs" -b 1048576 -comp xz

    ## Do you want a bigger, but faster-to-make squashfs?
    #mksquashfs "$d/edit" \
    #"$d/extract-cd/casper/filesystem.squashfs" -b 104857

    ## Do you want an even bigger, but even faster-to-make squashfs?
    #mksquashfs "$d/edit" "$d/extract-cd/casper/filesystem.squashfs"

    chmod 644 "$d/extract-cd/casper/filesystem.squashfs"

    ## TODO: Edit extract-cd/README.diskdefines
    ##nano "$d/extract-cd/README.TXT"
    #printf "\n#######\nThis ISO was prepared by $(whoami)\n" >>\
    #"$d/extract-cd/README.TXT"

    ## This is not needed in Trisquel
    #for f in ubiquity casper live-initramfs user-setup discover \
    #xresprobe os-prober libdebian-installer ; do
    #sed -i "/${f}/d" "$d/extract-cd/casper/filesystem.manifest-desktop"
    #done

    here=`/bin/pwd`
    cd "$d/extract-cd"
    # Update MD5 sums
    find . -type f -print0 | xargs -0 md5sum > md5sum.txt

    # Actually make the iso
    mkisofs -r -V "$IMAGE_NAME" -cache-inodes -J -l \
    -b isolinux/isolinux.bin -c isolinux/boot.cat \
    -no-emul-boot -boot-load-size 4 -boot-info-table \
    -o "$outfile" .

    # Make font bold (for announcement)
    /bin/echo -e "\033[1m"
    # Make it USB-bootable
    isohybrid "$outfile"
    # Put font back to normal (actually back to white)
    /bin/echo -e "\033[m"
    md5sum "$outfile" > "$outfile".md5
    cd "$here"

    # Stop printing commands on the screen
    set +x

    # Let know that it's done :)
    echo "# ******* READY :) ******"
    echo "# Your ISO was saved to $outfile"
    echo "# ***********************"
}


case x"$1" in
    xextract) shift ; set -e ; extract_img "$@";;
    xchroot) shift ; set -e ; chroot_live "$@" ;;
    xrelease) shift ; set -e ; release_live "$@" ;;
    xregen) shift ; set -e ; regenerate_img "$@";;
    x_actual_chroot_) _actual_chroot_ ;;
    *) _usage "Invalid command $1" ;;
esac
