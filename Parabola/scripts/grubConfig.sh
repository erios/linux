#!/bin/bash

# echo "KEYMAP=$(dialog --stdout --radiolist "Select Keymap" 20 70 50 ${keymaps[@]})" > /etc/vconsole.conf

mkinitcpio -p linux-libre

hdd=/dev/sdb1
grub-install --recheck --target i386-pc /dev/sdb
grub-mkconfig -o /boot/grub/grub.cfg

exit
