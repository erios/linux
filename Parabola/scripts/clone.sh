#!/usr/bin/env bash

# Exit on error code
set -e

# Check that there is a dialog interface:
[[ -f "$(which dialog)" ]] ||
    (printf "[ERROR]: install dialog first" && exit);

#################################################
#                   Functions                   #
#################################################
prepare_fstab_grub(){
    sudo cat << EOF > /tmp/"$target_uuid"/etc/fstab
#
# /etc/fstab: static file system information
#
# <file system>	<dir>	<type>	<options>	<dump>	<pass>
# UUID=$target_uuid LABEL=GNULINUX USB
# UUID=$target_uuid           	/         	ext4      	rw,relatime,block_validity,delalloc,barrier,user_xattr,acl	0 1
# /dev/sdc2
# /dev/sdc2           	/         	ext4      	rw,relatime	0 1
# UUID="$target_uuid" LABEL=GNULINUX
UUID="$target_uuid"           	/         	ext4      	rw,relatime,block_validity,delalloc,barrier,user_xattr,acl	0 1
EOF
    # Check if there is a NONLINUX partition
    # If there are two partitions, it means that there is a
    # FAT partition
    if [[ "$part_nb" == "2" ]]; then
        make_mounting_dirs
        sudo cat <<EOF >> /tmp/"$target_uuid"/etc/fstab

# Mount the NONLINUX partition
UUID="$non_uuid"       /run/media/NONLINUX     vfat   defaults,uid=1000,gid=100   0 2

# Mount the live user home folder on NONLINUX
/run/media/NONLINUX/DoNotDelete     /home/live   none    defaults,bind  0 2

# Mount the temporary directory (less writes on the USB = longer life)
# /run/media/NONLINUX/DoNotDelete/.tmp    /tmp    none    defaults,umask=000,bind 0 2

# Mount the pacman temporary directories
/run/media/NONLINUX/DoNotDelete/.tmp/pacman    /var/cache/pacman/    none    defaults,bind 0 2
EOF
    fi;
    sed -i "s/CHANGE-MY-UUID/$target_uuid/g" /tmp/"$target_uuid"/boot/grub/grub.cfg
}

make_mounting_dirs (){
    # Create mounting directory if necessary
    check_unmount_create_dir /tmp/"$non_uuid";
    # Mount NONLINUX partition (to create directories)
    sudo mount /dev/disk/by-uuid/"$non_uuid" \
         /tmp/"$non_uuid" ||
        (printf "[ERROR]: Unable to mount NONLINUX";
         printf "on /tmp/$non_uuid\n";
         exit 2);
    for dir in {.tmp,pacman}; do
        # Construct full path (make it look well)
        full_path=/tmp/"$non_uuid";
        full_path="$full_path"/NONLINUX/DoNotDelete
        full_path="$full_path"/"$dir"
        # Check if the directory exists. Create if it
        # doesn't
        [[ -d "$full_path" ]] || sudo mkdir "$full_path"
    done;
    sudo umount /tmp/"$non_uuid"
}

check_and_unmount () {
    # Retrieve which drive should be unmounted
    # Create local variable and retrieve first input
    local hd=$1
    # Unmount the drive (check if it's mounted first)
    ( [[ -z "$(mount | grep "$hd")" ]] ||
          sudo umount "$hd" >/dev/null 2>&1 ) ||
        (printf "[ERROR]: Unable to unmount $hd\n";
         exit 2;)
}

check_unmount_create_dir () {
    # Try to make sure that the requested drive is not
    # mounted (used by something else), and create the
    # mounting directory if it does not exist
    if [[ -d "$1" ]]; then
        check_and_unmount "$1";
    else
        # Create the mounting directory if it does not exist
        mkdir "$1";
    fi;
}

dialog_inputbox_def (){
    # Default inputbox dialog (adds CTRL + C)
    # Create a dialog screen (stdout) with a text input
    # with title given from first input,
    # height of 20 lines and width of 70 characters

    # Args:
    #  1 -- message (string)
    #  dlg_var -- array: ("my" "default" "value")
    # !!! Set dlg_var before running !!!

    # Capture the desired message from input 1
    msg="$1"
    # Capture variable from input 2 (into array)
    # http://stackoverflow.com/questions/1063347/ddg#4017175
    dlg_res=$(dialog --stdout \
                     --inputbox \
                     "$msg (Ctrl+C to exit completely)" \
                     20 70 \
                     "${dlg_var[@]}")
}

loop_inputbox_dialog () {
    # Indefinitely calls dialog_radio_ref if no option is
    # selected

    # (see dialog_inputbox_def)

    # Capture the desired message from input 1
    msg_orig="$1"
    # Capture variable from input 2 (into array)
    # http://stackoverflow.com/questions/1063347/ddg#4017175
    unset dlg_var
    declare -a dlg_var=("${!2}")
    # Loop until "$dlg_var" is not empty
    while [[ "x$dlg_res" == "x" ]]; do
        # If it's the first time the message is shown, set
        # original value
        [[ "$msg" =~ "again" ]] || msg="$msg_orig"
        dialog_inputbox_def "$msg" "${!dlg_var}"
        # # Retrieve answer (just to make sense; unneeded in
        # # Bash)
        # dlg_res="$dlg_res"
        # Add an " (again)" to the message for next try
        # (unless it's already there)
        [[ "$msg" =~ "again" ]] || msg="$msg_orig again"
    done
}

dialog_radioOrCheck_def (){
    # Default radio dialog (adds CTRL + C)
    # Create a dialog screen (stdout) with only one possible
    # option (radiolist), with title given from first input,
    # height of 20 lines, width of 70 characters (standard
    # screen) and list-height of 50. Use the values of hdds
    # for the list

    # Args:
    #  1 -- message (string)
    #  dlg_var -- array: "option_value tag [on|off]"

    # Capture the desired message from input 1
    msg="$1"
    # Type of dialog from input 3
    dlg_type="$3"
    # Capture variable from input 2 (into array)
    # http://stackoverflow.com/questions/1063347/ddg#4017175
    dlg_res=$(dialog --stdout \
                     --"$dlg_type" \
                     "$msg (Ctrl+C to exit completely)" \
                     20 70 50 \
                     ${dlg_var[@]})
}

loop_radioCheck_list_dialog () {
    # Indefinitely calls dialog_radio_ref if no option is
    # selected

    # Capture the desired message from input 1
    msg_orig="$1"
    # Type of dialog from input 3
    dlg_type="$3"
    # Capture variable from input 2 (into array)
    # http://stackoverflow.com/questions/1063347/ddg#4017175
    unset dlg_var
    declare -a dlg_var=("${!2}")
    # Loop until "$dlg_var" is not empty
    while [[ "x$dlg_res" == "x" ]]; do
        # If it's the first time the message is shown, set
        # original value
        [[ "$msg" =~ "again" ]] || msg="$msg_orig"
        dialog_radioOrCheck_def "$msg" "${!dlg_var}" "$dlg_type"
        # # Retrieve answer (just to make sense; unneeded in
        # # Bash)
        # dlg_res="$dlg_res"
        # Add an " (again)" to the message for next try
        # (unless it's already there)
        [[ "$msg" =~ "again" ]] || msg="$msg_orig again"
    done
}

request_orig_type (){
    # Ask from where to copy
    types=( "Path Directory,/dev/... on"
            "Drive Disk,USB,mount... off" )
    unset dlg_res msg
    echo "${new_types[@]}"
    loop_radioCheck_list_dialog \
        "Choose the type of cloning" types[@] radiolist
    orig_type="$dlg_res";

}

request_orig_part () {
    # Original drive (before partition) #########
    # Show plug drives (without hard-drive)
    # Read drives into array
    # https://stackoverflow.com/a/34224575
    # http://wiki.bash-hackers.org/commands/builtin/read
    local -a aux
    while IFS= read drive; do
        # Incrementally add the name of the drives into
        # array
        aux=( "${aux[@]}" "$drive")
    done < <( # Redirect the following to the while loop:
        # Print drive, label and size
        # (util-linux)
        sudo lsblk -ldn -o NAME,LABEL,SIZE -e 1,11)

    # Create array with useful information about the drives
    # (to be used with dialog)
    # From parabola 2016 MATE ISO root-image.fs
    # /root/.scriptsInstallation/install.sh
    for i in "${aux[@]}"; do
        # The name is just the first column (before the first
        # space). Remove anything after the first space.
        name="${i%% *}"
        # Incrementally add the name of the drives into
        # array (another way of doing it)
        hdds[$index]="${name} ${i#* } off"
        # Increase the index (starts with 0; empty, actually)
        index=$((index+1))
    done

    # Ask which is the original drive
    unset dlg_res msg
    loop_radioCheck_list_dialog \
        "Choose the original drive" hdds[@] radiolist
    hdd="$dlg_res"
    # -- end drive

    # Original partition ########################
    # Search and show the partitions from before.
    # Clear temporary variable
    unset aux
    local -a aux
    # Read list of partitions in the drive
    while IFS= read part; do
        aux=( "${aux[@]}" "$part")
    done < <(
        # Print drive, label and size
        # (util-linux)
        sudo lsblk -ln -o NAME,LABEL,SIZE /dev/"$hdd")

    # From /root/.scriptsInstallation/install.sh
    index=0;
    for i in "${aux[@]}"; do
        # The name is just the first column (before the first
        # space). Remove anything after the first space.
        name="${i%% *}"
        [[ "$hdd" == "$name" ]] && continue
        label=$(echo ${i#* } | sed -r 's- +-:-g')
        partitions[$index]="${name} ${label// /:} off"
        index=$((index+1))
    done

    # Ask which is the original paritition
    unset dlg_res msg
    loop_radioCheck_list_dialog \
        "Choose original partition" partitions[@] radiolist
    orig_part="$dlg_res"
    # -- end partition
}

request_orig_path () {
    # Ask which is the original drive
    unset dlg_res msg
    path_template=("/usr/local/play/customiso/parabola/usb")
    loop_inputbox_dialog \
        "Choose the original path" \
        path_template[@]
    orig_path="$dlg_res"
}
# -- end Functions

# *** Start ***

#################################################
#                Request origin                 #
#################################################
# Path or drive selection #######################
request_orig_type

if [[ "Path" == "$orig_type" ]]; then
    # Request original path #####################
    request_orig_path;
elif [[ "Drive" == "$orig_type" ]]; then
    # Request original partition ################
    request_orig_part
fi;

#################################################
#                 Request target                #
#################################################
# Show plug drives (without hard-drive)
# Read drives into array
# https://stackoverflow.com/a/34224575
unset aux drive hdds;
declare -a aux;
# Just make sure that hdd is not empty (and does not
# coincide with a drive, thus ><)
[[ -z "$hdd" ]] && hdd="><"
while IFS= read drive; do
    aux=( "${aux[@]}" "$drive")
done < <(
    # Print drive, label and size
    # (util-linux)
    sudo lsblk -ldn -o NAME,LABEL,SIZE -e 1,11 | grep -v "$hdd");

# Create array with useful information about the drives (to be used
# with dialog)
# From parabola 2016 MATE ISO root-image.fs
# /root/.scriptsInstallation/temporal
for i in "${aux[@]}"; do
    # The name is just the first column (before the first
    # space). Remove anything after the first space.
    name="${i%% *}"
    hdds[$index]="${name} ${i##* } off"
    index=$((index+1))
done

# Ask where to install ##########################
unset dlg_res msg
loop_radioCheck_list_dialog \
    "Choose destination partition(s)" hdds[@] checklist
targets="$dlg_res"

# Inform what is going to happen
dialog --stdout --msgbox \
       "Cloning ${orig} into ${targets} (Cancel with CTRL + C)" 20 70

#################################################
#          Begin whole cloning process          #
#################################################
if [[ "$1" == "--debug" ]]; then
    set -x;
fi;

for drive in ${targets}; do
    info_msg="[INFO]: Working on $drive";
    printf "$info_msg\n";
    # Partitoin drive ###########################
    # Unmount the drive (check if it's mounted first)
    for i in $(ls /dev/"$drive"* | grep -v /dev/"$drive"'$'); do
        check_and_unmount "$i"
    done;
    # Get volume size
    volsize=$(
        # Print the block devices of the system.
        # -d no dependencies (partition), just device
        # -n no header line
        # -o list of values
        # -e exclude: 1--RAM, 11--sdr (cdrom)
        lsblk -dn -o NAME,LABEL,SIZE -e 1,11 |
            awk "/$drive/{print \$NF}");
    # Capture units (using letters)
    units=${volsize##*[[:digit:].]};
    # Get rid of the fractional part and the units (anything after the dot)
    volsize_int=${volsize%.*};
    # Check if the size of the device can hold the system
    linux_size=40;
    if test $volsize_int -ge $linux_size; then
        printf "[INFO]: creating two partitions"
        part_nb=2;
        # Create a FAT32 partition (ugly!!!)
        # set size (leave enough for the system; GigaBytes)
        part1size="+$(( $volsize_int -  $linux_size ))G";
        # https://superuser.com/a/984637
        (
            printf "o\n"; # Create a new empty DOS partition table
            printf "n\n"; # Add a new partition
            printf "p\n"; # Primary partition
            printf "\n"; # Partition number (Accept default: 1)
            printf "\n"; # First sector (Accept default: 2048)
            printf "$part1size\n"; # Last sector (Use calculation: varies)
            printf "t\n"; # Request type change
            printf "c\n"; # Set as FAT32 LBA (ugly!!!)
            printf "x\n"; # Enable expert options
            printf "b\n"; # Ask for beginning of data change
            printf "1\n"; # Partition number (no default)
            # printf "256\n"; # 128KiB (131072 B: 256 x 512 bytes/sector)
            printf "512\n"; # 256KiB (512 x 512 bytes/sector)
            printf "r\n"; # Return to normal menu
            printf "n\n"; # Add a new partition
            printf "p\n"; # Primary partition
            printf "$part_nb\n";# Partition number (Accept default: 2)
            printf "\n"; # First sector (Accept default)
            printf "\n"; # Last sector (Accept default)
            printf "a\n"; # Set bootable
            printf "$part_nb\n"; # Linux partition
            printf "w\n"; # Write changes
        ) |
            sudo fdisk -H 224 -S 56 /dev/"$drive"; # -H 224: heads
                                                   # -S sectors/track
        sudo sync;
        ###########################################
        #      Create a faster FAT32              #
        ###########################################
        # Create a FAT32 filesystem (for
        # compatibility; ugly). To make it faster, we
        # provide -s 32, and make two runs
        # http://forums.patriotmemory.com/showthread.php?3696-HOWTO-Increase-write-speed-by-aligning-FAT32
        # Create a filesystem with 32 reserved sectors to
        # get the right number of sectors
        sectors=$(
            # Create FAT32
            sudo mkfs.fat -v -F 32 -n "NONLINUX" -s 32 \
                 /dev/"$drive""$(( $part_nb - 1))" |
                sed -n '/FAT size/{
                  s-.*size is \([[:digit:]]*\).*-\1-g;p}')
        # !!! Assuming 2 FATs (-F 32) !!!
        # FAT sectors x 2 FATs x 512 bytes/sector
        # sectors=61022;
        bytes_tot=$(( $sectors * 2 * 512 ));
        # 128KiB = 131072 B
        ratio_floor=$(( $bytes_tot / 131072 ));
        # Difference in bytes
        bytes_dif=$(( $bytes_tot - $ratio_floor * 131072));
        # Remaining sectors (byte equivalent)
        sect_remain=$(( $bytes_dif / 512 ))
        # New reserved sector count for alignment
        # (32: originally reserved sectors)
        new_res=$(( $sect_remain + 32 ))
        # Add space for GRUB kernel.img
        # (97KiB for largest: x86_64-efi <2017-08-05 Sat>)
        new_res=$(( $new_res + 100 * 1024 / 512 ))
        # Real formatting (it is not very easy to determine
        # -s : allocation unit size. -F 32 : FAT32; -R :
        # should have enough space for grub kernel.img and
        # the unusable sectors)
        sudo mkfs.fat \
             -F 32 \
             -n "NONLINUX" \
             -s 64 -R "$new_res" \
             /dev/"$drive""$(( $part_nb - 1))"

        # Store UUID from the NONLINUX partition
        non_uuid=$(sudo lsblk -no UUID /dev/"$drive"1 ||
                          exit)
        # Create ext4 without Journaling (to save device
        # life and arguably better maintenance than XFS;
        # Arch Wiki)
        # https://linux-howto-guide.blogspot.com/2009/10/increase-usb-flash-drive-write-speed.html
        # http://forums.patriotmemory.com/showthread.php?3696-HOWTO-Increase-write-speed-by-aligning-FAT32
        sudo mkfs.ext4 -O "^has_journal" -E stripe-width=32 \
             -L GNULINUX /dev/"$drive""$part_nb";
    else
        part_nb=1;
        printf "[INFO]: creating only one partition\n";
        # Assume that there is enough space, and create a partition
        # https://superuser.com/a/984637
        (
            printf "o\n"; # Create a new empty DOS partition table
            printf "n\n"; # Add a new partition
            printf "\n"; # Partition type (Accept default: primary)
            printf "\n"; # Partition number (Accept default: 1)
            printf "\n"; # First sector (Accept default)
            printf "\n"; # Last sector (Accept default)
            printf "x\n"; # Enable expert options
            printf "b\n"; # Ask for beginning of data change
            printf "1\n"; # Partition number (no default)
            # printf "256\n"; # 128KiB (131072 B: 256 x 512 bytes/sector)
            printf "512\n"; # 256KiB (512 x 512 bytes/sector)
            printf "r\n"; # Return to normal menu
            printf "a\n"; # Set bootable
            printf "$part_nb\n"; # Linux partition
            printf "w\n"; # Write changes
        ) | sudo fdisk /dev/"$drive";
        sudo sync;
        # Create ext4 without Journaling (to save device
        # life and arguably better maintenance than XFS;
        # Arch Wiki)
        # -E stripe-width=32 taken from
        # https://linux-howto-guide.blogspot.com/2009/10/increase-usb-flash-drive-write-speed.html
        sudo mkfs.ext4 -O "^has_journal" -E stripe-width=32 \
             -L GNULINUX /dev/"$drive""$part_nb";
    fi;
    # Clone partition ###########################
    target_part="$drive""$part_nb"
    ## Mount original drive (if selected)
    if [[ "$orig_type" == "Drive" ]]; then
        drv_prt="$drive""$part_nb";
        ## Mount original partition ##
        # Retrieve UUID from the original device
        orig_uuid=$(sudo lsblk -no UUID /dev/"$drv_prt" || exit)
        # If, for some reason we don't have the UUID, exit
        [[ ! "x$orig_part_uuid" == "x" ]] ||
            (printf "[ERROR]: Unable to get UUID from origin\n";
             exit 2)
        # Try to make sure that the requested drive is not
        # mounted (used by something else), and create the
        # mounting directory if it does not exist
        check_unmount_create_dir /tmp/"$orig_uuid";
        # Mount original partition
        check_and_unmount /dev/"$orig_part";
        # Mount the original partition in the assigned directory
        sudo mount /dev/"$orig_part" /tmp/"$orig_uuid"  ||
            (printf "[ERROR]: Unable to mount /dev/$orig_part";
             printf "on /tmp/$orig_uuid\n";
             exit 2);
        # Store the mounting path (inexistent so far for the
        # case of Drive)
        orig_path=/tmp/"$orig_uuid"
    fi;
    ## Mount target partition ##
    # Retrieve UUID from the target device
    target_uuid=$(sudo lsblk -no UUID /dev/"$target_part" || exit)
    # If, for some reason we don't have the UUID, exit
    [[ ! "x$target_uuid" == "x" ]] ||
        (printf "[ERROR]: Unable to get UUID from targetin\n";
         exit 2)
    # Try to make sure that the target directories are not
    # mounted (used by something else)
    check_unmount_create_dir /tmp/"$target_uuid"
    check_and_unmount /dev/"$target_part"
    # Mount target partition on the assigned directory
    sudo mount /dev/"$target_part" /tmp/"$target_uuid"  ||
        (printf "[ERROR]: Unable to mount /dev/$target_part";
         printf "on /tmp/$target_uuid\n";
         exit 2);

    #############################################
    #             Synchronise paths             #
    #############################################
    # Let the synchronisation run in the background
    (sudo rsync -a "$orig_path"/ /tmp/"$target_uuid"/ &&
     prepare_fstab_grub) &
    # Get pid of process
    pid_now="$!";
    # Store into array (to wait for them)
    pids=("${pids[@]} $pid_now");
    hd_num=("${hd_num[@]} $drive");
    printf "*************************************\n";
    printf "Copying files (this may take a while)\n";
    printf "*************************************\n";
done;


# Check errors ##################################
for job in "$pids"; do
    wait $job || failed=(${failed[@]} $job);
done

[[ $failed ]] && printf "Failed: ${failed[@]}";

printf "******************************\n"
printf "You are not done. Find grub-install\n"
printf "from Parabola and install it. See\n"
printf "install.sh install.sh.diff\n"
printf "systemConfig.sh systemConfig.sh.diff\n"
printf "in the scripts directory.\n"
printf "Also, make sure that /etc/mkinitcpio.conf\n"
printf "has  block keyboard  before autodetect\n"
printf "******************************\n"

printf "\nFinished successfully!\n"

if [[ "$1" == "--debug" ]]; then
    set +x;
fi;

exit 0;

# sudo qemu-system-x86_64 -enable-kvm -hda /dev/sdb -hdb /dev/sdd -m 6G -boot menu=on
# sudo qemu-system-x86_64 -enable-kvm -hda /dev/sdb -hdb /dev/sdd -cdrom ~/Downloads/parabola-mate-2016.11.03-dual.iso/parabola-mate-2016.11.03-dual.iso -boot d -m 6G
# https://askubuntu.com/a/54824
# 1. Use Ctrl + Alt + 2 to switch to the QEMU console.
# 2. Type sendkey ctrl-alt-f1 and press Enter.
# 3. Use Ctrl + Alt + 1 to switch back to the virtual system, which should now by at TTY1.
