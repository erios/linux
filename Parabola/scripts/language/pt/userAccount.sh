#!/bin/bash

user=$(cat /temporal | grep "userName")

password=$(dialog --stdout --passwordbox "Insira a senha para o usuário ${user#*=}" 8 40)

while [[ $password != $password2 ]]; do
	password2=$(dialog --stdout --passwordbox "Repita a senha" 8 40) 
done

useradd -m -g users -G "wheel" -s /bin/bash -p $(openssl passwd $password) ${user#*=}

if [ -x /usr/bin/setxkbmap ]; then
	echo "setxkbmap $(cat /.codecheck | grep XKBMAP= | cut -d '=' -f 2)" >> /home/${user#*=}/.bashrc
fi

if [ -x /usr/bin/gsettings ]; then
	sudo -u {user#*=} \
	if [ -d /usr/share/themes/Radiance-Purple ]; then
		gsettings set org.mate.interface gtk-theme 'Radiance-Purple'
		gsettings set org.mate.Marco.general theme 'Radiance-Purple'
	fi \
	if [ -d /usr/share/icons/RAVE-X-Dark-Purple ]; then
		gsettings set org.mate.interface icon-theme 'RAVE-X-Dark-Purple'
	fi \
	if [ -d /usr/share/icons/mate ]; then
		gsettings set org.mate.peripherals-mouse cursor-size '18'
		gsettings set org.mate.peripherals-mouse cursor-theme 'mate'
	fi \
	if [ -f /etc/wallpaper.png ]; then
		gsettings set org.mate.background picture-filename '/etc/wallpaper.png'
	fi
fi

exit
