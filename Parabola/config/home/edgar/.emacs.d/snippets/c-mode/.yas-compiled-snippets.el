;;; Compiled snippets and support files for `c-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'c-mode
                     '(("powf" "powf(${1:float}, ${2:float})\n" "powf(,);" nil nil nil "/home/edgar/.emacs.d/snippets/c-mode/math.powf" nil nil)
                       ("pow" "pow(${1:float}, ${2:float})\n" "pow(,);" nil nil nil "/home/edgar/.emacs.d/snippets/c-mode/math.pow" nil nil)
                       ("fl" "float $1" "float;" nil nil nil "/home/edgar/.emacs.d/snippets/c-mode/float" nil nil)))


;;; Do not edit! File generated at Mon Feb  5 01:35:19 2018
