;;; Compiled snippets and support files for `latex-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'latex-mode
                     '(("\\u" "\\\\und{$1}$0" "undamaged" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/und.snippet" nil nil)
                       ("uI" "\\\\uI{$1}$0" "undam invar" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/uI.snippet" nil nil)
                       ("\\t" "\\tensor\\{$1\\}" "\\tensor" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/tensor.snippet" nil nil)
                       ("s" "\\\\sum_{$1}^{$2}{$3}$0" "sum" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/sum.snippet" nil nil)
                       ("subsubsec" "\\subsubsection{${1:name}}\n\\label{sec:${2:label}}\n\n$0" "subsubsec" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/subsubsection" nil nil)
                       ("subsec" "\\subsection{${1:name}}\n\\label{sec:${2:label}}\n\n$0" "subsection" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/subsection" nil nil)
                       ("subfig" "\\begin{figure}[ht]\n  \\centering\n  \\begin{subfigure}[t]{${2:0.5}\\textwidth}\n    \\centering\n    \\includegraphics[width=\\textwidth]{${3:path}}\n    \\caption{${5:subcaption}}\n    \\label{fig:${4:label}}\n  \\end{subfigure}\n  \\caption{${6:caption}}\n\\label{fig:${7:label}}\n\\end{figure}\n" "subfigure" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/subfigure" nil nil)
                       ("cventry" "\\cventry{${1:year}}{${2:job}}{${3:employer}}{${4:city}}{${5:country}}{${6:description}}\n$0\n" "moderncv-cventry" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/moderncv-cventry" nil nil)
                       ("lrb" "\\lbrace{$1\\rbrace}" "\\left\\{\\right}" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/l{.snippet" nil nil)
                       ("lm" "\n\\\\[\n$1\n\\\\]\n$0" "math" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/long-eq.snippet" nil nil)
                       ("l" "\\\\($1\\\\)$0" "math" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/latex-eq.snippet" nil nil)
                       ("lrs" "\\left[$0\\right]" "\\left\\[\\right]" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/l[.snippet" nil nil)
                       ("lrp" "\\\\left($1\\\\right)" "\\left(\\right)" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/l(.snippet" nil nil)
                       ("i" "\\\\int_{$1}^{$2}{$3 d$4}$0" "int" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/int.snippet" nil nil)
                       ("inkssub" "\\begin{subfigure}[c]{${1:\\linewidth}}\n  \\centering\n  \\simplesvg{${2:height=\\textheight}}{${3:file without extension}}\n  \\caption{${4:subcaption}\n    \\label{${5:fig:$3}}\n  }\n\\end{subfigure}\n$0" "Inkscape \\subfig" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/inksub.yasnippet" nil nil)
                       ("inksub" "\\begin{figure}[htbp]\n  \\centering\n  \\begin{subfigure}[c]{$4\\linewidth}\n    \\centering\n    \\simplesvg{${5:height=\\textheight}}{${3:file without extension}}\n    \\caption{${6:subcaption}\n      \\label{${7:fig:$3}}\n    }\n  \\end{subfigure}\n  \\hfill\n  \\begin{subfigure}[c]{${9:$4}\\linewidth}\n    \\centering\n    \\simplesvg{${10:$5}}{${8:file without extension}}\n    \\caption{${11:subcaption}\n      \\label{${12:fig:$8}}\n    }\n  \\end{subfigure}\n  \\caption{${1:maincaption}\n    \\label{${2:fig:${4:label}}}\n  }\n\\end{figure}\n$0" "Inkscape \\fig\\subfig" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/inkfigsub.yasnippet" nil nil)
                       ("inkfig" "\\begin{figure}[htbp]\n  \\centering\n  \\simplesvg{${2:width=\\linewidth}}{${1:file without extension}}\n  \\caption{${3:caption}\n    \\label{fig:${4:$1}}}\n\\end{figure}\n$0" "Inkscape figure" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/inkfig.yasnippet" nil nil)
                       ("\\f" "\\frac{${1:numerator}}{${2:denominator}}$0" "frac" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/frac.snippet" nil nil)
                       ("fig" "\\begin{figure}[ht]\n  \\centering\n  \\includegraphics[${1:options}]{${2:path.pdf}}\n  \\caption{\\label{fig:${3:label}} $0}\n\\end{figure}\n" "figure" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/figure" nil nil)
                       ("devt" "\\\\devt{$1}$0" "dev tensor" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/devt.snippet" nil nil)
                       ("\\dev" "\\dev{$1}$0" "deviatoric" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/dev.snippet" nil nil)
                       ("\\d" "\\deriv{${1:function}}{${2:in terms of}}$0" "deriv" nil nil nil "/home/edgar/.emacs.d/snippets/latex-mode/deriv.snippet" nil nil)))


;;; Do not edit! File generated at Mon Feb  5 01:35:19 2018
