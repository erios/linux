;;; Compiled snippets and support files for `org-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'org-mode
                     '(("py-" "#+NAME: py-\n#+CAPTION:\n#+BEGIN_SRC python :results none\n$0\n#+END_SRC" "py-blk" nil nil nil "/home/edgar/.emacs.d/snippets/org-mode/py-blk.snippet" nil nil)
                       ("mxmal" "#+BEGIN_SRC maxima :results output latex replace :exports results :eval no-export\n  print(\"\\\\\\\\begin{align}\")$\n  set_tex_environment_default(\"\",\"\")$\n  1+1;$0\n  tex(\\\\%)$\n  print(\"\\\\\\\\end{align}\")$\n#+END_SRC\n" "mxmAlgn" nil nil
                        ((yas-indent-line 'fixed))
                        "/home/edgar/.emacs.d/snippets/org-mode/maximaAlign" nil nil)
                       ("mat3z" "\\left[\n\\begin{array}{${1:ccc}}\n${2:0} & ${3:0} & ${4:0} \\\\\\\\\n${5:0} & ${6:0} & ${7:0} \\\\\\\\\n${8:0} & ${9:0} & ${10:0}\n\\end{array}\n\\right]\n$0" "zeros3x3" nil nil
                        ((yas-indent-line 'fixed))
                        "/home/edgar/.emacs.d/snippets/org-mode/mat3z" nil nil)
                       ("mat3" "\\left[\n\\begin{array}{${1:ccc}}\n${2:m_11} & ${3:m_12} & ${4:m_13} \\\\\\\\\n${5:m_21} & ${6:m_22} & ${7:m_23} \\\\\\\\\n${8:m_31} & ${9:m_32} & ${10:m_33}\n\\end{array}\n\\right]\n$0" "matrix3x3" nil nil
                        ((yas-indent-line 'fixed))
                        "/home/edgar/.emacs.d/snippets/org-mode/mat3" nil nil)
                       ("lrb" "\\\\left\\{$0\\\\right\\}" "\\left{" nil nil nil "/home/edgar/.emacs.d/snippets/org-mode/lrb.snippet" nil nil)
                       ("ltx" "\\\\($1\\\\)$0" "latex" nil nil nil "/home/edgar/.emacs.d/snippets/org-mode/latex.snippet" nil nil)
                       ("algn" "\\\\begin\\{align}\n  $0\n\\\\end{align}" "algn" nil nil
                        ((yas-indent-line 'fixed))
                        "/home/edgar/.emacs.d/snippets/org-mode/envalign" nil nil)
                       ("3dm" "#+NAME: 3d-maxima\n#+HEADER: :file maxima-3d.png\n#+HEADER: :results graphics\n#+HEADER: :exports results\n#+BEGIN_SRC maxima\n  load(draw)$\n  o:[0,0,0];\n  ${1:vec1}: ${2:[0, 0, 0]}$\n\n  draw3d(\n    terminal   = 'png,\n    file_name  = \"maxima-3d\",\n    dimensions = [300,300],\n    head_length=0.2,\n    color = blue, vector(o, a), label([\"a\",5,2,0])\n    )$\n#+END_SRC" "3dmaximaPlot" nil nil
                        ((yas-indent-line 'fixed))
                        "/home/edgar/.emacs.d/snippets/org-mode/3dmx" nil nil)))


;;; Do not edit! File generated at Fri May 18 10:46:37 2018
