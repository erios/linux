;;; -*- no-byte-compile: t -*-
(define-package "find-file-in-repository" "1.2" "Quickly find files in a git, mercurial or other repository" 'nil :commit "8b888f85029a2ff9159a724b42aeacdb051c3420" :url "https://github.com/hoffstaetter/find-file-in-repository" :keywords '("files" "convenience" "repository" "project" "source control"))
