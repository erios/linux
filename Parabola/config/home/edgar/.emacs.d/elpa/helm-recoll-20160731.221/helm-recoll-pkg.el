;;; -*- no-byte-compile: t -*-
(define-package "helm-recoll" "20160731.221" "helm interface for the recoll desktop search tool." '((helm "1.9.9")) :commit "cc4c4fa9c8f4f99383647baa8512b60523dc8b36" :url "https://github.com/emacs-helm/helm-recoll" :keywords '("convenience"))
