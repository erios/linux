;; ;; Suppress warnings
;; ;; http://compgroups.net/comp.lang.lisp/switching-off-warning-messages-in-clisp/704581
;; (setf custom:*suppress-check-redefinition* t)

;; Additional mactex utilities.
;; All functions are based on code by Richard J. Fateman, copyright 1987.
;; Fateman's code was ported to Common Lisp by William Schelter.

;; If you want LaTeX style quotients, first load mactex and second
;; define tex-mquotient as follows

(defun tex-mquotient (x l r)
  (if (or (null (cddr x)) (cdddr x)) (wna-err (caar x)))
  (setq l (tex (cadr x) (append l '("\\frac{")) nil 'mparen 'mparen)
	r (tex (caddr x) (list "}{") (append '("}") r) 'mparen 'mparen))
  (append l r))

;; (defprop mtimes ("\\,") texsym)
;; adds a thin space.
;; The following tells TeX to use whatever it thinks best.
;; (defprop mtimes (" ") texsym)

;; To use amsmath style matrices, define tex-matrix as
;; (to TeX the code, you'll need to \usepackage{amsmath})

(defun tex-matrix (x l r) ;; matrix looks like ((mmatrix)((mlist) a b) ...)
  (append l `("\\begin{bmatrix}")
	 (mapcan #'(lambda(y)
			  (tex-list (cdr y) nil (list " \\\\ ") " & "))
		 (cdr x))
	 '("\\end{bmatrix}") r))

;;---------------------------------------Giorgio Nucci
;;--- REDEFINE THE FUNCTIONS TO INCLUDE PARENTHESIS---
;; (OR THE | SYMBOL FOR THE DETERMINANT)
;; I DON'T KNOW HOW TO DO THIS WITH tex-setup:
(defprop %determinant tex-determinant tex)
(defun tex-determinant(x l r)
  ;; format as \determinant\left( \right) assuming implicit parens for determinant grouping
  ;; (tex (cadr x) (append l '("\\left|")) (append '("\\right|") r) 'mparen 'mparen))
  (tex (cadr x) (append l '("\\det\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %acos tex-acos tex)
(defun tex-acos(x l r)
  ;; format as \acos\left( \right) assuming implicit parens for acos grouping
  (tex (cadr x) (append l '("\\acos\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %asin tex-asin tex)
(defun tex-asin(x l r)
  ;; format as \asin\left( \right) assuming implicit parens for asin grouping
  (tex (cadr x) (append l '("\\arcsin\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %atan tex-atan tex)
(defun tex-atan(x l r)
  ;; format as \atan\left( \right) assuming implicit parens for atan grouping
  (tex (cadr x) (append l '("\\atan\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %cosh tex-cosh tex)
(defun tex-cosh(x l r)
  ;; format as \cosh\left( \right) assuming implicit parens for cosh grouping
  (tex (cadr x) (append l '("\\cosh\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %cos tex-cos tex)
(defun tex-cos(x l r)
  ;; format as \cos\left( \right) assuming implicit parens for cos grouping
  (tex (cadr x) (append l '("\\cos\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %cot tex-cot tex)
(defun tex-cot(x l r)
  ;; format as \cot\left( \right) assuming implicit parens for cot grouping
  (tex (cadr x) (append l '("\\cot\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %coth tex-coth tex)
(defun tex-coth(x l r)
  ;; format as \coth\left( \right) assuming implicit parens for coth grouping
  (tex (cadr x) (append l '("\\coth\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %csc tex-csc tex)
(defun tex-csc(x l r)
  ;; format as \csc\left( \right) assuming implicit parens for csc grouping
  (tex (cadr x) (append l '("\\csc\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %del tex-del tex)
(defun tex-del(x l r)
  ;; format as \mathop{}\!\mathrm{d}\left( \right) assuming implicit parens for del grouping
  ;; www.latex-community.org/forum/viewtopic.php?f=46&t=11840&p=75894
  (tex (cadr x) (append l '("\\mathop{}\\!\\mathrm{d}\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %dim tex-dim tex)
(defun tex-dim(x l r)
  ;; format as \dim\left( \right) assuming implicit parens for dim grouping
  (tex (cadr x) (append l '("\\dim\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %exp tex-exp tex)
(defun tex-exp(x l r)
  ;; format as \exp\left( \right) assuming implicit parens for exp grouping
  (tex (cadr x) (append l '("\\exp\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %gcd tex-gcd tex)
(defun tex-gcd(x l r)
  ;; format as \gcd\left( \right) assuming implicit parens for gcd grouping
  (tex (cadr x) (append l '("\\gcd\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %inf tex-inf tex)
(defun tex-inf(x l r)
  ;; format as \inf\left( \right) assuming implicit parens for inf grouping
  (tex (cadr x) (append l '("\\inf\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %integrate tex-int tex)
(defun tex-int (x l r)
  (let ((s1 (tex (cadr x) nil nil 'mparen 'mparen)) ;;integrand delims / & d
	(var (tex (caddr x) nil nil 'mparen rop))) ;; variable
    (cond((= (length x) 3)
	  ;(append l `("\\int {" ,@s1 "}{\\;d" ,@var "}") r))
	  ;; www.latex-community.org/forum/viewtopic.php?f=46&t=11840&p=75894
	  (append l `("\\int {" ,@s1 "}{\\;\\mathop{}\\!\\mathrm{d}\\left(" ,@var "\\right)}") r))
	 (t ;; presumably length 5
	  (let ((low (tex (nth 3 x) nil nil 'mparen 'mparen))
		;; 1st item is 0
		(hi (tex (nth 4 x) nil nil 'mparen 'mparen)))
	    ;(append l `("\\int_{" ,@low "}^{" ,@hi "}{" ,@s1 "\\;d" ,@var "}") r))))))
	    ;; www.latex-community.org/forum/viewtopic.php?f=46&t=11840&p=75894
	    (append l `("\\int_{" ,@low "}^{" ,@hi "}{" ,@s1 "\\;\\mathop{}\\!\\mathrm{d}\\left(" ,@var "\\right)}") r))))))

(defprop %ln tex-ln tex)
(defun tex-ln(x l r)
  ;; format as \ln\left( \right) assuming implicit parens for ln grouping
  (tex (cadr x) (append l '("\\ln\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %log tex-log tex)
(defun tex-log(x l r)
  ;; format as \log\left( \right) assuming implicit parens for log grouping
  (tex (cadr x) (append l '("\\log\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %max tex-max tex)
(defun tex-max(x l r)
  ;; format as \max\left( \right) assuming implicit parens for max grouping
  (tex (cadr x) (append l '("\\max\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %min tex-min tex)
(defun tex-min(x l r)
  ;; format as \min\left( \right) assuming implicit parens for min grouping
  (tex (cadr x) (append l '("\\min\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %sec tex-sec tex)
(defun tex-sec(x l r)
  ;; format as \sec\left( \right) assuming implicit parens for sec grouping
  (tex (cadr x) (append l '("\\sec\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %sin tex-sin tex)
(defun tex-sin(x l r)
  ;; format as \sin\left( \right) assuming implicit parens for sin grouping
  (tex (cadr x) (append l '("\\sin\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %sinh tex-sinh tex)
(defun tex-sinh(x l r)
  ;; format as \sinh\left( \right) assuming implicit parens for sinh grouping
  (tex (cadr x) (append l '("\\sinh\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %tan tex-tan tex)
(defun tex-tan(x l r)
  ;; format as \tan\left( \right) assuming implicit parens for tan grouping
  (tex (cadr x) (append l '("\\tan\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %tanh tex-tanh tex)
(defun tex-tanh(x l r)
  ;; format as \tanh\left( \right) assuming implicit parens for tanh grouping
  (tex (cadr x) (append l '("\\tanh\\left(")) (append '("\\right)") r) 'mparen 'mparen))

(defprop %texbar tex-texbar tex)
(defun tex-texbar(x l r)
  ;; format as \atan\left( \right) assuming implicit parens for atan grouping
  (tex (cadr x) (append l '("\\bar{")) (append '("}") r) 'mparen 'mparen))

;;--- FINISH RE-DEFINING FUNCTIONS---
;;-----------------------------------

;; REDEFINE GREEK VARIABLES (I LIKE vartheta,
;; BUT IT MAKES IT HARDER TO READ)
;(defprop $theta "\\vartheta" texword)
(defprop $vartheta "\\vartheta" texword)
(defprop $theta "\\theta" texword)
;(defprop $epsilon "\\varepsilon" texword)
(defprop $varepsilon "\\varepsilon" texword)
(defprop $epsilon "\\epsilon" texword)
;(defprop $phi "\\varphi" texword)
(defprop $varphi "\\varphi" texword)
(defprop $phi "\\phi" texword)

;; ******************************
;; * Trying to tweak imath-mode *
;; ******************************
;; (defun imath-maxima-str-lstrip (imstr sub)
;;   "Deletes a substring (sub) at the beginning of a string (imstr).
;; sub must be at the beginning and shorter than imstr. This function
;; just strips as many characters as sub is long.
;;
;; To be used with ___ when loading an imath's file to delete `{maxima '
;; Note: this is my first function ever!"
;;   (subseq imstr (length sub)) ;; just skip the number of characters
;;   )
;;
;; (defun imath-maxima-str-rstrip (imstr sub)
;;   "Deletes a substring (sub) at the end of a string (imstr).
;; sub must be at the beginning and shorter than imstr. This function
;; just strips as many characters as sub is long.
;;
;; To be used with ___ when loading an imath's file to delete `{maxima '
;; Note: this is my second function. I used let for the first time! (but
;; it was too much, so it's gone now)"
;;   (subseq imstr 0 (- (length imstr) (length sub)))
;;   )

;; (defun $imath_maxima_str_cond_lstrip (imstr sub)
;;   "Removes a substring at the beginning of a string.
;; Warning: Doesn't check if sub is longer than imstr
;; imstr: string in which substitution is performed
;; sub: string which should be equal or shorter than imstr in length
;; "
;;   (let ((len_imstr (length imstr)) ;; create variables for length of strings
;;       (len_sub (length sub))
;;       )
;;   (;; Get rid of the initial substring (sub) in imstr
;;    setq imstr (if ;; make sure that the string is not empty
;; 		  (and (> len_imstr 0) (> len_imstr len_sub))
;; 		  (if ;; check if there is substring at the beginning of imstr
;; 		      (string=
;; 		       (subseq imstr 0 len_sub)
;; 		       sub
;; 		       )
;; 		      (subseq ;; then strip substring
;; 		       imstr len_sub
;; 		       )
;; 		    imstr ;; no substring, just return imstr
;; 		    ) ;; end if, check beginning
;; 		imstr ;; empty or shorter than sub, just return imstr
;; 		) ;; end if, check empty or shorter
;; 	);; imstr modified or kept (side effect)
;;   )
;;   )
;;
;; (defun $imath_maxima_str_cond_rstrip (imstr sub)
;;   "Removes a substring at the end of a string.
;; Warning: Doesn't check if sub is longer than imstr
;; imstr: string in which substitution is performed
;; sub: string which should be equal or shorter than imstr in length
;; "
;;   (let ((len_imstr (length imstr)) ;; create variables for length of strings
;;       (len_sub (length sub))
;;       )
;;   (;; Get rid of the ending substring (sub) in imstr
;;    setq imstr (if ;; make sure that the string is not empty
;; 		  (and (> len_imstr 0) (> len_imstr len_sub))
;; 		  (if ;; check if there is substring at the beginning of imstr
;; 		      (string=
;; 		       (subseq imstr (- len_imstr len_sub) len_imstr)
;; 		       sub
;; 		       )
;; 		      (subseq ;; then strip substring
;; 		       subseq imstr 0 (- len_imstr len_sub)
;; 		       )
;; 		    imstr ;; no substring, just return imstr
;; 		    ) ;; end if, check beginning
;; 		imstr ;; empty or shorter than sub, just return imstr
;; 		) ;; end if, check empty or shorter
;; 	);; imstr modified or kept (side effect)
;;   )
;;   )
;;
;; (defun $imath_batch (filename &optional (demo :batch)
;; 	       &aux tem   (possible '(:demo :batch :test)))
;;   "giving a second argument makes it use demo mode, ie pause after evaluation
;;    of each command line"
;;   (cond ((setq tem (member ($mkey demo) possible :test #'eq))
;; 	 (setq demo (car tem)))
;; 	(t (format t (intl:gettext "batch: second argument must be 'demo, 'batch, or 'test; found: ~A, assume 'batch~%") demo)))
;;
;;   (setq filename ($file_search1 filename
;; 				(if (eql demo :demo)
;; 				    '((mlist) $file_search_demo )
;; 				    '((mlist) $file_search_maxima ))))
;;   (cond ((eq demo :test)
;; 	 (test-batch filename nil :show-all t))
;; 	(t
;; 	 (let (($load_pathname filename) (*read-base* 10.))
;; 	   (with-open-file (in-stream filename)
;; 	     (format t (intl:gettext "~%read and interpret file: ~A~%")
;; 		     (truename in-stream))
;; 	     (catch 'macsyma-quit (continue in-stream demo))
;; 	     (namestring in-stream))))))

;; ;; Re-enable warnings
;; (setf custom:*suppress-check-redefinition* nil)
