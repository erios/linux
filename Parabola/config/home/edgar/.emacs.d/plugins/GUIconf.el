(custom-set-faces
     ;; custom-set-faces was added by Custom.
     ;; If you edit it by hand, you could mess it up, so be careful.
     ;; Your init file should contain only one such instance.
     ;; If there is more than one, they won't work right.
     '(default ((t (:inherit nil :stipple nil :background "#2e3436" :foreground "light goldenrod" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 120 :width normal :foundry "PfEd" :family "DejaVu Sans Mono"))))
     '(company-scrollbar-bg ((t (:background "#454e51"))))
     '(company-scrollbar-fg ((t (:background "#394143"))))
     '(company-tooltip ((t (:inherit default :background "#32393b"))))
     '(company-tooltip-annotation ((t (:inherit font-lock-comment-face))))
     '(company-tooltip-common ((t (:inherit font-lock-constant-face))))
     '(company-tooltip-selection ((t (:inherit font-lock-function-name-face))))
     '(font-lock-string-face ((t (:foreground "orange3"))))
     '(org-footnote ((t (:foreground "thistle4" :underline t))))
     '(org-ref-ref-face ((t (:inherit org-link :foreground "coral")))))

;; Disable SIGSTSP signal to suspend Emacs
;; https://www.emacswiki.org/emacs/DotEmacsChallenge
(when window-system
  (global-unset-key "\C-z")) ; iconify-or-deiconify-frame (C-x C-z)

;; Dark theme for company
;; https://company-mode.github.io/
(require 'color)

(let ((bg (face-attribute 'default :background)))
  (custom-set-faces
   `(company-tooltip ((t (:inherit default :background ,(color-lighten-name bg 2)))))
   `(company-scrollbar-bg ((t (:background ,(color-lighten-name bg 10)))))
   `(company-scrollbar-fg ((t (:background ,(color-lighten-name bg 5)))))
   `(company-tooltip-selection ((t (:inherit font-lock-function-name-face))))
   `(company-tooltip-annotation ((t (:inherit font-lock-comment-face))))
   `(company-tooltip-common ((t (:inherit
                                 font-lock-constant-face))))))
