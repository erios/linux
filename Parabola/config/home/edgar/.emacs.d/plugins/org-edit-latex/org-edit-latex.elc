;ELC   
;;; Compiled
;;; in Emacs version 24.4.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\210\300\303!\210\300\304!\210\305\306\307\310\311DD\312\313\314\315\316&\210\305\317\307\310\320DD\321\313\314\315\316&\207" [require org org-element ox-latex preview custom-declare-variable org-edit-latex-frag-master funcall function #[0 "\300\207" ["frag-master.tex"] 1 "frag-master.tex\n\n(fn)"] "Master file for LaTeX fragments." :group org-edit-latex :version "24.4" org-edit-latex-create-master #[0 "\300\207" [t] 1 "\n\n(fn)"] "Decide whether we should create a TeX-master file."] 8)
#@31 Element type before wrapping.
(defvar org-edit-latex--before-type nil (#$ . 938))
(make-variable-buffer-local 'org-edit-latex--before-type)
#@43 Regexp to match beginning of inline LaTeX
(defconst org-edit-latex-inline-beg-regexp "\\\\(\\|\\$[^$]\\|\\\\\\sw" (#$ . 1084))
#@107 Non-nil if Org-Edit-Latex mode is enabled.
Use the command `org-edit-latex-mode' to change this variable.
(defvar org-edit-latex-mode nil (#$ . 1218))
(make-variable-buffer-local 'org-edit-latex-mode)
#@48 LaTeX editing in org mode.

(fn &optional ARG)
(defalias 'org-edit-latex-mode #[256 "\301 \302=\203 ?\202 \303!\304V\211\203. \305\306\307\310#\210\305\311\307\312#\210\313 \210\314\315\316\"\210\202= \317\306\310\"\210\317\311\312\"\210\320\315\316\"\210\321\322\203G \323\202H \324\"\210\325\326!\203i \301 \203\\ \211\301 \232\203i \327\330\203f \331\202g \332\"\210\210\333 \210\207" [org-edit-latex-mode current-message toggle prefix-numeric-value 0 advice-add org-edit-special :around org-edit-latex--wrap-maybe org-edit-src-exit org-edit-latex--unwrap-maybe org-edit-latex-create-master-maybe add-hook org-src-mode-hook org-edit-latex--set-TeX-master advice-remove remove-hook run-hooks org-edit-latex-mode-hook org-edit-latex-mode-on-hook org-edit-latex-mode-off-hook called-interactively-p any message "Org-Edit-Latex mode %sabled" "en" "dis" force-mode-line-update] 7 (#$ . 1425) (byte-code "\206 \301C\207" [current-prefix-arg toggle] 1)])
#@184 Hook run after entering or leaving `org-edit-latex-mode'.
No problems result if this variable is not bound.
`add-hook' automatically binds it.  (This is true for all hook variables.)
(defvar org-edit-latex-mode-hook nil (#$ . 2394))
(byte-code "\301\302\303\304\300!\205\n \305\211%\207" [org-edit-latex-mode-map add-minor-mode org-edit-latex-mode " Edit-LaTeX" boundp nil] 6)
#@63 Set `TeX-master' variable for specific src-edit buffer.

(fn)
(defalias 'org-edit-latex--set-TeX-master #[0 "\303!\205 	\304=\205 \305\306 \307\310#\207" [org-edit-latex-frag-master major-mode TeX-master file-exists-p latex-mode define-key current-local-map [remap preview-at-point] org-edit-latex-preview-at-point] 4 (#$ . 2778)])
#@50 Preview LaTeX at point in the edit buffer.

(fn)
(defalias 'org-edit-latex-preview-at-point #[0 "\301\302 )\207" [buffer-file-name nil preview-at-point] 1 (#$ . 3121) nil])
#@352 Create master file based on value of variable `org-edit-latex-create-master'.

Its value should be one of the following cases:

'overwrite:    when master file already exists, overwrite it.
'ask:          will ask first before creating master file.
other non-nil: when master doesn't exist, create one.
nil:           do not create master file.

(fn)
(defalias 'org-edit-latex-create-master-maybe #[0 "\302!\211\203 	\303=\203 \304\305!\203 \306 \202< \211?\205< \307	\310\"\203/ \304\311!\205< \306 \202< \307	\312\"\203: \313\202< \306 \207" [org-edit-latex-frag-master org-edit-latex-create-master file-exists-p overwrite y-or-n-p "This will overwrite existing TeX-master. Are you sure?" org-edit-latex--create-master memql 'ask "There is no TeX-master. Do you want to create one?" 'nil nil] 4 (#$ . 3302)])
#@146 Force the creation of the AUCTeX auto file for a master
buffer. Borrowed from auctex. The original function name is
`bib-create-auto-file'

(fn)
(defalias 'org-edit-latex-create-auto-file #[0 "\303\304!\204\n \305\306!\210\307\211	\204 \n\310 )\207" [TeX-auto-save TeX-header-end LaTeX-header-end require latex error "Sorry, This is only useful if you have AUCTeX" t TeX-auto-write] 3 (#$ . 4124) nil])
#@75 Create a TeX-master file. Borrowed from
`org-create-formula-image'.

(fn)
(defalias 'org-edit-latex--create-master #[0 "\302\303\304\305!!\"\306	!\211\307\310\311!!\312\313\314\315\316\317!\320\"\321\322%DC\216r\211q\210c\210\323\324\325\261\210\326\327 \266\313\314\330\316\317!\331\"\332\322%\333K\312\313\314\334\316\317!\335\"\321\322%DC\216\333M\210\336 )\266\202)rq\210\337\326\211\326\314%\210*\266\202\207" [org-format-latex-header org-edit-latex-frag-master org-latex-make-preamble org-export-get-environment org-export-get-backend latex expand-file-name get-buffer-create generate-new-buffer-name " *temp file*" funcall make-byte-code 0 "\301\300!\205	 \302\300!\207" vconcat vector [buffer-name kill-buffer] 2 "\n\n(fn)" "\n\\begin{document}\n" "This is the master of LaTeX fragments.\n" "\n\\end{document}\n" nil latex-mode "\300\207" [] 1 buffer-file-name "\301\300M\207" [buffer-file-name] org-edit-latex-create-auto-file write-region] 13 (#$ . 4537) nil])
#@101 Update TeX-master file.

This function should be called whenever you change the latex
header.

(fn)
(defalias 'org-edit-latex-update-master #[0 "\301\302 )\207" [org-edit-latex-create-master overwrite org-edit-latex-create-master-maybe] 1 (#$ . 5528) nil])
#@53 Wrap latex fragment in a latex src block.

(fn ELE)
(defalias 'org-edit-latex--wrap-latex #[257 "\302\211;\203 \303\304#\266\202\202 \305A@\"\266\202\306\211;\203* \303\304#\266\202\2022 \305A@\"\266\202\307\211;\203C \303\304#\266\202\202K \305A@\"\266\202\310\211;\203\\ \303\304#\266\202\202d \305A@\"\266\202@\311=\203o \312\202\205 \212b\210\313\314!)\262)\203\204 \315\202\205 \316`\212\312=\203\302 b\210m\203\253 \304\232\203\253 \212\317 \210\320\313\314!)\262)\204\263 T[y\210\316\210\321c\210\211\262b\210\322c\210\202\350 \315=\203\331 Zb\210\323c\210b\210\324c\210\202\350 b\210\321c\210b\210\317 \210\322c\210)\211U\205\363 \211Tb\207" [org-edit-latex-inline-beg-regexp inhibit-changing-match-data :begin get-text-property 0 plist-get :end :post-blank :post-affiliated latex-environment environment t looking-at inline nil beginning-of-line "[ 	]*\\\\end{" "\n#+END_SRC" "#+BEGIN_SRC latex\n" "}" " src_latex{"] 10 (#$ . 5792)])
#@34 Unwrap latex fragment.

(fn ELE)
(defalias 'org-edit-latex--unwrap-latex #[257 "\301\211;\203 \302\303#\266\202\202 \304A@\"\266\202\305\211;\203* \302\303#\266\202\2022 \304A@\"\266\202\306\211;\203C \302\303#\266\202\202K \304A@\"\266\202\307\211;\203\\ \302\303#\266\202\202d \304A@\"\266\202\310\211;\203u \302\303#\266\202\202} \304A@\"\266\202@\211\311=\203\305 \212b\210m\203\255 \303\232\203\255 \212\312 \210\313\314\315!)\262)\203\255 \316 \317 |\210\202\271 T[y\210\316 \317 T|\210b\210\317 T|)\202\343 \211\320=\203\342 \212\321\322#b\210\323\322!\210Sb\210\323\324!)\202\343 \325\207" [inhibit-changing-match-data :language get-text-property 0 plist-get :begin :end :post-affiliated :post-blank src-block beginning-of-line "#\\+end_src" t looking-at point-at-bol point-at-eol inline-src-block - 1 delete-char 11 nil] 11 (#$ . 6787)])
#@84 Unwrap latex fragment only if it meets certain predicates.

(fn OLDFUN &rest ARGS)
(defalias 'org-edit-latex--unwrap-maybe #[385 "\304\300!\203$ \212\305!q\210\211b\210\306 @)\307=\262\203$ \310 \210)\202'  \210\n\2059 \311>\2059 \312\306 !\210\313\211\207" [org-src--beg-marker org-src--remote org-edit-latex-mode org-edit-latex--before-type boundp marker-buffer org-element-context inline-src-block t (latex-fragment latex-environment) org-edit-latex--unwrap-latex nil] 5 (#$ . 7679)])
#@99 Wrap element at point if its type is latex-fragment or
latex-environment.

(fn OLDFUN &rest ARGS)
(defalias 'org-edit-latex--wrap-maybe #[385 "\203: \304 \211@\211\211\305=\204' \211\306=\2035 \212\307 \210\310\311\312!)\262)\2045 \313!\210\311\314\")\2029 \314\"\207\314\"\207" [org-edit-latex-mode org-edit-latex--before-type inhibit-changing-match-data org-src-preserve-indentation org-element-context latex-fragment latex-environment beginning-of-line "^#\\+" t looking-at org-edit-latex--wrap-latex apply] 7 (#$ . 8181)])
(provide 'org-edit-latex)
