;; See first command at the top of this file also
;; Just load packages, forget about the load-path
(require 'package)
(package-initialize)
;; Do not auto-load packages lists
(setq package-archives nil)
;; ;; Add some repositories
(add-to-list 'package-archives
             '("gnu" . "http://elpa.gnu.org/packages/"))
(add-to-list 'package-archives
             '("org" . "http://orgmode.org/elpa/") t)
(add-to-list 'package-archives
             '("melpa-stable" . "http://stable.melpa.org/packages/"))
;; (add-to-list 'package-archives
;;              '("melpa" . "https://melpa.org/packages/"))
;; (add-to-list 'package-archives
;;              '("milkbox" . "http://melpa.milkbox.net/packages/"))
;; http://orgmode.org/cgit.cgi/org-mode.git/plain/contrib/lisp/ox-taskjuggler.el

;; I don't know why it get's stuck if I move it down... debug
;; ****** Version Control ******
(use-package magit
  ;; :ensure magit
  :config
  (global-set-key "\C-xg" 'magit-status)
  (global-set-key [f7] 'find-file-in-repository))
;; ******************************

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(LaTeX-amsmath-label "eq:")
 '(LaTeX-clean-intermediate-suffixes
   (quote
    ("\\.aux" "\\.bbl" "\\.blg" "\\.brf" "\\.fot" "\\.glo" "\\.gls" "\\.idx" "\\.ilg" "\\.ind" "\\.lof" "\\.log" "\\.lot" "\\.nav" "\\.out" "\\.snm" "\\.toc" "\\.url" "\\.synctex\\.gz" "\\.bcf" "\\.run\\.xml" "\\.fls" "-blx\\.bib" "\\.mxp" "\\.mac" "\\.mxx" "-inc\\.eps" "\\.sta")))
 '(LaTeX-default-author "Edgar Rios")
 '(LaTeX-default-environment "align")
 '(LaTeX-indent-environment-list
   (quote
    (("multlined")
     ("multline")
     ("tabularx" LaTeX-indent-tabular)
     ("gnuplot")
     ("maximacmd")
     ("dgroup")
     ("dmath")
     ("verbatim" current-indentation)
     ("verbatim*" current-indentation)
     ("tabular" LaTeX-indent-tabular)
     ("tabular*" LaTeX-indent-tabular)
     ("align" LaTeX-indent-tabular)
     ("align*" LaTeX-indent-tabular)
     ("array" LaTeX-indent-tabular)
     ("eqnarray" LaTeX-indent-tabular)
     ("eqnarray*" LaTeX-indent-tabular)
     ("displaymath")
     ("equation")
     ("equation*")
     ("picture")
     ("tabbing")
     ("table")
     ("table*"))))
 '(LaTeX-label-alist
   (quote
    (("multline" . "")
     ("dgroup" . "LaTeX-eqnarray-label")
     ("dmath" . "LaTeX-eqnarray-label")
     ("figure" . LaTeX-figure-label)
     ("table" . LaTeX-table-label)
     ("figure*" . LaTeX-figure-label)
     ("table*" . LaTeX-table-label)
     ("equation" . LaTeX-equation-label)
     ("eqnarray" . LaTeX-eqnarray-label))))
 '(LaTeX-verbatim-environments (quote ("verbatim" "verbatim*" "maximacmd")))
 '(TeX-command-list
   (quote
    (("TeX" "%(PDF)%(tex) %(file-line-error) %(extraopts) %`%S%(PDFout)%(mode)%' %t" TeX-run-TeX nil
      (plain-tex-mode texinfo-mode ams-tex-mode)
      :help "Run plain TeX")
     ("LaTeX" "%`%l%(mode)%' %t" TeX-run-TeX nil
      (latex-mode doctex-mode)
      :help "Run LaTeX")
     ("Makeinfo" "makeinfo %(extraopts) %t" TeX-run-compile nil
      (texinfo-mode)
      :help "Run Makeinfo with Info output")
     ("Makeinfo HTML" "makeinfo %(extraopts) --html %t" TeX-run-compile nil
      (texinfo-mode)
      :help "Run Makeinfo with HTML output")
     ("AmSTeX" "amstex %(PDFout) %(extraopts) %`%S%(mode)%' %t" TeX-run-TeX nil
      (ams-tex-mode)
      :help "Run AMSTeX")
     ("ConTeXt" "%(cntxcom) --once --texutil %(extraopts) %(execopts)%t" TeX-run-TeX nil
      (context-mode)
      :help "Run ConTeXt once")
     ("ConTeXt Full" "%(cntxcom) %(extraopts) %(execopts)%t" TeX-run-TeX nil
      (context-mode)
      :help "Run ConTeXt until completion")
     ("BibTeX" "bibtex %s" TeX-run-BibTeX nil t :help "Run BibTeX")
     ("Biber" "biber %s" TeX-run-Biber nil t :help "Run Biber")
     ("View" "%V" TeX-run-discard-or-function t t :help "Run Viewer")
     ("Print" "%p" TeX-run-command t t :help "Print the file")
     ("Queue" "%q" TeX-run-background nil t :help "View the printer queue" :visible TeX-queue-command)
     ("File" "%(o?)dvips %d -o %f " TeX-run-dvips t t :help "Generate PostScript file")
     ("Dvips" "%(o?)dvips %d -o %f " TeX-run-dvips nil t :help "Convert DVI file to PostScript")
     ("Ps2pdf" "ps2pdf %f" TeX-run-ps2pdf nil t :help "Convert PostScript file to PDF")
     ("Index" "makeindex %s" TeX-run-index nil t :help "Run makeindex to create index file")
     ("Xindy" "texindy %s" TeX-run-command nil t :help "Run xindy to create index file")
     ("Check" "lacheck %s" TeX-run-compile nil
      (latex-mode)
      :help "Check LaTeX file for correctness")
     ("ChkTeX" "chktex -v6 %s" TeX-run-compile nil
      (latex-mode)
      :help "Check LaTeX file for common mistakes")
     ("Spell" "(TeX-ispell-document \"\")" TeX-run-function nil t :help "Spell-check the document")
     ("Clean" "TeX-clean" TeX-run-function nil t :help "Delete generated intermediate files")
     ("Clean All" "(TeX-clean t)" TeX-run-function nil t :help "Delete generated intermediate and output files")
     ("Limpia para VCS" "rm -fr *.{gnp,bbl,blg,mxx,mxp,aux,log,ps,eps,upa,upbout} *-inc{,.eps} *-eps-converted-to.pdf ../../Figures/*-eps-converted-to.pdf _region_* prv_*" TeX-run-command nil t)
     ("Other" "" TeX-run-command t t :help "Run an arbitrary command"))))
 '(TeX-debug-warnings t)
 '(TeX-expand-list (quote (("%raw" (find-file-other-window file)))))
 '(TeX-fold-unfold-around-mark nil)
 '(TeX-shell "/bin/bash")
 '(bmkp-last-as-first-bookmark-file "~/.emacs.d/bookmarks")
 '(font-latex-math-environments
   (quote
    ("align" "alignat" "dcases" "display" "displaymath" "dgroup" "dmath" "equation" "eqnarray" "flalign" "gather" "math" "multlined" "multline" "split" "xalignat" "xxalignat")))
 '(hl-sexp-background-color "#efebe9")
 '(package-selected-packages
   (quote
    (helm-gtags auctex-latexmk visual-fill-column nxml-mode helm-recoll auctex pdf-tools ggtags company-c-headers helm-projectile helm-swoop helm-tramp interleave helm-bibtex helm projectile use-package workgroups highlight-parentheses py-autopep8 elpy)))
 '(preview-default-preamble
   (quote
    ("\\RequirePackage["
     ("," . preview-default-option-list)
     "]{preview}[2004/11/05]" "\\PreviewEnvironment{dmath}" "\\PreviewEnvironment{dgroup}" "\\PreviewEnvironment{multlined}" "\\PreviewEnvironment{dcases}")))
 '(reftex-include-file-commands (quote ("include" "input" "subfile")))
 '(safe-local-variable-values
   (quote
    ((flycheck-clang-language-standard . c99)
     (ColumLimit . 50})
     (clang-format-style . {BasedOnStyle:)
     (clang-format-style . \.clang-format)
     (clang-format-style . \./\.clang-format)
     (clang-format-style . \.\./\.\./Supplement/clang-format)
     (flycheck-clang-language-standard . gnu99)
     (TeX-command-extra-options . "-shell-escape"))))
 '(spice-simulator-alist
   (quote
    (("GNUcap" "gnucap -b" ""
      ("\\s-*[eE]rror[	 ]+on[ 	]+line[	 ]+\\([0-9]+\\) +:.+" 0 1 nil
       (buffer-file-name))
      ("[cC]ircuit: \\(.*\\)$" 1))
     ("gnetlist" "gnetlist -g spice-sdb -O sort_mode -o" ""
      ("\\s-*[eE]rror[	 ]+on[ 	]+line[	 ]+\\([0-9]+\\) +:.+" 0 1 nil
       (buffer-file-name))
      ("[cC]ircuit: \\(.*\\)$" 1))
     ("Spice3" "spice3 -b" ""
      ("\\s-*Error[	 ]+on[ 	]+line[	 ]+\\([0-9]+\\) +:.+" 0 1 nil
       (buffer-file-name))
      ("Circuit: \\(.*\\)$" 1))
     ("Hspice" "hspice" ""
      ("\\s-*\\(..?error..?[: ]\\).+" 0 spice-linenum 1
       (buffer-file-name))
      ("[* ]* [iI]nput [fF]ile: +\\([^ 	]+\\).*$" 1))
     ("Eldo" "eldo -i" ""
      ("\\s-*\\(E[rR][rR][oO][rR] +[0-9]+:\\).*" 0 spice-linenum 1
       (buffer-file-name))
      ("Running \\(eldo\\).*$" 1))
     ("Spectre" "spectre" ""
      ("\\s-*\"\\([^
]+\\)\" +\\([0-9]+\\):.*" 1 2)
      ("" 0)))))
 '(spice-standard (quote (spice2g6 nil))))

;; Change colors only if I am running a GUI (not for terminal)
(if
     (display-graphic-p)
     (progn
      ;; Disable SIGSTSP signal to suspend Emacs
      ;; https://www.emacswiki.org/emacs/DotEmacsChallenge
      (when window-system
        (global-unset-key "\C-z")) ; iconify-or-deiconify-frame (C-x C-z)

      ;; Dark theme for company
      ;; https://company-mode.github.io/
      (require 'color)

      (let ((bg (face-attribute 'default :background)))
        (custom-set-faces
         `(company-tooltip ((t (:inherit default :background ,(color-lighten-name bg 2)))))
         `(company-scrollbar-bg ((t (:background ,(color-lighten-name bg 10)))))
         `(company-scrollbar-fg ((t (:background ,(color-lighten-name bg 5)))))
         `(company-tooltip-selection ((t (:inherit font-lock-function-name-face))))
         `(company-tooltip-annotation ((t (:inherit font-lock-comment-face))))
         `(company-tooltip-common ((t (:inherit
                                       font-lock-constant-face))))))
      (custom-set-faces
       ;; custom-set-faces was added by Custom.
       ;; If you edit it by hand, you could mess it up, so be careful.
       ;; Your init file should contain only one such instance.
       ;; If there is more than one, they won't work right.
       '(default ((t (:inherit nil :stipple nil :background "#2e3436" :foreground "light goldenrod" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 120 :width normal :foundry "PfEd" :family "DejaVu Sans Mono"))))
       '(company-scrollbar-bg ((t (:background "#454e51"))))
       '(company-scrollbar-fg ((t (:background "#394143"))))
       '(company-tooltip ((t (:inherit default :background "#32393b"))))
       '(company-tooltip-annotation ((t (:inherit font-lock-comment-face))))
       '(company-tooltip-common ((t (:inherit font-lock-constant-face))))
       '(company-tooltip-selection ((t (:inherit font-lock-function-name-face))))
       '(font-lock-string-face ((t (:foreground "orange3"))))
       '(org-footnote ((t (:foreground "thistle4" :underline t))))
       '(org-ref-ref-face ((t (:inherit org-link :foreground "coral")))))
      )

  ;; if we are running in terminal
  (setq frame-background-mode 'dark))

;; Change size and weight of the default font
(set-face-attribute 'default nil :height 120 :weight 'normal)

(define-key emacs-lisp-mode-map (kbd "C-c C-r") 'eval-region)

;; Use [Control] + [;] to comment or uncomment region
;; https://hristos.triantafillou.us/editing-python-emacs/
(defun toggle-comment ()
  "Toggle comments on the current line or highlighted region."
  (interactive)
  (if mark-active
      (let ((mark (mark))
            (point (point)))
        (if (> (mark) (point))
            (comment-or-uncomment-region
             point
             mark)
          (comment-or-uncomment-region
           mark
           point)))
    (comment-or-uncomment-region
     (line-beginning-position)
     (line-end-position))))
(global-set-key (kbd "C-;") 'toggle-comment)
;; -----------------------------
;;     ---- Pure Emacs ----
;; -----------------------------
;; ;; Start as a server if started as a GUI (otherwise the
;; ;; interface is messed up)
;; (if
;;     (display-graphic-p)
;;     (server-mode 1))

;; Tango-dark theme
(load-theme 'tango-dark)

;; Turn-on menus
(menu-bar-mode 1)
;; Turn-off tool-bar
(tool-bar-mode -1)
;; If the tool-bar is displayed, show icons only
(setq tool-bar-style 'image)

;; Hide startup screen and message
(setq inhibit-startup-screen t)
(setq inhibit-startup-message t)

;; *** Show lines only with goto-line ***
;; http://whattheemacsd.com//key-bindings.el-01.html
(global-set-key [remap goto-line] 'goto-line-with-feedback)
;;
(defun goto-line-with-feedback ()
  "Show line numbers temporarily, while prompting for the line number input"
  (interactive)
  (unwind-protect
      (progn
        (linum-mode 1)
        (goto-line (read-number "Goto line: ")))
    (linum-mode -1)))
;; ******************************

;; Overwrite selection
(delete-selection-mode t)

;; Hard-wrap to 60 characters (when requested)
;; https://www.emacswiki.org/emacs/FillColumnIndicator
(setq-default fill-column 60)

;; Enable M-l by default
(put 'downcase-region 'disabled nil)

;; (setq gc-cons-threshold 100000000)
(setq gc-cons-threshold 1000000) ; 1M

;; Add plugins directory to path
(add-to-list 'load-path "~/.emacs.d/plugins")

;; Use SUPER to move around windows
(windmove-default-keybindings 'super)

;; to answer y or n instead of yes or no
;; http://emacs-fu.blogspot.com/2009/04/dot-emacs-trickery.html
(defalias 'yes-or-no-p 'y-or-n-p)

;; Comment empty lines when commenting region
(setq comment-empty-lines t)

;; Default dictionary
(require 'ispell)
(setq ispell-dictionary "british-ise")

;; Default shell
(setq shell-file-name "/bin/bash")

;; use space to indent by default
;; https://www.emacswiki.org/emacs/DotEmacsChallenge
(setq-default indent-tabs-mode nil)

;; ;; Swap between ` and |
;; (define-key key-translation-map (kbd "`") (kbd "|"))
;; (define-key key-translation-map (kbd "|") (kbd "`"))

;; set appearance of a tab that is represented by 4 spaces
(setq-default tab-width 4)

;; Compilation
(global-set-key (kbd "<f5>") (lambda ()
                               (interactive)
                               (setq-local compilation-read-command nil)
                               (call-interactively 'compile)))

;; Duplicate line
;; http://www.emacswiki.org/emacs/CopyingWholeLines
(defun duplicate-current-line (&optional n)
  "duplicate current line, make more than 1 copy given a numeric argument"
  (interactive "p")
  (save-excursion
    (let ((nb (or n 1))
    	  (current-line (thing-at-point 'line)))
      ;; when on last line, insert a newline first
      (when (or (= 1 (forward-line 1)) (eq (point) (point-max)))
    	(insert "\n"))

      ;; now insert as many time as requested
      (while (> n 0)
    	(insert current-line)
    	(decf n)))))
;; (...cont)
(global-set-key (kbd "C-S-d") 'duplicate-current-line)

;; Use ssh instead of scp for remote files
;; FIX assignment to free variable
(setq tramp-default-method "ssh")

;; **** Define a root directory for my documents ****
(defcustom my-docs-dir             ; customisable variable
  (let ((my-dir "~/Documentos/"))  ; temp name: my-dir
    (if (file-exists-p my-dir)     ; conditional
        my-dir                     ; yes output (local name)
      (                            ; alternative
       (let ((my-dir "~/Documentos/"))
         (if (file-exists-p my-dir)
             my-dir)))                   ; ---alt ends
       ))
  "My main documents directory"         ; documentation
  :group 'my-custom-vars
  )

(defcustom main-research-dir
  (concat my-docs-dir "Research/Meniscus/")
  "Path to my main research at the moment"
  :group 'my-custom-vars)
;; **********

;;********** Highlight current word or symbol **********
;; http://nschum.de/src/emacs/highlight-symbol/
;; I don't always use it
;; (require 'highlight-symbol)
(global-set-key [(control f3)] 'highlight-symbol-at-point)
(global-set-key [f3] 'highlight-symbol-next)
(global-set-key [(shift f3)] 'highlight-symbol-prev)
(global-set-key [(meta f3)] 'highlight-symbol-query-replace)
;;******************************

;; ********** GDB **********
;; FIX assignment to free variable
(setq
 ;; use gdb-many-windows by default
 gdb-many-windows t

 ;; Non-nil means display source file containing the main routine at startup
 gdb-show-main t
 )
;; ******************************

;; Wrap selected text between parentheses or backets
(electric-pair-mode t)
;; (require 'autopair)

;; ;; Enable folding mode for hooks
;; (autoload 'folding-mode          "folding" "Folding mode" t)
;; (autoload 'turn-off-folding-mode "folding" "Folding mode" t)
;; (autoload 'turn-on-folding-mode  "folding" "Folding mode" t)

;; File name on title bar
;; http://www.aaronbedra.com/emacs.d/
(when window-system
  (setq frame-title-format '(buffer-file-name "%f" ("%b"))))

;; Delete spaces at the end of line before saving
;; http://stackoverflow.com/questions/7746965/how-do-you-delete-trailng-white-space-in-emacs
(add-hook 'before-save-hook
          'delete-trailing-whitespace)
;; Make sure that the file has a newline at the end
;; https://www.emacswiki.org/emacs/DotEmacsChallenge
(setq require-final-newline 't)

;; Always indent using spaces, never tabs
;; https://www.emacswiki.org/emacs/DotEmacsChallenge
(setq-default indent-tabs-mode nil)

;; *** Reload dir-locals variables ***
;; https://emacs.stackexchange.com/a/13096
(defun reload-dir-locals-for-current-buffer ()
  "reload dir locals for the current buffer"
  (interactive)
  (let ((enable-local-variables :all))
    (hack-dir-local-variables-non-file-buffer)))
(defun reload-dir-locals-for-all-buffer-in-this-directory ()
  "For every buffer with the same `default-directory` as the
current buffer's, reload dir-locals."
  (interactive)
  (let ((dir default-directory))
    (dolist (buffer (buffer-list))
      (with-current-buffer buffer
        (when (equal default-directory dir))
        (reload-dir-locals-for-current-buffer)))))
;; ******************************

;; Use ibuffer instead of list-buffers
(with-eval-after-load "ibuffer" (global-set-key (kbd "C-x C-b") 'ibuffer))

(require 'use-package)
;; ******** Python ********
;; Set Python3 for python command
;; elpy (need to put it on top, because it brakes helm and other stuff)
;; https://realpython.com/blog/python/emacs-the-best-python-editor/
(use-package elpy
;; :ensure elpy
  :config
  (elpy-enable)
  ;; For elpy
  (setq elpy-rpc-python-command "python3")
  ;; For interactive shell
  (setq python-shell-interpreter "python3")
  (use-package py-autopep8
;; :ensure py-autopep8
	       :config
	       (add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)))
;; ******************************

;; ********** Show matching parenthesis **********
;; http://www.emacswiki.org/emacs/ShowParenMode
(load-library "paren")
(show-paren-mode t)
(use-package highlight-parentheses
;; :ensure highlight-parentheses
  :config
  (global-highlight-parentheses-mode 1)
  (setq show-paren-mode t)
  ;; Colors
  (setq hl-paren-colors '("tomato" "slate blue"
                        "salmon" "DarkOrange3")))
;; ******************************

;; ********** Restore and undo frame configurations
(when (fboundp 'winner-mode)
  (winner-mode 1))
;; ;; https://github.com/tlh/workgroups.el
;; (add-to-list 'load-path "~/.emacs.d/plugins/workgroups/")
(use-package workgroups
;; :ensure workgroups
  :config
  ;; Deactivate the morphing effect
  (setq wg-morph-on nil)
  ;; Set keyboard shortcurt for mode
  (setq wg-prefix-key (kbd "C-c w"))
  ;; Disable morphing
  (setq wg-morph-on nil)
  ;; Auto-load configuration
  (if (file-exists-p "~/.emacs.d/workgroup.ini")
      (wg-load "~/.emacs.d/workgroup.ini"))
  ;; Activate (after all customisations)
  (workgroups-mode 1))

;; ;; Restore windows through sessions
;; ;; (add-to-list 'load-path "~/.emacs.d/elpa/bookmark+-20141205.319/")
;; (use-package bookmark+
;; ;; :ensure bookmark+
;;   :config
;;   (setq bmkp-last-as-first-bookmark-file "~/.emacs.d/bookmarks"))

;; Auto-compile .emacs
(defun autocompile nil
  "compile itself if ~/.emacs"
  (interactive)
  (require 'bytecomp)
  (if (string= (buffer-file-name) (expand-file-name (concat default-directory ".emacs")))
      (progn
        (byte-compile-file (buffer-file-name)))))
                                        ; (cont...)
(add-hook 'after-save-hook 'autocompile)

(use-package desktop
;; :ensure desktop
  :config
  ;; Load 5 files at a time
  (setq desktop-restore-eager 5)
  ;; What to clear across secions
  (setq desktop-globals-to-clear
        (quote
         (kill-ring kill-ring-yank-pointer search-ring-yank-pointer regexp-search-ring-yank-pointer)))
  ;; (setq desktop-path
  ;;       (quote
  ;;        ("~/.emacs.d/" "~/Documentos/Research/Bicycles/Outcome/Reports/LastYearPlan/" "~/Documentos/Research/Bicycles/Outcome/Reports/Methods/" "~/Documentos/Research/Bicycles/Outcome/Code/DynamicAnalysis")))
  ;; Save session only if a file for it exists
  (setq desktop-save (quote if-exists))
  ;; Activate session save
  (setq desktop-save-mode nil)
  ;; Allow to save history ??
  (setq savehist-mode t))
;; ******************************

;; ************ Dired ******************
;; Improve Dired
(use-package dired-details
;; :ensure dired-details
  :config
  ;; Allow to hide details in dired
  (dired-details-install)

  (setq dired-listing-switches "-alth")

  (put 'dired-find-alternate-file 'disabled nil)
  ;; Open directories in the same window using Dired we want dired not
  ;; not make always a new buffer if visiting a directory but using only
  ;; one dired buffer for all directories.
  (defadvice dired-advertised-find-file (around dired-subst-directory activate)
    "Replace current buffer if file is a directory."
    (interactive)
    (let ((orig (current-buffer))
          (filename (dired-get-filename)))
      ad-do-it
      (when (and (file-directory-p filename)
                 (not (eq (current-buffer) orig)))
        (kill-buffer orig))))
                                        ; (...cont)
  (eval-after-load "dired"
    ;; don't remove `other-window', the caller expects it to be there
    '(defun dired-up-directory (&optional other-window)
       "Run Dired on parent directory of current directory."
       (interactive "P")
       (let* ((dir (dired-current-directory))
              (orig (current-buffer))
              (up (file-name-directory (directory-file-name dir))))
         (or (dired-goto-file (directory-file-name dir))
             ;; Only try dired-goto-subdir if buffer has more than one dir.
             (and (cdr dired-subdir-alist)
                  (dired-goto-subdir up))
             (progn
               (kill-buffer orig)
               (dired up)
               (dired-goto-file dir)))))))
;; TODO close previous directory
;; ******************************

;; ***** Rectangular selections *****
;; http://emacs-fu.blogspot.com/2008_12_01_archive.html
(require 'rect-mark)
;; ;; Original key strokes
;; (global-set-key (kbd "C-x r C-SPC") 'rm-set-mark)
;; (global-set-key (kbd "C-x r C-x")   'rm-exchange-point-and-mark)
;; (global-set-key (kbd "C-x r C-w")   'rm-kill-region)
;; (global-set-key (kbd "C-x r M-w")   'rm-kill-ring-save)
;; (global-set-key (kbd "C-x r <down-mouse-1>") 'rm-mouse-drag-region)
(global-set-key (kbd "C-x r C-SPC") 'rm-set-mark)
(global-set-key (kbd "C-w")
                '(lambda(b e) (interactive "r")
                   (if rm-mark-active
                       (rm-kill-region b e)
                     (kill-region b e))))
;;
(global-set-key (kbd "M-w")
                '(lambda(b e) (interactive "r")
                   (if rm-mark-active
                       (rm-kill-ring-save b e)
                     (kill-ring-save b e))))
;;
(global-set-key (kbd "C-x C-x")
                '(lambda(&optional p) (interactive "p")
                   (if rm-mark-active
                       (rm-exchange-point-and-mark p)
                     (exchange-point-and-mark p))))
;; *********************************

;; ********** Yasnippets **********
(use-package yasnippet
;; :ensure yasnippet
  :config
  (yas-global-mode 1)
  ;; allow to insert a snippet within a snippet
  (setq yas-triggers-in-field t)
  ;; Change trigger key
  (define-key yas-minor-mode-map (kbd "<tab>") nil)
  (define-key yas-minor-mode-map (kbd "TAB") nil)
  (define-key yas-minor-mode-map (kbd "C-i") nil)
  (define-key yas-minor-mode-map (kbd "C-S-i") 'yas-expand))
;; (define-key yas-minor-mode-map (kbd "<C-tab>") 'yas-expand)
;; ******************************

;; ********** Auto-complete with company **********
(use-package company
;; :ensure company
  :init
  (global-company-mode)
  (delete 'company-semantic company-backends)
  (add-to-list 'company-backends 'company-c-headers)
  ;; ;; Company interferes in between Maxima and Python3
  ;; (setq company-global-modes '(not python-mode cython-mode sage-mode))
  :config
  (progn
    ;; (setq company-tooltip-limit 20) ; bigger popup window
    (setq company-idle-delay .3)    ; decrease delay before autocompletion popup shows
    (setq company-echo-delay 0)     ; remove annoying blinking
    (setq company-begin-commands '(self-insert-command)) ; start autocompletion only after typing
    ))

;; ;; https://emacs.stackexchange.com/a/12457
;; ;; TAB to trigger company-mode on demand
;; (define-key company-mode-map [remap indent-for-tab-command]
;;   'company-indent-for-tab-command)
;; TAB first tries to indent the current line, and if the line
;; was already indented, then try to complete the thing at point.
(setq tab-always-indent 'complete)

;; (defvar completion-at-point-functions-saved nil)
;;
;; (defun company-indent-for-tab-command (&optional arg)
;;   (interactive "P")
;;   (let ((completion-at-point-functions-saved completion-at-point-functions)
;;         (completion-at-point-functions '(company-complete-common-wrapper)))
;;     (indent-for-tab-command arg)))
;;
;; (defun company-complete-common-wrapper ()
;;   (let ((completion-at-point-functions completion-at-point-functions-saved))
;;     (company-complete-common)))
;; --end

;; ;; Interferes with Python
;; (add-hook 'flycheck-mode-hook
;;           (lambda ()
;;             (when (display-graphic-p)
;;               (setq-local flycheck-indication-mode nil))))
;; ******************************

;; Code project management
;; Package: projectile
(use-package projectile
;; :ensure projectile
  :init
  (projectile-mode)
  (setq projectile-enable-caching t))

;; ********** Maxima **********
;; imaxima
;; (autoload 'imath "imath" "Interactive Math mode" t)
;; http://www.emacswiki.org/emacs/MaximaMode
(add-to-list 'load-path "/usr/share/emacs/site-lisp/maxima/")
(autoload 'imaxima "imaxima" "Frontend for Maxima with Image support" t)
(autoload 'maxima "maxima" "Maxima interaction" t)
(autoload 'maxima-mode "maxima" "Maxima mode" t)
(autoload 'maxima-minor-mode "maxima" "Minor mode for Maxima" t)
(autoload 'imath-mode "imath" "Imath mode for math formula input" t)
;; Interferes with EMaxima... and what is the point?
;; (setq imaxima-use-maxima-mode-flag t)
(add-to-list 'auto-mode-alist '("\\.ma[cx]" . maxima-mode))
;; To make sure that emaxima.el is loaded when necessary
;; EMaximaIntro.ps
(autoload 'emaxima-mode "emaxima" "EMaxima" t)
;; Disallow abbreviations (I never use them) to speed up package assembly
(setq emaxima-abbreviations-allowed nil)
;; Load my custom mactex-utilities.lisp
(setq maxima-args '(;; "--quiet"           ; Suppress Maxima start-up message.
                    "--init-lisp"      ; Load Lisp initialization file at startup.
                    "~/.emacs.d/plugins/mactex-utilities.lisp"))

;; (add-hook 'imaxima-startup-hook
;;           (lambda ()
;;             (let ((b (get-buffer "*imaxima*"))
;;                   (p (get-process "imaxima")))
;;               (if (and b p)
;;                   (apply comint-input-sender
;;                          (list (get-process "imaxima")
;;                                "load(\"/home/edgar/.emacs.d/plugins/mactex-utilities\");"))))))
;; Change size of font in imaxima
(setq imaxima-fnt-size "Large")

;; Force org-mode to read my mactex-utilities file
(setq org-babel-maxima-command
      "maxima -p ~/.emacs.d/plugins/mactex-utilities.lisp")
;; ******************************

;; ********** Yasnippets **********
(use-package yasnippet
;; :ensure yasnippet
  :config
  (yas-global-mode 1)
  ;; allow to insert a snippet within a snippet
  (setq yas-triggers-in-field t)
  ;; Change trigger key
  (define-key yas-minor-mode-map (kbd "<tab>") nil)
  (define-key yas-minor-mode-map (kbd "TAB") nil)
  (define-key yas-minor-mode-map (kbd "C-i") nil)
  (define-key yas-minor-mode-map (kbd "C-S-i") 'yas-expand)
  ;; (define-key yas-minor-mode-map (kbd "<C-tab>") 'yas-expand)
  )
;; ******************************

;; ******** Undo-tree ***********
;; Improve undo
;; Package: undo-tree
;; GROUP: Editing -> Undo -> Undo Tree
(use-package undo-tree)
(global-undo-tree-mode 1)
;; ******************************

;; ********** anzu **********
;; Anzu: visual help when replacing
;; GROUP: Editing -> Matching -> Isearch -> Anzu
(use-package anzu
;; :ensure anzu
  :init
  (global-anzu-mode)
  (global-set-key (kbd "M-%") 'anzu-query-replace)
  (global-set-key (kbd "C-M-%") 'anzu-query-replace-regexp))
;; ******************************

;; ********** LaTeX **********
(use-package reftex)
;; EmacsWiki AUCTeX
;; FIX assignment to free variable
(setq TeX-auto-save t ; Enable parse on save
      TeX-parse-self t ; Enable parse on load
      )
(setq-default TeX-master nil)

(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)

(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(setq reftex-plug-into-AUCTeX t)

;; ;; EmacsWiki LaTeX
;; (add-hook 'reftex-load-hook 'imenu-add-menubar-index)
;; (add-hook 'reftex-mode-hook 'imenu-add-menubar-index)

;; Fold code C-c C-o C-o
(add-hook 'LaTeX-mode-hook (lambda ()
                             (TeX-fold-mode 1)))
;; (add-hook 'LaTeX-mode-hook 'folding-mode)

;; To compile documents to PDF (pdflatex)
(setq TeX-PDF-mode 1)

;; Load some defaults on LaTeX
(add-hook 'LaTeX-mode-hook
          (lambda ()
            ;; ;; Hard break long lines
            ;; (auto-fill-mode)
            ;; Math symbols menu and shortcuts
            (LaTeX-math-mode)
            ))

;; Keep old preview visible when editing
(add-hook 'LaTeX-mode-hook
          (lambda ()
  (defadvice preview-inactive-string (around preview-show-old nil activate)
    "Show old preview when editing source code."
    (when (overlay-get ov 'preview-state)
      (let ((preview-icon (or (car-safe (overlay-get ov 'preview-image)) preview-icon)))
        (overlay-put ov 'preview-old-image preview-icon)
        ad-do-it
        )))
  ;;
  (defadvice preview-disabled-string (around preview-show-old nil activate)
    "Show old preview when editing source code."
    (when (overlay-get ov 'preview-state)
      (let ((preview-icon (or (overlay-get ov 'preview-old-image) preview-icon)))
        ad-do-it
        )))))

;; Load yasnippet with LaTeX
(add-hook 'LaTeX-mode-hook 'yas-minor-mode)

;; Change scale of preview
(set-default 'preview-scale-function 1.5)
;; ****************************************

;; ********** Helm **********
;; Make the buffer list look better
;; https://tuhdo.github.io/helm-intro.html
;; (require 'setup-helm)
(use-package helm
;; :ensure helm
  :config
  (progn
    (require 'helm-config)
    (require 'helm-grep)
    ;; To fix error at compile:
    ;; Error (bytecomp): Forgot to expand macro with-helm-buffer in
    ;; (with-helm-buffer helm-echo-input-in-header-line)
    (if (version< "26.0.50" emacs-version)
        (eval-when-compile (require 'helm-lib)))

    (defun helm-hide-minibuffer-maybe ()
      (when (with-helm-buffer helm-echo-input-in-header-line)
        (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
          (overlay-put ov 'window (selected-window))
          (overlay-put ov 'face (let ((bg-color (face-background 'default nil)))
                                  `(:background ,bg-color :foreground ,bg-color)))
          (setq-local cursor-type nil))))

    (add-hook 'helm-minibuffer-set-up-hook 'helm-hide-minibuffer-maybe)
    ;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
    ;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
    ;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.
    (global-set-key (kbd "C-c C-h") 'helm-command-prefix)
    (global-unset-key (kbd "C-x c"))

    (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebihnd tab to do persistent action
    (define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
    (define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

    (define-key helm-grep-mode-map (kbd "<return>")  'helm-grep-mode-jump-other-window)
    (define-key helm-grep-mode-map (kbd "n")  'helm-grep-mode-jump-other-window-forward)
    (define-key helm-grep-mode-map (kbd "p")  'helm-grep-mode-jump-other-window-backward)

    (when (executable-find "curl")
      (setq helm-net-prefer-curl t))

    (setq helm-net-prefer-curl t
          helm-scroll-amount 4 ; scroll 4 lines other window using M-<next>/M-<prior>
          ;; helm-quick-update t ; do not display invisible candidates
          helm-ff-search-library-in-sexp t ; search for library in `require' and `declare-function' sexp.

          ;; you can customize helm-do-grep to execute ack-grep
          ;; helm-grep-default-command "ack-grep -Hn --smart-case --no-group --no-color %e %p %f"
          ;; helm-grep-default-recurse-command "ack-grep -H --smart-case --no-group --no-color %e %p %f"
          helm-split-window-in-side-p t ;; open helm buffer inside current window, not occupy whole other window

          helm-echo-input-in-header-line t

          ;; helm-candidate-number-limit 500 ; limit the number of displayed canidates
          helm-ff-file-name-history-use-recentf t
          helm-move-to-line-cycle-in-source t ; move to end or beginning of source when reaching top or bottom of source.
          helm-buffer-skip-remote-checking t

          helm-mode-fuzzy-match t

          helm-buffers-fuzzy-matching t ; fuzzy matching buffer names when non-nil
                                        ; useful in helm-mini that lists buffers
          helm-org-headings-fontify t
          ;; helm-find-files-sort-directories t
          ;; ido-use-virtual-buffers t
          helm-semantic-fuzzy-match t
          helm-M-x-fuzzy-match t
          helm-imenu-fuzzy-match t
          helm-lisp-fuzzy-completion t
          ;; helm-apropos-fuzzy-match t
          helm-buffer-skip-remote-checking t
          helm-locate-fuzzy-match t
          helm-display-header-line nil)

    (add-to-list 'helm-sources-using-default-as-input 'helm-source-man-pages)

    (global-set-key (kbd "M-x") 'helm-M-x)
    (global-set-key (kbd "M-y") 'helm-show-kill-ring)
    (global-set-key (kbd "C-x b") 'helm-buffers-list)
    (global-set-key (kbd "C-x C-f") 'helm-find-files)
    (global-set-key (kbd "C-c r") 'helm-recentf)
    (global-set-key (kbd "C-h SPC") 'helm-all-mark-rings)
    (global-set-key (kbd "C-c h o") 'helm-occur)

    (global-set-key (kbd "C-c h w") 'helm-wikipedia-suggest)
    (global-set-key (kbd "C-c h g") 'helm-google-suggest)

    (global-set-key (kbd "C-c h x") 'helm-register)
    ;; (global-set-key (kbd "C-x r j") 'jump-to-register)

    (define-key 'help-command (kbd "C-f") 'helm-apropos)
    (define-key 'help-command (kbd "r") 'helm-info-emacs)
    (define-key 'help-command (kbd "C-l") 'helm-locate-library)

    ;; use helm to list eshell history
    ;; https://www.reddit.com/r/emacs/comments/6y3q4k/yes_eshell_is_my_main_shell/dmn6z7u/
    (defun eshell-set-keys ()
      (define-key eshell-mode-map (kbd "M-l")  'helm-eshell-history))
    (add-hook 'eshell-first-time-mode-hook 'eshell-set-keys)
    (add-hook 'eshell-mode-hook
              #'(lambda ()
                  (define-key eshell-mode-map (kbd "M-l")  'helm-eshell-history)))

    ;;; Save current position to mark ring
    (add-hook 'helm-goto-line-before-hook 'helm-save-current-pos-to-mark-ring)

    ;; show minibuffer history with Helm
    (define-key minibuffer-local-map (kbd "M-p") 'helm-minibuffer-history)
    (define-key minibuffer-local-map (kbd "M-n") 'helm-minibuffer-history)

    (define-key global-map [remap find-tag] 'helm-etags-select)

    ;; (define-key global-map [remap list-buffers] 'helm-buffers-list)

    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; PACKAGE: helm-swoop                ;;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;; Locate the helm-swoop folder to your path
    (use-package helm-swoop
;; :ensure helm-swoop
      ;; Set keys
      ;; ;; This does not work
      ;; :bind (("C-c h o" . helm-swoop)
      ;;        ("C-c s" . helm-multi-swoop-all))
      :commands
      (helm-swoop helm-multi-swoop-all)
      :config
      (bind-key "C-c h s" 'helm-swoop)
      (bind-key "C-c h S" 'helm-multi-swoop-all)
      ;; When doing isearch, hand the word over to helm-swoop
      (define-key isearch-mode-map (kbd "M-i") 'helm-swoop-from-isearch)

      ;; From helm-swoop to helm-multi-swoop-all
      (define-key helm-swoop-map (kbd "M-i") 'helm-multi-swoop-all-from-helm-swoop)

      ;; Save buffer when helm-multi-swoop-edit complete
      (setq helm-multi-swoop-edit-save t)

      ;; If this value is t, split window inside the current window
      (setq helm-swoop-split-with-multiple-windows t)

      ;; Split direcion. 'split-window-vertically or 'split-window-horizontally
      (setq helm-swoop-split-direction 'split-window-vertically)

      ;; If nil, you can slightly boost invoke speed in exchange for text color
      (setq helm-swoop-speed-or-color t))

    (helm-mode 1)

    (use-package helm-projectile
;; :ensure helm-projectile
      :init
      (helm-projectile-on)
      (setq projectile-completion-system 'helm)
      (setq projectile-indexing-method 'alien))))
;; ******************************

;; *************** PDF files ***************
;; ;; Allow to disable linum-mode (for PDF; doc-view-mode;
;; ;; Major modes where linum is disabled)
;; (use-package linum-off)
;; Set auto-bounding-box
(add-hook 'doc-view-mode-hook 'doc-view-set-slice-from-bounding-box)

;; ;; Keybinding for goto-page
;; (add-hook 'doc-view-mode-hook
;;           (lambda ()
;;             (local-set-key "\C-cg" 'doc-view-goto-page)))
;; ;; ;; It would be nice if I could have both pages scrolling on the same
;; ;; ;; window... (not possible with this)
;; ;; '(doc-view-continuous t)

;; Replace DocView with PDF Tools
;; Load with use-package
;; https://www.reddit.com/r/emacs/comments/44yxsq/pdf_tools/d0dqhou/
;; http://tech.memoryimprintstudio.com/pdf-annotation-related-tools/
(use-package pdf-tools
;; :ensure pdf-tools
  :config

  (pdf-tools-install)

  ;; Use the "traditional" Emacs' key to go to the beginning and end
  (define-key pdf-view-mode-map (kbd "C-a") 'image-bol)
  (define-key pdf-view-mode-map (kbd "C-e") 'image-eol)

  ;; Store the current page as a link for Org-mode with C-c l
  (define-key pdf-view-mode-map (kbd "C-c l") 'org-store-link)

  (setq
   ;; Custom colors for midnight mode
   pdf-view-midnight-colors '("light goldenrod" . "#2e3436")
   ;; Do not skip page when getting to the end
   pdf-view-continuous nil)
  (add-hook 'pdf-view-mode-hook 'pdf-view-midnight-minor-mode)

  ;; AUCTeX + pdf-tools
  ;; https://emacs.stackexchange.com/a/21764
  ;; http://pastebin.com/v9bPHfVe
  ;; to use pdfview with auctex
  (setq TeX-view-program-selection '((output-pdf "PDF Tools"))
        TeX-view-program-list '(("PDF Tools" TeX-pdf-tools-sync-view))
        ;; TeX-source-correlate-start-server t ;; not sure if needed

        ;; https://tex.stackexchange.com/a/211742
        TeX-source-correlate-method 'synctex
        TeX-source-correlate-mode t
        )

  ;; to have the buffer refresh after compilation
  (add-hook 'TeX-after-compilation-finished-functions
            #'TeX-revert-document-buffer)
  ;; System-wide open PDF with PDF Tools
  ;; https://emacs.stackexchange.com/a/24502
  ;; If you want your pdfs opened in the same instance of Emacs, you need
  ;; to have it running in daemon mode (i.e., start it with emacs
  ;; --daemon), or call (server-start) in your init so that emacsclient
  ;; will work.

  ;; https://github.com/machc/pdf-tools-org
  ;; from https://matt.hackinghistory.ca/2015/11/11/note-taking-with-pdf-tools/
  ;; (add-to-list 'load-path "~/.emacs.d/plugins/pdf-tools-org")
  (use-package pdf-tools-org
    :load-path "~/.emacs.d/plugins/pdf-tools-org")
  )
;; ******************************

;; ******** (Ba)sh ********
;; Set tab width = 4, and allow for hard word-wrap
(add-hook 'sh-mode-hook (lambda ()
                          (setq tab-width 4) ; set tab width to 4
                          ; (auto-fill-mode) ; hard break long lines
                          (yas-minor-mode) ; activate yasnippet
                          ))

(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-language-environment "UTF-8")
(prefer-coding-system 'utf-8)
;; ******************************

;; ********** Spice **********
(add-to-list 'load-path "~/.emacs.d/plugins/spice-mode/")
(autoload 'spice-mode "spice-mode" "Spice/Layla Editing Mode" t)
(setq auto-mode-alist
      (append (list (cons "\\.sp$" 'spice-mode)
                    (cons "\\.cir$" 'spice-mode)
                    (cons "\\.ckt$" 'spice-mode)
                    (cons "\\.mod$" 'spice-mode)
                    (cons "\\.cdl$" 'spice-mode)
                    (cons "\\.net$" 'spice-mode)
                    (cons "\\.chi$" 'spice-mode) ;eldo outpt
                    (cons "\\.inp$" 'spice-mode))
              auto-mode-alist))
;; !! Not working
;; (add-to-list 'load-path "~/.emacs.d/plugins/ob-spice/")
;; (autoload 'ob-spice "ob-spice" "Spice for babel" t)
;; ******************************

;; ********** Getpot **********
;; ;; Conditional to check if the getpot-mode file exists
;; (if
;;     (file-exists-p
;;      (expand-file-name
;;       "~/.emacs.d/plugins/emacs-getpot-mode.el"))
;;     ;; If it exists, load it
;;     (load-file "~/.emacs.d/plugins/emacs-getpot-mode.el"))

;; Automatically load conf-mode for MOOSE input files (getpot is ugly)
(define-derived-mode getpot-mode conf-unix-mode "MOOSE input"
  "Input deck for MOOSE mode."
  (make-local-variable 'tab-width))
(autoload 'getpot-mode "GetPot" "Input deck for MOOSE mode." t)
(add-to-list 'auto-mode-alist '("\\.i$" . getpot-mode))
;; ****************************

;; *************** Org-mode ***************
(setq
 ;; Set Monday as the first day of the week
 calendar-week-start-day 1

 ;; ;; Force dates in English
 ;; calendar-day-name-array
 ;; ["Sunday" "Monday" "Tuesday" "Wednesday" "Thursday" "Friday"
 ;;  "Saturday"]

 ;; calendar-month-name-array
 ;; ["January" "February" "March" "April" "May" "June" "July"
 ;;  "August" "September" "October" "November" "December"]
 )

(add-hook 'calendar-load-hook
          (lambda ()
            (calendar-set-date-style 'iso)))

;; Load org-agenda at start-up
;; https://stackoverflow.com/a/2010955
(add-hook 'after-init-hook 'org-agenda-list)

(use-package org
;;   :ensure t

  ;; Doesn't work
  ;; (setq org-odt-category-map-alist
  ;;       '(("__Table__" "Table" "value" "Table" org-odt--enumerable-p)
  ;;         ("__Figure__" "Illustration" "value" "Figure" org-odt--enumerable-image-p)
  ;;         ("__MathFormula__" "Text" "math-formula" "Equation" org-odt--enumerable-formula-p)
  ;;         ("__DvipngImage__" "Equation" "value" "Equation" org-odt--enumerable-latex-image-p)
  ;;         ("__Listing__" "Block" "value" "Listing" org-odt--enumerable-p)))

  :config
  ;; Establishing your own keybindings for org-mode.
  ;; Variable org-mode-map is available only after org.el or org.elc is loaded.
  (define-key org-mode-map (kbd "C-c C-u L") 'emaxima-replace-line-with-tex)
  (define-key org-mode-map (kbd "C-c C-u m") 'maxima-send-region)
  (define-key org-mode-map (kbd "C-c C-u RET") 'outline-up-heading)
  (define-key org-mode-map (kbd "C-c l") 'org-store-link)
  (define-key org-mode-map (kbd "C-c a") 'org-agenda)
  (define-key org-mode-map (kbd "C-c c") 'org-capture)
  (define-key org-mode-map (kbd "C-c b") 'org-iswitchb)
  (define-key org-mode-map (kbd "C-c M-v") 'org-up-element)
  ;; ;; Wrap text
  ;; (add-hook 'org-mode-hook 'visual-fill-column-mode)

  ;; Add [timestamp] after going from TODO → DONE
  (setq org-log-done 'time)

  ;; Show images after block evaluation
  ;; https://github.com/erikriverson/org-mode-R-tutorial/blob/master/org-mode-R-tutorial.org
  (add-hook 'org-babel-after-execute-hook 'org-display-inline-images)
  (add-hook 'org-mode-hook 'org-display-inline-images)

  ;; Highlight LaTeX
  (setq org-highlight-latex-and-related '(latex))

  ;; Limit the backends to use
  (setq org-export-backends '(latex odt ascii html icalendar taskjuggler beamer md org))
  (org-babel-do-load-languages ; babel, for executing
   'org-babel-load-languages   ; code in org-mode.
   '((shell . t)
     (latex . t)
     (python . t)
     (maxima . t)
     (R . t)
     (C . t)
     (awk . t)
     (emacs-lisp . t)
     (gnuplot . t)
     (org . t)
     (octave . t)
     (ditaa . T)
     ;; (spice . t)
     ;; (dot .t)
     ))

  (;; Use LaTeXML to export to ODT formulas
   ;; (https://github.com/brucemiller/LaTeXML)
   ;; (set #+OPTIONS: LaTeX:t somewhere in your documents)
   setq org-latex-to-mathml-convert-command
        "latexmlmath \"%i\" --presentationmathml=%o")

  ;; Mark broken links, and keep exporting
  (setq org-export-with-broken-links "mark")

  ;; Python3
  ;; https://emacs.stackexchange.com/questions/28184/org-mode-how-can-i-point-to-python3-5-in-my-org-mode-doc
  (setq org-babel-python-command "python3")
  ;; -- end Python3

  ;; If I edit some hiden structure, show the structure and edit
  (setq org-catch-invisible-edits 'show)

  ;; Allow alphabetic lists
  ;; https://emacs.stackexchange.com/a/32743
  (setq org-list-allow-alphabetical t)

  ;; Activate coloring and tabbing in blocks
  ;; http://mescal.imag.fr/membres/arnaud.legrand/misc/init.php
  (setq org-src-fontify-natively t
        org-src-preserve-indentation nil)

  ;; Completion in blocks
  (setq org-src-tab-acts-natively nil) ;; you want this to have
  ;; ;; To hide the *,=, or / markers
  ;; (setq org-hide-emphasis-markers t) ;;
  ;; ;; \alpha, \to and others display as utf8
  ;; ;; http://orgmode.org/manual/Special-symbols.html
  ;; (setq org-pretty-entities t)       ;;

  ;; ***** LaTeX *****
  ;; ---- Previews
  '(org-preview-latex-process-alist
    (quote
     ((dvipng :programs
              ("lualatex" "dvipng")
              :description "dvi > png" :message "you need to install the programs: latex and dvipng." :image-input-type "dvi" :image-output-type "png" :image-size-adjust
              (1.0 . 1.0)
              :latex-compiler
              ("lualatex -output-format dvi -interaction nonstopmode -output-directory %o %f")
              :image-converter
              ("dvipng -fg %F -bg %B -D %D -T tight -o %O %f"))
      (dvisvgm :programs
               ("latex" "dvisvgm")
               :description "dvi > svg" :message "you need to install the programs: latex and dvisvgm." :use-xcolor t :image-input-type "xdv" :image-output-type "svg" :image-size-adjust
               (1.7 . 1.5)
               :latex-compiler
               ("xelatex -no-pdf -interaction nonstopmode -output-directory %o %f")
               :image-converter
               ("dvisvgm %f -n -b min -c %S -o %O"))
      (imagemagick :programs
                   ("latex" "convert")
                   :description "pdf > png" :message "you need to install the programs: latex and imagemagick." :use-xcolor t :image-input-type "pdf" :image-output-type "png" :image-size-adjust
                   (1.0 . 1.0)
                   :latex-compiler
                   ("xelatex -no-pdf -interaction nonstopmode -output-directory %o %f")
                   :image-converter
                   ("convert -density %D -trim -antialias %f -quality 100 %O")))))

  (setq org-preview-latex-default-process 'dvisvgm)
  (setq org-latex-inputenc-alist '(("utf8" . "utf8x")))
  (add-to-list 'org-latex-packages-alist '("" "xunicode"))
  ;; ----
  ;; Use different fonts
  ;; (Parabola removed fonts due to licenses)
  (setq org-latex-default-packages-alist
        '(("AUTO" "inputenc" t
           ("pdflatex"))
          ;; ("" "lmodern" t)              ; This
          ;; ("QX" "fontenc" t             ; and this
          ;;  ("pdflatex"))
          ("" "graphicx" t)
          ("" "grffile" t)
          ("" "longtable" nil)
          ("" "wrapfig" nil)
          ("" "rotating" nil)
          ("normalem" "ulem" t)
          ("" "amsmath" t)
          ("" "textcomp" t)
          ("" "amssymb" t)
          ("" "capt-of" nil)
          ("" "hyperref" nil)))

  ;; Change link colors when exporting with LaTeX
  ;; https://lists.gnu.org/archive/html/emacs-orgmode/2013-09/msg00927.html
  (setq org-latex-hyperref-template
        "\\hypersetup{
 pdfauthor={%a},
 pdftitle={%t},
 pdfkeywords={%k},
 pdfsubject={%d},
 pdfcreator={%c},
 pdflang={%L},
 %%
 %% Enable coloring links
 colorlinks=true,
 %% Change color of links
 linkcolor=[HTML]{2E4D2A}, %% Very dark green inner links
 citecolor=Blue,           %% Blue citations
 urlcolor=[HTML]{23294A},  %% Very dark blue external links
 filecolor=[HTML]{400940}, %% Very dark purple for file links
}
"
 ;; %% To enable autonum (see texdoc autonum; autonum.pdf)
 ;; hypertexnames=false}"
)

  (setq org-latex-packages-alist
        '(;; ("e" "esvect" t)
          ("" "adjustbox" t)
          ("usenames,dvipsnames,table" "xcolor" nil)
          ("" "multirow" nil)
          ("" "booktabs,array,tabularx" nil)
          ("" "mathtools" t)
          ("" "listings" t)
          ("
  % The type of reference is included in the hyperlink
  nameinlink
  %% The type of reference is not abbreviated (eq becomes equation)
  % noabbrev
" "cleveref" nil)
          ("
  % Link to the top of the image
  hypcap,
  % Centre last line of the caption only
  justification=centerlast,
  % Justify the text which is after the type of caption
  format=hang,
  % Make the font of label small and italics
  textfont={small},
  % Put the type of caption in versalitas
  labelfont={sc,small}
" "caption" nil)
          ("
  % Put the type of caption in small and slanted shape
  labelfont={footnotesize,sl},
  % Put the text of the caption in versalitas and really small
  textfont={scriptsize}
" "subcaption" nil)
          ;; ("" "autonum" t)
          ("" "pdfcomment" nil)
          ("" "stackengine" t)))

  ;; Add my symbols to the LaTeX header by default
  (setq org-format-latex-header
        (concat
         org-format-latex-header
         (if
             ;; Make sure that it exists
             (file-exists-p
              (expand-file-name
               "~/.emacs.d/plugins/symbols.tex"))
             ;; ... in which case add it to the header as
             ;; \input{}
             (concat
              "\\input{"
              (expand-file-name
               "~/.emacs.d/plugins/symbols.tex")
              "}")
           )))

  ;; Set booktabs and longtabu as default to format tables
  (setq org-latex-tables-booktabs t
        org-latex-default-table-environment "longtabu")

  ;; Colours of source code blocks in LaTeX
  (require 'ox-latex)
  ;; ;; Overide with minted (install pygmentize python-pygments)
  ;; ;; https://emacs.stackexchange.com/a/20841
  ;; (add-to-list 'org-latex-packages-alist '("" "minted"))
  ;; (setq org-latex-listings 'minted)

  (setq org-latex-listings 'listings)
  ;; Options for \lset (code blocks with listings package)
  (setq org-latex-listings-options
        '(
          ;; ("backgroundcolor" "{\\color[gray]{0.95}}")
          ("basicstyle" "\\footnotesize\\ttfamily")
          ("keywordstyle" "\\bfseries\\color[rgb]{0.133, 0.545, 0.133}") ;green
          ("commentstyle" "\\itshape\\color[rgb]{0.58,0,0.82}") ; purple
          ("identifierstyle" "\\color{blue}")
          ("stringstyle" "\\color[RGB]{214,132,25}")
          ("belowcaptionskip" "1\\baselineskip")
          ("breaklines" "true")
          ;; ("language" "C")
          ("showstringspaces" "false")
          ;; Frame
          ("frame" "L")
          ;; ("linewidth" "0.9\\paperwidth")
          ;; ("xleftmargin" "-0.1\\paperwidth")
          ("xleftmargin" "\\parindent")
          ;; Numbers
          ("numbers" "left")
          ("numberstyle" "\\tiny")
          ("stepnumber" "1")
          ("numbersep" "5pt")
          ("numberfirstline" "false")
          ;; Other comments
          ("morecomment" "[l][\\color{green}]{\\#}")
          ))

  ;; Scientific notation
  (setq org-latex-table-scientific-notation "$%s\\mathrm{E}^{%s}$")

  ;; Preview LaTeX in org-edit-buffer
  ;; https://github.com/et2010/org-edit-latex
  ;; https://www.reddit.com/r/emacs/comments/6i5g5l/latex_preview_in_org_source_block/
  (use-package org-edit-latex
    :config
    (add-hook 'org-mode-hook #'org-edit-latex-mode)
    )

  ;; (use-package org-edit-latex
  ;;   :load-path "~/.emacs.d/plugins/org-edit-latex"
  ;;   :config
  ;;   (setq org-edit-latex-create-master t)
  ;;   ;; (setq org-edit-latex-packages-alist
  ;;   ;;       '(("range-phrase=-,range-units=single" "siunitx")
  ;;   ;;         ("" "amsmath")
  ;;   ;;         ("" "amssymb")
  ;;   ;;         ("" "mathtool")
  ;;   ;;         ))
  ;;   )

  ;; ;; This creates trouble, because I cannot see if the process went well
  ;; ;; Run LaTeX in the background
  ;; ;; http://pragmaticemacs.com/emacs/speed-up-pdf-export-from-org-mode-with-latexmk/#comments
  ;; (setq org-export-in-background t)

  ;; Beamer (slides) export
  (use-package ox-beamer)
  ;; ;; allow multiple slides for a title
  ;; (setq org-beamer-frame-default-options "allowframebreaks")
  (setq org-src-tab-acts-natively t)

  ;; Add LaTeX Math menu to org-mode
  (require 'latex)
  (add-hook 'org-mode-hook 'latex-math-mode)
  ;; (add-hook 'org-load-hook 'latex-math-mode)
  (add-hook 'org-mode-hook
            (lambda ()
              ;; Activate yasnippet
              (yas-minor-mode)
              ;; Add the latex snippets
              (yas-activate-extra-mode 'latex-mode)
              ;; ;; Turn on auto-fill (hard text wrap)
              ;; ;; https://github.com/bixuanzju/emacs.d/blob/master/emacs-init.org
              ;; (turn-on-auto-fill)
              ;; ;; Turn on latex-math shortcuts
              ;; (turn-on-org-cdlatex)
              ))

  ;; Make the LaTeX preview bigger
  ;; (How to make formule bigger in org-mode of Emacs?)
  ;; (https://stackoverflow.com/a/11272625)
  (setq org-format-latex-options (plist-put
                                  org-format-latex-options
                                  :scale 1.5))

  ;; LaTeX + subfigures + org-mode
  ;; https://github.com/linktohack/ox-latex-subfigure
  (use-package ox-latex-subfigure
    :init
    (setq org-latex-caption-above nil
          org-latex-prefer-user-labels t)
    :load-path "~/.emacs.d/plugins/"
    :config (require 'ox-latex-subfigure))
  ;; -- end LaTeX ------------------------------

  ;; Is this creating trouble with org? (end of buffer)
  ;; Wrap text
  ;; http://danlovesprogramming.com/making-text-look-good-in-org-mode/
  ;; https://caolan.org/dotfiles/emacs.html
  (use-package visual-fill-column
    :ensure t
    :config
    ;; If this is enabled, it fucks up Helm (according to
    ;; the documentation, it should only affect those
    ;; buffers with visual-line-mode
    ;; https://github.com/joostkremers/visual-fill-column)
    ;; (progn
    ;;   (global-visual-fill-column-mode))
    )

  ;; Set soft line wrapping (only on screen, not addig
  ;; newlines to files)
  (remove-hook 'org-mode-hook #'turn-on-auto-fill)
  (add-hook 'org-mode-hook (lambda ()
                             (turn-on-visual-line-mode)
                             ;; Visual indent
                             (org-indent-mode)
                             ;; Turn off hard wrapping
                             ;; (adding newlines to text)
                             (auto-fill-mode -1)
                             ;; Turn on soft wrapping
                             ;; (on screen only)
                             (visual-fill-column-mode)
                             ;; Align tags from right to
                             ;; left to the width of the column
                             ;; (setq org-tags-column -60)
                             (setq org-tags-column
                                   (- fill-column))
                             ;; Show little arrows to
                             ;; indicate where there is a
                             ;; soft wrapping
                             (setq visual-line-fringe-indicators
                                   '(left-curly-arrow
                                     right-curly-arrow))
                             ;; Needed for
                             ;; visual-fill-column-mode
                             (visual-line-mode 1)
                             ))

  ;; Improve quotes
  ;; ;; https://stackoverflow.com/a/19332031
  ;; (setq org-export-smart-quotes-alist nil)
  ;; (add-to-list 'org-export-smart-quotes-alist
  ;;              '("en"
  ;;                (primary-opening   :utf-8 "“" :html "&ldquo;" :latex "\\blockquote{"  :texinfo "``")
  ;;                (primary-closing   :utf-8 "”" :html "&rdquo;" :latex "}"           :texinfo "''")
  ;;                (secondary-opening :utf-8 "‘" :html "&lsquo;" :latex "\\blockquote*{" :texinfo "`")
  ;;                (secondary-closing :utf-8 "’" :html "&rsquo;" :latex "}"           :texinfo "'")
  ;;                (apostrophe        :utf-8 "’" :html "&rsquo;")))

  ;; Match \( \)
  ;; https://emacs.stackexchange.com/questions/30962/is-it-possible-to-define-multi-character-pair-for-electric-pair-mode
  ;; https://emacs.stackexchange.com/questions/17284/adding-tilde-to-electric-pairs-in-org-mode

  ;; Template for org-capture (make notes quicker)
  ;; https://www.reddit.com/r/emacs/comments/4gudyw/help_me_with_my_orgmode_workflow_for_notetaking/d2l16uj/
  ;; https://cestlaz.github.io/posts/using-emacs-23-capture-1/#.WSCwWzHLcTs
  ;; http://doc.norang.ca/org-mode.html#HowToUseThisDocument
  (setq org-capture-templates
        '(("r"               ; key
           "Review"          ; name
           entry             ; type
           (file+headline
            (concat main-research-dir "/Reviews/reviews.org")
            "Article")  ; type target-file target-headline
           (concat      ; template ----
            "* %^{Title} %(org-set-tags)  :article: \n"
            ":PROPERTIES:\n"
            ":Created: %U\n"
            ":Linked: %a\n"
            ":END:\n"
            "%i\n"
            "Brief description:\n"
            "%?")                ; ---- template
           :prepend t        ; properties
           :empty-lines 1    ; properties
           :created t        ; properties
           )

          ;; * Meeting title
          ;; :PROPERTIES:
          ;; :PLACE:
          ;; :RESOURCE_ID:
          ;; :END:
          ;; See: [[link][description]]
          ("m"               ; key
           "Meeting"        ; name
           entry             ; type
           (file+headline    ; type
            (concat main-research-dir "Planning/events.org") ;target-file
            "Meetings")                          ;target-headline
           "* %? %^{SCHEDULED}p %^{PLACE}p %^{RESOURCE_ID}p\n"  ;template
           )

          ;; * My question :question:
          ;; See: [[link][description]]
          ("q"               ; key
           "Question"        ; name
           entry             ; type
           (file+headline    ; type
            (concat main-research-dir "questions.org") ;file
            "New")                          ;target-headline

           "* %? %(org-set-tags) :question: \n"
           ":PROPERTIES:"
           ":EXPORT_FILE_NAME:  /tmp/q.html"
           ":END:"
           "\nSee: %A\n"  ;template (cont.)
           )

          ;; Start to define school templates
          ("s" "School")
          ;; * Meeting title
          ;; SCHEDULED <time and date>
          ;; :PROPERTIES:
          ;; :PLACE:
          ;; :RESOURCE_ID:
          ;; :END:
          ("sm"               ; key
           "School meeting"   ; name
           entry              ; type
           (file+headline     ; type
            (concat my-docs-dir "Class/school_work.org") ;target-file
            "Meetings")                          ;target-headline
           "* %? %^{SCHEDULED}p %^{PLACE}p %^{RESOURCE_ID}p"  ;template
           )

          ;; School assignment
          ("sa"               ; key
           "Assignment"       ; name
           entry              ; type
           (file+headline     ; type
            (concat my-docs-dir "Class/school_work.org") ;target-file
            "Assignments")                          ;target-headline
           "* TODO %? %^{DEADLINE}p %^g"  ;template
            )

          ;; ("k"               ; key
          ;;  "Repeat test"     ; name
          ;;  entry             ; type
          ;;  (file+headline "~/Temp/repeat.org" "Test")  ; target
          ;;  "* %^{Title} %(org-set-tags)  :article: \n:PROPERTIES:\n:Created: %U\n:Linked: %a\n:END:\n%i\nBrief description:\n%?"  ; template
          ;;  :prepend t        ; properties
          ;;  :empty-lines 1    ; properties
          ;;  :created t        ; properties
          ;;  )
          ;;
          ;; ;; https://emacs.cafe/emacs/orgmode/gtd/2017/06/30/orgmode-gtd.html
          ;; ("t" "Tasks [inbox]" entry
          ;;  (file+headline "~/Documents/Research/Meniscus/Planning/inbox.org" "Tasks")
          ;;  "* TODO %i%?")
          ;; ("T" "Reminder" entry
          ;;  (file+headline "~/Documents/Research/Meniscus/Planning/reminders.org" "Reminder")
          ;;  "* %i%? \n %U")
          ))

  ;; ---------- Keywords and tags ----------
  ;; http://doc.norang.ca/org-mode.html#HowToUseThisDocument
  ;; Meetings are special. They are created in a done state
  ;; by a capture task. I use the MEET capture template
  ;; when someone interrupts what I'm doing with a question
  ;; or discussion. This is handled similarly to phone calls
  ;; where I clock the amount of time spent with whomever it
  ;; is and record some notes of what was discussed (either
  ;; during or after the meeting) depending on content,
  ;; length, and complexity of the discussion.
  (setq org-todo-keywords
        (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
                (sequence "WAIT(w@/!)""HOLD(h@/!)" "|"
                          "CANC(c@/!)" "PHONE" "MEET"))))
  ;; Change colors
  (setq org-todo-keyword-faces
        (quote (("TODO" :foreground "OrangeRed" :weight bold)
                ("NEXT" :foreground "RoyalBlue" :weight bold)
                ("DONE" :foreground "LightSeaGreen" :weight bold)
                ("WAIT" :foreground "Navajowhite4" :weight bold)
                ("HOLD" :foreground "magenta" :weight bold)
                ("CANC" :foreground "ForestGreen" :weight bold)
                ("MEET" :foreground "Hotpink2" :weight bold)
                ("PHONE" :foreground "forest green" :weight bold))))
  ;; Moving a task to CANC adds a CANC tag
  ;; Moving a task to WAIT adds a WAIT tag
  ;; Moving a task to HOLD adds WAIT and HOLD tags
  ;; Moving a task to a done state removes WAIT and HOLD tags
  ;; Moving a task to TODO removes WAIT, CANC, and HOLD tags
  ;; Moving a task to NEXT removes WAIT, CANC, and HOLD tags
  ;; Moving a task to DONE removes WAIT, CANC, and HOLD tags
  (setq org-todo-state-tags-triggers
        (quote (("CANC" ("CANC" . t))
                ("WAIT" ("WAIT" . t))
                ("HOLD" ("WAIT") ("HOLD" . t))
                (done ("WAIT") ("HOLD"))
                ("TODO" ("WAIT") ("CANC") ("HOLD"))
                ("NEXT" ("WAIT") ("CANC") ("HOLD"))
                ("DONE" ("WAIT") ("CANC") ("HOLD")))))

  ;; This is the default (use a single key to change state)
  ;; Changing a task state is done with C-c C-t KEY
  (setq org-use-fast-todo-selection t)

  ;; Skips setting timestamps and entering notes with S-left
  ;; and S-right
  (setq org-treat-S-cursor-todo-selection-as-state-change nil)
  ;; ------------------------ Keywords and tags

  ;; Agenda --------------------
  ;; Add my project tasks depending on the path where
  ;; it is located (check for Documentos and Documents)
  (with-eval-after-load "org-agenda"
     (setq org-agenda-files
              (list (concat main-research-dir "Planning/project_tasks.org")
                    (concat main-research-dir "Planning/events.org")
                    (concat my-docs-dir "Class/school_work.org")))
    ;; Hide taskjuggler tags
    ;; https://emacs.stackexchange.com/a/17357
    (setq org-agenda-hide-tags-regexp "taskjuggler_.*\\|something_else")

    ;; https://orgmode.org/worg/org-tutorials/org-custom-agenda-commands.html
    ;; https://orgmode.org/worg/org-tutorials/advanced-searching.html
    (add-to-list
     'org-agenda-custom-commands
     '("P" "Project agenda ToDo"
       (;; Create a custom view with an agenda and ToDo's
        ;; which are split by type
        (;; Create an agenda display
         agenda ""
                (
                 ;; limited to the present week (this is not
                 ;; really necessary, because the restriction
                 ;; below also applies here)
                 (org-agenda-ndays 7)
                 ))
        (;; Make a ToDo list with these tags
         todo "TODO|NEXT")
        (;; Make a ToDo list with tasks which are inactive
         todo "WAIT|HOLD|CANC")
        (;; Make a ToDo list with meetings
         todo "MEET")
        ;; ;; By priority
        ;; (tags-todo "+PRIORITY=\"A\"")
        ;; ;; These tags
        ;; (tags-todo "project")
        ;; ;; If I had categories...
        ;; (tags "project+CATEGORY=\"elephants\"")
        ;; limits the tag search to the file circuspeanuts.org
        )
       ;; options set here apply to the entire block
       ;; ((org-agenda-compact-blocks t))
       (;; Filters and stuff (for all the blocks; whole view)
        (;; Start on Monday
         org-agenda-start-on-weekday 1)
        (;; limits the whole view to the present week
         org-agenda-ndays 7)
        (;; limits the whole view to these files
         org-agenda-files
         '((concat main-research-dir "Planning/project_tasks.org")
           (concat main-research-dir "Planning/events.org")))
        )
       ))

    (add-to-list
     'org-agenda-custom-commands
     '("S" "School agenda ToDo"
       (;; Create a view with agenda and ToDo's
        (;; Create an agenda display
         ;; limited to the present week
         agenda "" ((org-agenda-ndays 7)))
        (todo "TODO|NEXT|MEET"
              (;; Filters and stuff
               ;; Customize title
               (org-agenda-overriding-header "This week")
               ;; limits the whole view to the present week
               (org-agenda-ndays 7)
               ))
        (todo "TODO|NEXT|MEET"
              (;; Filters and stuff
               ;; Customize title
               (org-agenda-overriding-header "Everything")
               ))
        )
       (;; Filters and stuff (for all the blocks; whole view)
        (;; Start on Monday
         org-agenda-start-on-weekday 1)
        (;; Limit to files related to school work
         org-agenda-files
         '(concat my-docs-dir "Class/school_work.org"))
        )
       )))
  ;; -------------------- Agenda

  ;; ---------- Alarms ----------
  ;; Add the agenda to appointments
  (with-eval-after-load "org-agenda"
    (org-agenda-to-appt)
    ;; Play an alarm with the agenda
    (defun play-alarm (sound)
      "Play a sound with ffplay. SOUND is a string with tne path to a
file which ffplay can play

Reference: https://www.gnu.org/software/emacs/manual/html_node/elisp/Asynchronous-Processes.html"
      (start-process "alarm"            ; process name
                     "*appt-buf*"       ; buffer name
                     "ffplay"           ; command (player)
                     "-autoexit"        ; arguments (close automatically,
                     "-vn" "-nodisp"    ; no video, no frame or window,
                     "-loglevel" "quiet"
                     sound))

    ;; https://emacs-fu.blogspot.fr/2009/11/showing-pop-ups.html
    (defun djcb-popup (title msg &optional icon sound)
      "Show a popup if we're on X, or echo it otherwise; TITLE is the title
of the message, MSG is the context. Optionally, you can provide an ICON and
a sound to be played"

      (interactive)
      (when sound (play-alarm sound))
      (if (eq window-system 'x)
          (start-process "pop-up"         ; internal process name (do not repeat)
                         "*alarms*"       ; buffer for output
                         "notify-send"    ; process (notification daemon)
                         "-i"
                         (if (file-exists-p icon) icon) ; first argument (icon)
                         title
                         msg)

        ;; text only version
        (message (concat title ": " msg)
                 ;; (play-alarm "/usr/share/sounds/freedesktop/stereo/alarm-clock-elapsed.oga")
                 )))

    ;; the appointment notification facility
    (setq
     appt-message-warning-time 15 ; warn 15 min in advance
     appt-display-interval 5      ; replay warning every 5 min

     appt-display-mode-line t     ; show in the modeline
     appt-display-format 'window) ; use our func
    (appt-activate 1)              ;; active appt (appointment notification)
    (display-time)                 ;; time display is required for this...

    ;; update appt each time agenda opened
    (add-hook 'org-finalize-agenda-hook 'org-agenda-to-appt)

    ;; our little façade-function for djcb-popup
    (defun djcb-appt-display (min-to-app new-time msg)
      (djcb-popup (format "Appointment in %s minute(s)" min-to-app) msg
                  "/usr/share/icons/HighContrast/48x48/status/alarm.png"

                  "/usr/share/sounds/freedesktop/stereo/alarm-clock-elapsed.oga"))
    (setq appt-disp-window-function (function djcb-appt-display)))
  ;; -------------------- Alarms

  ;; Interleave (To annotate PDF)
  ;; https://github.com/sriramkswamy/dotemacs/blob/master/config.org
  ;; https://www.reddit.com/r/emacs/comments/4gudyw/help_me_with_my_orgmode_workflow_for_notetaking/d2l16uj/
  (use-package interleave
;;     :ensure t
    :config
    (setq interleave-org-notes-dir-list
          '("~/Documents/Research/Meniscus/Reviews/"))
    )

  ;; ;; Check this
  ;; ;; https://github.com/machc/pdf-tools-org
  ;; ;; http://matt.hackinghistory.ca/2015/11/11/note-taking-with-pdf-tools/
  ;; (use-package pdf-tools-org
  ;;   :load-path "~/.emacs.d/plugins/pdf-tools-org"
  ;;   :ensure t)

  ;; ;; To transform from table to properties
  ;; (add-to-list 'load-path "~/.emacs.d/plugins/org-transform-tree-table")
  ;; (require 'org-transform-tree-table)

  ;; ;; Org-gantt
  ;; (use-package org-gantt
  ;;   :load-path "~/.emacs.d/plugins/org-gantt"
  ;;   ;; :config
  ;;   ;; (require 'org-gantt)
  ;;   ;; (setq 'org-gantt-default-title-calendar)
  ;;   )

  ;; --- References ---
  (use-package org-ref
;; :ensure org-ref
	:init

	;; ;; ******************************
	;; ;; Set default bibliography and files
	;; ;; ******************************
	;; ;; From org-ref README.org
	;; (setq reftex-default-bibliography
	;;       '("~/Documents/Research/Meniscus/References.bib")
	;;       ;; https://www.reddit.com/r/emacs/comments/4gudyw/help_me_with_my_orgmode_workflow_for_notetaking/d2l16uj/
	;;       org-ref-notes-directory
	;;       "~/Documents/Research/Meniscus/Reviews/"
	;;       org-ref-bibliography-notes
	;;       "~/Documents/Research/Meniscus/Reviews/reviews.org"
	;;       org-ref-default-bibliography
	;;       '("~/Documents/Research/Meniscus/References.bib")
	;;       org-ref-pdf-directory
	;;       "~/Documents/Research/Meniscus/Sources/")
	;; (setq bibtex-completion-bibliography
	;;       "~/Documents/Research/Meniscus/References.bib"
	;;       bibtex-completion-library-path
	;;       "~/Documents/Research/Meniscus/Sources/"
	;;       bibtex-completion-notes-path
	;;       "~/Documents/Research/Meniscus/Reviews/reviews.org")
	;; ;; https://www.reddit.com/r/emacs/comments/4gudyw/help_me_with_my_orgmode_workflow_for_notetaking/d2l16uj/
	;; (setq helm-bibtex-bibliography
	;;       "~/Documents/Research/Meniscus/References.bib"
	;;       helm-bibtex-library-path
	;;       "~/Documents/Research/Meniscus/Sources/"
	;;       helm-bibtex-notes-path
	;;       "~/Documents/Research/Meniscus/Reviews/reviews.org")
    ;; ;; Prefer labels created by user instead of automatic ones (readability)
    ;; (setq org-latex-prefer-user-labels t)

	;; ;; Make sure that the directory for PDF exists
	;; ;; from org-ref-melpa.el
    ;; (with-eval-after-load "pdf-tools"
    ;;   (unless (file-exists-p org-ref-pdf-directory)
    ;;     (make-directory org-ref-pdf-directory t)))

	;; Set key for org-ref + hydra
	(setq org-ref-bibtex-hydra-key-binding "\C-cj")
	;; Reset org-agenda command:
	;; http://kitchingroup.cheme.cmu.edu/blog/2014/05/13/Using-org-ref-for-citations-and-references/
	(setq org-ref-insert-cite-key "C-c )")

	;; Open PDF with pdf-tools
	;; (defun my/org-ref-open-pdf-at-point ()
	;;   "Open the pdf for bibtex key under point if it exists."
	;;   (interactive)
	;;   (let* ((results (org-ref-get-bibtex-key-and-file))
	;;          (key (car results)))
	;;     (funcall bibtex-completion-pdf-open-function
	;;              (car (bibtex-completion-find-pdf key)))))
	;; (setq org-ref-open-pdf-function
	;;       'my/org-ref-open-pdf-at-point)
	(setq org-ref-open-pdf-function
	      'org-ref-open-pdf-at-point)

	:config
	;; Set auto-key
	;; from org-ref-melpa.el (modified)
	;; Author1Author2Author3YY-t     ;-t: first letter of the title
	(setq bibtex-autokey-year-length 2  ; my own
	      bibtex-autokey-name-length 5 ; my own
	      bibtex-autokey-names 3        ; my own
	      bibtex-autokey-name-separator "" ; my own
	      ;; https://tex.stackexchange.com/questions/212908/how-to-label-my-references-so-i-know-how-to-refer-to-them
	      bibtex-autokey-name-case-convert-function (quote capitalize)
	      ;; http://jblevins.org/log/bibtex
	      bibtex-autokey-titleword-first-ignore '("the" "a" "if" "and" "an")
	      bibtex-autokey-name-year-separator "" ; orig "-"
	      bibtex-autokey-year-title-separator "" ; orig "-"
	      bibtex-autokey-titleword-separator "" ; orig "-"
	      bibtex-autokey-titlewords 0   ; orig 2
	      bibtex-autokey-titlewords-stretch 0 ; orig 1
	      bibtex-autokey-titleword-length 0)  ; orig 5

	;; Add abstract to the fields
	(setq bibtex-BibTeX-field-alist
	      '(("author" "Author1 [and Author2 ...] [and others]")
            ("editor" "Editor1 [and Editor2 ...] [and others]")
            ("journaltitle" "Name of the journal (use string, remove braces)")
            ("year" "Year of publication")
            ("month" "Month of the publication as a string (remove braces)")
            ("note" "Remarks to be put at the end of the \\bibitem")
            ("publisher" "Publishing company")
            ("address" "Address of the publisher")
            ("abstract" "Abstract provided by publisher")))

  	;; Add bibtex to the PDF command
  	(setq org-latex-pdf-process
          ;; (list "latexmk -shell-escape -pdflatex=xelatex -pdf -quiet -bibtex %f"))
          ;; '("xelatex -interaction nonstopmode -output-directory %o %f"
          '("xelatex -interaction nonstopmode -shell-escape -output-directory %o %f"
            "biber %b"
            "xelatex -interaction nonstopmode -shell-escape -output-directory %o %f"
            "xelatex -interaction nonstopmode -shell-escape -output-directory %o %f"
            "xelatex -interaction nonstopmode -shell-escape -output-directory %o %f")
          )
  	      ;; ;; '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
  	      ;; '("pdflatex -interaction nonstopmode -output-directory %o %f"
          ;;   "biber %b"
          ;;   "pdflatex -interaction nonstopmode -output-directory %o %f"
          ;;   "pdflatex -interaction nonstopmode -output-directory %o %f"))
  	;; (setq org-latex-pdf-process
  	;;       '("latexmk -shell-escape -bibtex -pdf %f"))
    ;; From org-ref-help
    (setq bibtex-dialect 'biblatex)

  	;; Extra utilities for org-ref (org-ref.org)
  	(with-eval-after-load "org-ref"
  	  (require 'org-id)
  	  (require 'org-ref-wos)
  	  (require 'org-ref-scopus)
  	  (require 'org-ref-pubmed)))
    ;; --end: References org-ref ------------------------

    ;; (add-to-list 'load-path "~/.emacs.d/plugins/org-gantt")
    ;; (require 'org-gantt)

    ;; PDF links for org-mode
    ;; https://lists.gnu.org/archive/html/emacs-orgmode/2016-11/msg00169.html
    (with-eval-after-load "pdf-tools"
      (use-package org-pdfview
;; :ensure org-pdfview
        :config
        ;; ;; Before adding, remove it (to avoid clogging)
        ;; (delete '("\\.pdf\\'" . default) org-file-apps)
        ;; https://lists.gnu.org/archive/html/emacs-orgmode/2016-11/msg00176.html
        (add-to-list 'org-file-apps
                     '("\\.pdf\\'" .
                       (lambda (file link)
                         (org-pdfview-open link))))))
    ;; --end: PDF links ------------------------------

    ;; ;; Export svg (does not work)
    ;; ;; https://lists.gnu.org/archive/html/emacs-orgmode/2013-08/msg00221.html
    ;; ;; https://github.com/tsdye/tufte-org-mode/blob/master/ox-tufte-latex.el
    ;; (defun my-svg-graphics (contents backend info)
    ;;   (when (eq backend 'latex)
    ;;     (replace-regexp-in-string "\\`\\\\includegraphics.+\\({.+\.svg}\\)"
    ;;                               "\\\\simplesvg\1" contents))
    ;;   )
    ;; (add-to-list 'org-export-filter-link-functions 'my-svg-graphics)

    ;; Don't ask if I want to evaluate LaTeX source blocks
    (defun my-org-confirm-babel-evaluate (lang body)
      (not (string= lang "latex")))  ; don't ask for ditaa
    (setq org-confirm-babel-evaluate 'my-org-confirm-babel-evaluate)

    ;; ;; Hide headlines (does not work)
    ;; ;; https://emacs.stackexchange.com/a/17677
    ;; (require 'ox-extra)
    ;; ;; https://emacs.stackexchange.com/a/30151
    ;; (add-hook 'org-export-filter-parse-tree-functions
    ;;           'org-export-notignore-headlines nil t)

    ;; ;; Replace all links with descriptions
    ;; ;; https://emacs.stackexchange.com/questions/10707/in-org-mode-how-to-remove-a-link
    ;; (defun afs/org-replace-link-by-link-description ()
    ;;   "Replace an org link by its description or if empty its address"
    ;;   (interactive)
    ;;   (if (org-in-regexp org-bracket-link-regexp 1)
    ;;       (let ((remove (list (match-beginning 0) (match-end 0)))
    ;;             (description (if (match-end 3)
    ;;                              (match-string-no-properties 3)
    ;;                            (match-string-no-properties 1))))
    ;;         (apply 'delete-region remove)
    ;;         (insert description))))
    (setq org-ref-open-pdf-function
          'org-ref-open-pdf-at-point)
    )
;; ****************************** org-mode

;; ********** Ditaa **********
;; (org-babel-do-load-languages
;;  'org-babel-load-languages
;;  '((ditaa .t)))
(setq org-ditaa-jar-path "~/.emacs.d/plugins/ditaa.jar")
;; ******************************

;; ********** Dot / graphviz ****
;; https://github.com/dfeich/org-babel-examples/blob/master/graphviz/graphviz-babel.org
;; ******************************

;; ********** Others **********
;; Package: volatile-highlights
;; GROUP: Editing -> Volatile Highlights
(use-package volatile-highlights
;; :ensure volatile-highlights
  :init
  (volatile-highlights-mode t))

;; Package: ws-butler
(use-package ws-butler
;; :ensure ws-butler
  :init
  (add-hook 'prog-mode-hook 'ws-butler-mode)
  (add-hook 'text-mode 'ws-butler-mode)
  (add-hook 'fundamental-mode 'ws-butler-mode))
;; ******************************

;; ****** Version Control ******
(use-package magit
;; :ensure magit
  :config
  (global-set-key "\C-xg" 'magit-status)
  (global-set-key [f7] 'find-file-in-repository))
;; ******************************

;; ;; ********** c-mode **********
; Add cmake listfile names to the mode list.
(setq auto-mode-alist
	  (append
	   '(("CMakeLists\\.txt\\'" . cmake-mode))
	   '(("\\.cmake\\'" . cmake-mode))
	   auto-mode-alist))
(autoload 'cmake-mode "cmake-mode" "CMake mode" t)
;; https://github.com/tuhdo/emacs-c-ide-demo/tree/master/custom
;; ========== helm- gtags ==========
;; (require 'setup-helm-gtags)
(use-package helm-gtags
  :init
  ;; these variables must be set before load helm-gtags
  ;; you can change to any prefix key of your choice
  (setq helm-gtags-prefix-key "\C-cg")
  (require 'ggtags)

  (progn
    (setq helm-gtags-ignore-case t
          helm-gtags-auto-update t
          helm-gtags-use-input-at-cursor t
          helm-gtags-pulse-at-cursor t
          helm-gtags-prefix-key "\C-cg"
          helm-gtags-suggested-key-mapping t)

    ;; Enable helm-gtags-mode in Dired so you can jump to any tag
    ;; when navigate project tree with Dired
    (add-hook 'dired-mode-hook 'helm-gtags-mode)

    ;; Enable helm-gtags-mode in Eshell for the same reason as above
    (add-hook 'eshell-mode-hook 'helm-gtags-mode)

    ;; Enable helm-gtags-mode in languages that GNU Global supports
    (add-hook 'c-mode-hook 'helm-gtags-mode)
    (add-hook 'c++-mode-hook 'helm-gtags-mode)
    (add-hook 'java-mode-hook 'helm-gtags-mode)
    (add-hook 'asm-mode-hook 'helm-gtags-mode)

    ;; key bindings
    (with-eval-after-load 'helm-gtags
      (define-key helm-gtags-mode-map (kbd "C-c g a") 'helm-gtags-tags-in-this-function)
      (define-key helm-gtags-mode-map (kbd "C-j") 'helm-gtags-select)
      (define-key helm-gtags-mode-map (kbd "M-.") 'helm-gtags-dwim)
      (define-key helm-gtags-mode-map (kbd "M-,") 'helm-gtags-pop-stack)
      (define-key helm-gtags-mode-map (kbd "C-c <") 'helm-gtags-previous-history)
      (define-key helm-gtags-mode-map (kbd "C-c >") 'helm-gtags-next-history))))

;; (require 'setup-c)
(use-package company-c-headers
  :config
  (add-to-list 'company-backends 'company-c-headers))
;; hs-minor-mode for folding source code
(add-hook 'c-mode-common-hook 'hs-minor-mode)
;; Set default style
(setq c-default-style '((java-mode . "java")
                        (awk-mode . "awk")
                        (python . "python")
                        (other . "gnu")))

(use-package cc-mode
  ;; :commands
  ;; (company-complete)
  ;; :config
  ;; ;; Uncomment this
  ;; (define-key c-mode-map  [(tab)] 'company-complete)
  ;; (define-key c++-mode-map  [(tab)] 'company-complete)
  )

;; C Development with Emacs malb::blog
(use-package semantic
  :config
  (use-package semantic/bovine/gcc)
  (add-to-list 'semantic-default-submodes
               '(global-semanticdb-minor-mode
                 ;; 'global-semantic-idle-local-symbol-highlight-mode)
                 global-semantic-idle-scheduler-mode
                 global-semantic-idle-summary-mode))

  (semantic-mode 1))

(use-package ede
  :config
  (global-ede-mode t)
  (ede-enable-generic-projects))

;; (require 'setup-cedet)
(defun alexott/cedet-hook ()
  (local-set-key "\C-c\C-j" 'semantic-ia-fast-jump)
  (local-set-key "\C-c\C-s" 'semantic-ia-show-summary))

(add-hook 'c-mode-common-hook 'alexott/cedet-hook)
(add-hook 'c-mode-hook 'alexott/cedet-hook)
(add-hook 'c++-mode-hook 'alexott/cedet-hook)
;;--ends

(use-package function-args
  :config
  (add-hook 'c-mode-common-hook (lambda ()
                                  (function-args-mode) ; Find definitions in current buffer
                                  (setq tab-width 4) ; set tab width to 4
                                  (auto-fill-mode) ; break long lines
                                  (yas-minor-mode) ; activate yasnippet
                                  )))
;; Already in the company configuration
;; (add-to-list 'company-backends 'company-c-headers)

;; Change the style
(setq-default flycheck-gcc-openmp t)
(setq flycheck-gcc-language-standard "gnu99")
;; (add-hook 'c-mode-hook (lambda () (setq flycheck-gcc-language-standard "gnu99")))

(use-package clang-format
  :config
  (setq clang-format-executable "clang-format-3.8"))

;; Irony mode ??
;; https://gist.github.com/soonhokong/7c2bf6e8b72dbc71c93b
;; ********************

;; ***** Recoll *****
(use-package helm-recoll
;; :ensure helm-recoll
  ;; :load-path "~/.emacs.d/plugins/helm-recoll"
  :config
  ;; !!! Adapt this !!!
  (helm-recoll-create-source
   "biblio" (concat my-docs-dir "Biblio/.recoll-biblio/"))
  (helm-recoll-create-source
   "sources" (concat main-research-dir "Sources/.recoll-sources/"))
  ;; (helm-recoll-create-source
  ;;  "coen" "~/Documents/Research/Meniscus/.recoll-coen/")
  (setq hem-recoll-options '("recoll" "-t" "-b" "-q")))
;; ******************************

;; ;; ***** Deleting, pasting and others ****
;; ;; Fix this
;; (use-package iedit
;;   :commands
;;   (iedit-mode)
;;   ;; :bind (("C-c C-t" . iedit-mode))
;;   :init
;;   (bind-key "C-c C-t" 'iedit-mode)
;;   :config
;;   (setq iedit-toggle-key-default nil))


(defun prelude-move-beginning-of-line (arg)
  "Move point back to indentation of beginning of line.

Move point to the first non-whitespace character on this line.
If point is already there, move to the beginning of the line.
Effectively toggle between the first non-whitespace character and
the beginning of the line.

If ARG is not nil or 1, move forward ARG - 1 lines first. If
point reaches the beginning or end of the buffer, stop there."
  (interactive "^p")
  (setq arg (or arg 1))

  ;; Move lines first
  (when (/= arg 1)
    (let ((line-move-visual nil))
      (forward-line (1- arg))))

  (let ((orig-point (point)))
    (back-to-indentation)
    (when (= orig-point (point))
      (move-beginning-of-line 1))))

(global-set-key (kbd "C-a") 'prelude-move-beginning-of-line)

(defadvice kill-ring-save (before slick-copy activate compile)
  "When called interactively with no active region, copy a single
line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (message "Copied line")
     (list (line-beginning-position)
           (line-beginning-position 2)))))

(defadvice kill-region (before slick-cut activate compile)
  "When called interactively with no active region, kill a single
  line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (list (line-beginning-position)
           (line-beginning-position 2)))))

(defadvice kill-ring-save (before slick-copy activate compile)
  "When called interactively with no active region, copy a single
line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (message "Copied line")
     (list (line-beginning-position)
           (line-beginning-position 2)))))

(defadvice kill-region (before slick-cut activate compile)
  "When called interactively with no active region, kill a single
  line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (list (line-beginning-position)
           (line-beginning-position 2)))))

;; kill a line, including whitespace characters until next non-whiepsace character
;; of next line
(defadvice kill-line (before check-position activate)
  (if (member major-mode
              '(emacs-lisp-mode scheme-mode lisp-mode
                                c-mode c++-mode objc-mode
                                latex-mode plain-tex-mode))
      (if (and (eolp) (not (bolp)))
          (progn (forward-char 1)
                 (just-one-space 0)
                 (backward-char 1)))))

;; taken from prelude-editor.el
;; automatically indenting yanked text if in programming-modes
(defvar yank-indent-modes
  '(LaTeX-mode TeX-mode)
  "Modes in which to indent regions that are yanked (or yank-popped).
Only modes that don't derive from `prog-mode' should be listed here.")

(defvar yank-indent-blacklisted-modes
  '(python-mode slim-mode haml-mode)
  "Modes for which auto-indenting is suppressed.")

(defvar yank-advised-indent-threshold 1000
  "Threshold (# chars) over which indentation does not automatically occur.")

(defun yank-advised-indent-function (beg end)
  "Do indentation, as long as the region isn't too large."
  (if (<= (- end beg) yank-advised-indent-threshold)
      (indent-region beg end nil)))

(defadvice yank (after yank-indent activate)
  "If current mode is one of 'yank-indent-modes,
indent yanked text (with prefix arg don't indent)."
  (if (and (not (ad-get-arg 0))
           (not (member major-mode yank-indent-blacklisted-modes))
           (or (derived-mode-p 'prog-mode)
               (member major-mode yank-indent-modes)))
      (let ((transient-mark-mode nil))
        (yank-advised-indent-function (region-beginning) (region-end)))))

(defadvice yank-pop (after yank-pop-indent activate)
  "If current mode is one of `yank-indent-modes',
indent yanked text (with prefix arg don't indent)."
  (when (and (not (ad-get-arg 0))
             (not (member major-mode yank-indent-blacklisted-modes))
             (or (derived-mode-p 'prog-mode)
                 (member major-mode yank-indent-modes)))
    (let ((transient-mark-mode nil))
      (yank-advised-indent-function (region-beginning) (region-end)))))

;; prelude-core.el
(defun indent-buffer ()
  "Indent the currently visited buffer."
  (interactive)
  (indent-region (point-min) (point-max)))

;; prelude-editing.el
(defcustom prelude-indent-sensitive-modes
  '(coffee-mode python-mode slim-mode haml-mode yaml-mode)
  "Modes for which auto-indenting is suppressed."
  :type 'list)

(defun indent-region-or-buffer ()
  "Indent a region if selected, otherwise the whole buffer."
  (interactive)
  (unless (member major-mode prelude-indent-sensitive-modes)
    (save-excursion
      (if (region-active-p)
          (progn
            (indent-region (region-beginning) (region-end))
            (message "Indented selected region."))
        (progn
          (indent-buffer)
          (message "Indented buffer.")))
      (whitespace-cleanup))))

(global-set-key (kbd "C-c i") 'indent-region-or-buffer)

;; add duplicate line function from Prelude
;; taken from prelude-core.el
(defun prelude-get-positions-of-line-or-region ()
  "Return positions (beg . end) of the current line
or region."
  (let (beg end)
    (if (and mark-active (> (point) (mark)))
        (exchange-point-and-mark))
    (setq beg (line-beginning-position))
    (if mark-active
        (exchange-point-and-mark))
    (setq end (line-end-position))
    (cons beg end)))

;; smart openline
(defun prelude-smart-open-line (arg)
  "Insert an empty line after the current line.
Position the cursor at its beginning, according to the current mode.
With a prefix ARG open line above the current line."
  (interactive "P")
  (if arg
      (prelude-smart-open-line-above)
    (progn
      (move-end-of-line nil)
      (newline-and-indent))))

(defun prelude-smart-open-line-above ()
  "Insert an empty line above the current line.
Position the cursor at it's beginning, according to the current mode."
  (interactive)
  (move-beginning-of-line nil)
  (newline-and-indent)
  (forward-line -1)
  (indent-according-to-mode))

(global-set-key (kbd "M-o") 'prelude-smart-open-line)
(global-set-key (kbd "M-o") 'open-line)
;; ******************************

;; ********** XML **********
;; The old “how to fold XML” question
;; https://emacs.stackexchange.com/a/3157
(use-package nxml-mode
  :init
  (use-package hideshow
;; :ensure hideshow
)
  (use-package sgml-mode
;;     :ensure sgml-mode
)

  (add-hook 'nxml-mode-hook 'hs-minor-mode)
  :commands
  (hs-toggle-hiding)
  :config

  (add-to-list 'hs-special-modes-alist
               '(nxml-mode
                 "<!--\\|<[^/>]*[^/]>"
                 "-->\\|</[^/>]*[^/]>"

                 "<!--"
                 sgml-skip-tag-forward
                 nil))
  ;; optional key bindings, easier than hs defaults
  ;; optional key bindings, easier than hs defaults
  (define-key nxml-mode-map (kbd "C-c C-c h") 'hs-toggle-hiding));;
;; --end

;; ;; Fold XML
;; ;; https://github.com/ataka/html-fold
;; ;; C-c C-o C-b / C-c C-o b folding/unfolding buffer
;; ;; C-c C-o C-r / C-c C-o r folding/unfolding region
;; ;; C-c C-o C-p / C-c C-o p folding/unfolding paragraph
;; ;; C-c C-o C-o folding and unfolding case by case
;; ;; C-c C-o C-e folding block elements
;; ;; C-c C-o C-m folding inline elements
;; (autoload 'html-fold-mode
;;   "html-fold"
;;   "Minor mode for hiding and revealing elements." t)
;;
;; (add-hook 'nxml-mode-hook 'html-fold-mode)
;; ;; --end

;; ;; Fold XML
;; (add-hook 'nxml-mode-hook (lambda () (require 'noxml-fold)))
;; ******************************

;; ********** aliases **********
(add-hook 'eshell-mode-hook 'eshell-load-bashrc-aliases)
(defun re-n-matches ()
  (1- (/ (length (match-data)) 2)))

(defun match-strings-all (&optional string)
  "Return the list of all expressions matched in last search.
      STRING is optionally what was given to `string-match'."
  (loop for i from 0 to (re-n-matches)
    	collect (match-string-no-properties i string)))

;; There is no regexp-count-capture-groups
;; (defun re-find-all (regexp string &optional groups yes-props)
;;   "like python's re.find_all"
;;   (let (
;;     	(groups (or groups (list (regexp-count-capture-groups regexp))))
;;     	(match-string-fun (if (not yes-props) 'match-string 'match-string-no-properties))
;;     	(start 0)
;;     	(matches nil )
;;     	)
;;     (while (setq start (and (string-match regexp string start) (match-end 0)))
;;       (setq matches (cons (cdr (match-strings-all string)) matches))
;;       )
;;     (setq matches (reverse matches))
;;     (if (not (cdar matches))
;;     	(mapcar 'car matches)
;;       matches
;;       )
;;     )
;;   )

;; There is no eshell-flatten-and-stringify
;; (defun apply-eshell-alias (alias &rest definition)
;;   "basically taken from eshell/alias function"
;;   (if (not definition)
;;       (setq eshell-command-aliases-list
;;             (delq (assoc alias eshell-command-aliases-list)
;;                   eshell-command-aliases-list))
;;     (and (stringp definition)
;;          (set-text-properties 0 (length definition) nil definition))
;;     (let ((def (assoc alias eshell-command-aliases-list))
;;           (alias-def (list alias
;;                            (eshell-flatten-and-stringify definition))))
;;       (if def
;;           (setq eshell-command-aliases-list
;;                 (delq def eshell-command-aliases-list)))
;;       (setq eshell-command-aliases-list
;;             (cons alias-def eshell-command-aliases-list))))
;;   )
;; There is no get-string-from-file
;; (defun eshell-load-bashrc-aliases ()
;;   (interactive)
;;   (mapc (lambda (alias-def) (apply 'eshell/alias alias-def))
;;     	(re-find-all "^alias \\([^=]+\\)='?\\(.+?\\)'?$"
;;                      (get-string-from-file  (concat (getenv "HOME") "/" ".bashrc"))
;;                      )
;;     	)
;;   )
;; ******************************

;; ********** Gmsh **********
(use-package gmsh
  :load-path "~/.emacs.d/plugins/")
;; ******************************

;; ********** TaskJuggler (planning) *****
;; Requires org-mode > 0.8
(use-package ox-taskjuggler
  :config
  (add-to-list 'org-export-backends 'taskjuggler)
  ;; Change default version of taskjuggler
  (setq org-taskjuggler-target-version 3.6)
  ;; Define macros which load before the project definition.
  ;; This is useful to use the same keyword (e.g. extend)
  ;; more than once when defining the properties of a project
  ;; in org-mode
  (setq org-taskjuggler-default-global-header
        (concat
         "# A macro is replaced by its contents when it is called. To call\n"
         "# it, use ${  }, for instance ${allocate_students}\n"
         "macro extend_resource [\n"
         "    extend resource {\n"
         "        text Phone \"Phone\"\n"
         "        }\n"
         "]\n"
         "macro extend_task [\n"
         "    extend task {\n"
         "        text Place \"Place\"\n"
         "        }\n"
         "]\n"))
  ;; This is written after the definition of the project in the tjp file
  (setq org-taskjuggler-default-global-properties
        (concat
         "# Hours in which I am not in class (lab hours)\n"
         "shift labTime \"Lab time\" {
         workinghours mon 9:00 - 13:00, 14:00 - 18:00
         workinghours tue 9:00 - 12:00, 14:30 - 15:00
         workinghours wed 8:30 - 12:00, 15:00 - 16:30
         workinghours thu 9:00 - 12:00, 14:30 - 15:00, 16:30 - 18:00
         workinghours fri 9:00 - 12:30, 14:30 - 18:00
         }\n"

         "# Time for school (out of classroom)\n"
         "shift school \"School work\" {
         workinghours mon 19:00 - 21:30
         workinghours tue 19:00 - 21:30
         workinghours wed 16:30 - 18:00, 19:00 - 21:30
         workinghours thu 19:00 - 21:30
         workinghours fri 19:00 - 20:00
         }\n"
         ))
  ;; Add the custom macros which were defined above to the
  ;; valid keywords
  (add-to-list 'org-taskjuggler-valid-project-attributes
               '${extend_task} '${extend_resource})
  (add-to-list 'org-taskjuggler-valid-resource-attributes
               'Phone)
  (add-to-list 'org-taskjuggler-valid-task-attributes
               'Place)

  ;; Modify the default report
  (setq org-taskjuggler-default-reports
        '("textreport report \"Plan\" {
  formats html
  header '== %title =='

  center -8<-
    [#Plan Plan] | [#Resource_Allocation Resource Allocation]
    ----
    === Plan ===
    <[report id=\"plan\"]>
    ----
    === Resource Allocation ===
    <[report id=\"resourceGraph\"]>
  ->8-
}

# A traditional Gantt chart with a project overview.
taskreport plan \"\" {
  headline \"Project Plan\"
  columns bsi, name, start, end, effort, chart
  loadunit shortauto
  hideresource 1
}

# A graph showing resource allocation. It identifies whether each
# resource is under- or over-allocated for.
resourcereport resourceGraph \"\" {
  headline \"Resource Allocation Graph\"
  columns no, name, effort, weekly
  loadunit shortauto
  hidetask ~(isleaf() & isleaf_())
  sorttasks plan.start.up
}"
          )))

(use-package taskjuggler-mode
  ;; http://jurjenbokma.com/ApprenticesNotes/editing_taskjuggler_with_emacs.xhtml
  ;; https://www.skamphausen.de/cgi-bin/ska/taskjuggler-mode
  :load-path "~/.emacs.d/plugins/")
;; ******************************

;; ********** Dictionary **********
(use-package dictionary
  :load-path "/usr/share/emacs/site-lisp/dictionary/"
  :config
  (progn
    (load "dictionary-init")))
;; ******************************

;; Theme
;; '(custom-enabled-themes (quote (wombat)))
;; (load-theme 'tango-dark)
;; (load-theme 'zenburn t)
;; (load-theme 'tangotango t)
;; (load-theme 'tango-plus t)
;; (load-theme 'material t)

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;; function to wrap blocks of text in org templates                       ;;
;; ;; e.g. latex or src etc                                                  ;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (defun org-begin-template ()
;;   "Make a template at point."
;;   (interactive)
;;   (if (org-at-table-p)
;;       (call-interactively 'org-table-rotate-recalc-marks)
;;     (let* ((choices '(("s" . "SRC")
;;                       ("e" . "EXAMPLE")
;;                       ("q" . "QUOTE")
;;                       ("v" . "VERSE")
;;                       ("c" . "CENTER")
;;                       ("l" . "LaTeX")
;;                       ("h" . "HTML")
;;                       ("a" . "ASCII")))
;;            (key
;;             (key-description
;;              (vector
;;               (read-key
;;                (concat (propertize "Template type: " 'face 'minibuffer-prompt)
;;                        (mapconcat (lambda (choice)
;;                                     (concat (propertize (car choice) 'face 'font-lock-type-face)
;;                                             ": "
;;                                             (cdr choice)))
;;                                   choices
;;                                   ", ")))))))
;;       (let ((result (assoc key choices)))
;;         (when result
;;           (let ((choice (cdr result)))
;;             (cond
;;              ((region-active-p)
;;               (let ((start (region-beginning))
;;                     (end (region-end)))
;;                 (goto-char end)
;;                 (insert "#+END_" choice "\n")
;;                 (goto-char start)
;;                 (insert "#+BEGIN_" choice "\n")))
;;              (t
;;               (insert "#+BEGIN_" choice "\n")
;;               (save-excursion (insert "#+END_" choice))))))))))
;;
;; ;;bind to key
;; (define-key org-mode-map (kbd "C-<") 'org-begin-template)


;; ;; The following adds all dirs under ~/.emacs.d/site-lisp to load
;; ;; https://emacs.stackexchange.com/a/8297
;; ;; path, so you can just require the package and you are done:
;; (let* ((my-lisp-dir "~/.emacs.d/site-lisp/")
;;        (default-directory my-lisp-dir)
;;        (orig-load-path load-path))
;;   (setq load-path (cons my-lisp-dir nil))
;;   (normal-top-level-add-subdirs-to-load-path)
;;   (nconc load-path orig-load-path))

;; ;; Code to install packages automatically
;; ;; https://stackoverflow.com/a/10093312
;; ;; Set my default list of packages (to install)
;; (setq package-list
;;  '(org-pdfview helm-projectile helm-swoop org-edit-latex
;; 	hideshow interleave org-edit-latex ;; org-gantt ;; pdf-tools-org
;; 	org-ref  sgml-mode company ;; gmsh ;; ox-latex-subfigure
;; 	visual-fill-column anzu company desktop dired-details elpy
;; 	helm helm-recoll highlight-parentheses magit ;; nxml-mode
;; 	org pdf-tools projectile py-autopep8 reftex ;;ox-taskjuggler
;; 	undo-tree use-package volatile-highlights workgroups ws-butler
;;      yasnippet))

;; ; activate all the packages (in particular autoloads)
;; (package-initialize)

;; ; fetch the list of packages available
;; (unless package-archive-contents
;;   (package-refresh-contents))

;; ; install the missing packages
;; (dolist (package package-list)
;;   (unless (package-installed-p package)
;;     (package-install package)))

(message (concat "Finished loading " (message user-init-file)))
;;; .emacs ends here
