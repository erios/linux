alias ls='ls --color=auto'

# Debian
# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
