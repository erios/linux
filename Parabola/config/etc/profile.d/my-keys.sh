if [[ $- != *i* ]]; then

    # Set UTF font on terminal
    setfont Lat2-Terminus16
    # Set keyboard with dead tildes and swapping CTRL and CAPS
    loadkeys latam-ctrlswapcaps

fi;
