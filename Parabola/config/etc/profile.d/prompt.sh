# Define some colors first:
# Define a few Color's
LIGHTGREEN=$(tput setaf 2)
LIGHTRED=$(tput setaf 1)
BOLD=$(tput bold)
NC=$(tput sgr0)


#-------------------------------------------------------------
# Shell Prompt
#-------------------------------------------------------------
PS1='[\[$LIGHTRED$BOLD\]\u@\h\[$NC\]][\[$LIGHTGREEN$BOLD\]\w\[$NC\]]\$ '
