# Maintainer: Stefan Husmann <stefan-husmann@t-online.de>
# Maintainer: shmilee <shmilee.zju at gmail dot com>
# Contributor: mcmillan <awmcmillan at gmail dot com>

_pkgbase='engauge-digitizer'

pkgname=('engauge' 'engauge-samples')
pkgbase='engauge'
pkgver=10.4
pkgrel=1
url="http://markummitchell.github.io/engauge-digitizer/"
arch=('i686' 'x86_64')
license=('GPL')
makedepends=('qt5-tools' 'fftw' 'log4cpp' 'libjpeg-turbo' 'libpng' 'openjpeg2' 'poppler-qt5')
source=("$pkgbase-$pkgver.tar.gz::https://github.com/markummitchell/$_pkgbase/archive/v$pkgver.tar.gz"
        "$pkgbase.sh"
        "$pkgbase.desktop")
sha512sums=('5dc794d2cc8a109e1a6390f9643f399f5cfeb72e99783faefdea93e2f476d31ed1c34d117adf5a3d8674cc9242615f79002992fe8f1388f2919052d35c7b93dd'
         '67bd598edd39891a7686a70b07319ccfb2fba83b380d6f9817eb4df4a8ccbf2382d992f1fed452a2b9815553e31bd9480ee2ec26b2861a40439d6b32e0abb42e'
         '85f42257ce86a898202855514b761c98ef518446853a3872797fb62a11d7778493bd9c25fd04e124660340816c1f57000b9109a3cdb55aeab656870814db89c8')
install=engauge.install

build() {
  cd "$srcdir/${_pkgbase}-$pkgver"
  export OPENJPEG_INCLUDE=/usr/include/openjpeg-2.3 OPENJPEG_LIB=/usr/lib # openjpeg2 2.3.x
  export POPPLER_INCLUDE=/usr/include/poppler/qt5 POPPLER_LIB=/usr/lib
  qmake-qt5 engauge.pro "CONFIG+=jpeg2000 pdf"
  make -j2
  lrelease engauge.pro
  cd help/
  ./build
}

package_engauge() {
  pkgdesc="Extracts data points from images of graphs"
  depends=('qt5-tools' 'fftw' 'log4cpp' 'libpng' 'libjpeg-turbo' 'openjpeg2' 'poppler-qt5')

  cd "$srcdir/${_pkgbase}-$pkgver"
  install -Dm755 ../$pkgbase.sh "$pkgdir/usr/bin/$pkgbase"
  install -Dm755 bin/engauge "$pkgdir/usr/share/$_pkgbase/engauge"
  # translations
  install -dm755 "$pkgdir/usr/share/$_pkgbase/translations/"
  cp translations/*.qm "$pkgdir/usr/share/$_pkgbase/translations/"
  # help
  install -Dm644 bin/documentation/engauge.qhc \
    "$pkgdir/usr/share/$_pkgbase/documentation/engauge.qhc"
  install -Dm644 bin/documentation/engauge.qch \
    "$pkgdir/usr/share/$_pkgbase/documentation/engauge.qch"
  # icon
  install -Dm644 src/img/$_pkgbase.svg \
    "$pkgdir/usr/share/icons/$_pkgbase.svg"
  # desktop
  install -Dm644 $srcdir/$pkgbase.desktop \
	  "$pkgdir/usr/share/applications/$pkgbase.desktop"
}

package_engauge-samples() {
  pkgdesc="sample image files for engauge copied into the doc subdirectory"
  arch=('any')

  cd "$srcdir/${_pkgbase}-$pkgver"
  install -d "$pkgdir/usr/share/doc/$_pkgbase"
  cp -r samples "$pkgdir/usr/share/doc/$_pkgbase"
}
