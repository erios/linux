At the same time, of course, Marxism arose -- Rosa Luxembourg, Leninism, anarchism -- and art became political.
		-- Douglas Sirk
%
The great appear great because we are on our knees: Let us rise.
		-- James Larkin
%
Victory is for them, not for us. We have not made profit out of our country's misfortune. Victory does not bring us luck.
		-- Ba Jin
%
Lock up your libraries if you like; but there is no gate, no lock, no bolt that you can set upon the freedom of my mind.
		-- Virginia Woolf
%
The more corrupt the state, the more numerous the laws.
		-- Tacitus
%
In our so very civilized society it is necessary for me to live the life of a savage. I must be free, even of governments.
		-- Gustave Courbet
%
It is dangerous to be right when the government is wrong.
		-- Voltaire
%
I desire neither to rule nor to be ruled.
		-- Otanes
%
Whoever denies authority and fights against it is an anarchist.
		-- Sebastien Faure
%
Anarchy is order; government is civil war.
		-- Anselme Bellagarrigue
%
Up to now you have believed in the existence of tyrants. Well, you were mistaken. There are only slaves. Where none obeys, none commands.
		-- Anselme Bellagarrigue
%
If you wish to remain men, do not be soldiers; if you cannot stand humiliations do not don the uniform. If, however, you have already committed the imprudence of clothing yourselves therewith, and some day you find yourselves in the situation of being unable to control your indignation, then neither insult nor strike your superiors; LET DAYLIGHT THROUGH THEM! You will pay no more for it.
		-- Jean Grave
%
Power tends to corrupt and absolute power corrupts absolutely.
		-- Lord Acton
%
No nation keeps its word. A nation is a big, blind worm, following what? Fate perhaps. A nation has no honour, it has no word to keep.
		-- Carl Jung
%
Women have no government. Men have organized a government, and they maintain it to the utter exclusion of women.
		-- Victoria Woodhull
%
Never do anything against conscience even if the state demands it.
		-- Albert Einstein
%
An unjust law is no law at all.
		-- Augustine of Hippo
%
From each according to his ability, to each according to his need.
		-- Louis Blanc
%
The world does not need governing; in fact it should not be governed.
		-- Zhuangzi
%
Order results spontaneously when things are let alone.
		-- Zhuangzi
%
Write on my gravestone: "Infidel, Traitor" -- infidel to every church that compromises with wrong; traitor to every government that oppresses the people.
		-- Wendell Phillips
%
I am not a political man and I have no political convictions. I am an individual and a believer in liberty. That is all the politics I have.
		-- Charlie Chaplin
%
I hate governments and rules and fetters. Can't stand caged animals. People must be free.
		-- Charlie Chaplin
%
Anarchism is an ethic, it's a way of behaving.
		-- Henri Cartier-Bresson
%
In a world that is buckling under the weight of profit making, that is overrun by the destructive sirens of Techno-science and the power hunger of globalisation -- that new brand of slavery -- beyond all that, friendship exists, love exists.
		-- Henri Cartier-Bresson
%
Free election of masters does not abolish the masters or the slaves.
		-- Herbert Marcuse
%
Contemporary industrial society is now characterised more than ever by the need for stupefying work where it is no longer a real necessity.
		-- Herbert Marcuse
%
A philosopher is dead when he is no longer read. Some, then, know the strange fortune of death while still alive.
		-- Michel Onfray
%
Who cares about the victim if the gesture is beautiful?
		-- Laurent Tailhade
%
Can one live without working? We can only live without working.
		-- Raoul Vaneigem
%
Who wants a world in which the guarantee that we shall not die of starvation entails the risk of dying of boredom?
		-- Raoul Vaneigem
%
There is no such thing as a good usage or a bad usage of the freedom of speech, only an insufficient usage.
		-- Raoul Vaneigem
%
Faith can move mountains but let them happily fall down on the heads of other people.
		-- Boris Vian
%
Remove the conditional and you destroy God.
		-- Boris Vian
%
If you want a ruler, you are a slave. I'm surprised how many people are comfortable publicly identifying themselves as "slaves."
		-- Jeff Berwick
%
Democracy has turned out to be not majority rule but rule by well-organized and well-connected minority groups who steal from the majority.
		-- Lew Rockwell
%
Pledge allegiance to your principles, your family, your faith, but don't be foolish enough to pledge allegiance to a gang of thieves.
		-- Lew Rockwell
%
Our comrades had not read Marx and were scarcely familiar with all of Proudhon's theories, but common sense was their guide.
		-- Gaston Leval
%
