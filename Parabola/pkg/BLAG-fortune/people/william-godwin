Perfectibility is one of the most unequivocal characteristics of the human species.
		-- William Godwin
%
If there be such a thing as truth, it must infallibly be struck out by the collision of mind with mind.
		-- William Godwin
%
Whenever government assumes to deliver us from the trouble of thinking for ourselves, the only consequences it produces are those of torpor and imbecility.
		-- William Godwin
%
The proper method for hastening the decay of error is not by brute force, or by regulation which is one of the classes of force, to endeavour to reduce men to intellectual uniformity; but on the contrary by teaching every man to think for himself.
		-- William Godwin
%
The true object of education is not to render the pupil the mere copy of his preceptor, it is rather to be rejoiced in, than lamented, that various reading should lead him into new trains of thinking.
		-- William Godwin
%
