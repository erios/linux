Theft is the restitution, the regaining of possession. Instead of being cloistered in a factory, like in a penal colony; instead of begging for what I had a right to, I preferred to rebel and fight my enemy face to face by making war on the rich, by attacking their goods.
		-- Marius Jacob
%
Very young, the virus of justice was inoculated to me and it earned me many a trouble.
		-- Marius Jacob
%
In order to destroy an effect you must first destroy the cause. If there is theft it is only because there is abundance on one hand and famine on the other; because everything only belongs to some. The struggle will only disappear when men will put their joys and suffering in common, their labors and their riches, when all will belong to everyone.
		-- Marius Jacob
%
Those who produce everything have nothing, and those who produce nothing have everything. Such a state of affairs can only produce antagonism between the laboring class and the owning, i.e., do-nothing, class. The fight breaks out and hatred delivers its blows.
		-- Marius Jacob
%
