We anarchists do not want to emancipate the people; we want the people to emancipate themselves.
		-- Errico Malatesta
%
By anarchist spirit I mean that deeply human sentiment, which aims at the good of all, freedom and justice for all, solidarity and love among the people; which is not an exclusive characteristic only of self-declared anarchists, but inspires all people who have a generous heart and an open mind.
		-- Errico Malatesta
%
If you say that you reject violence when it exceeds the limits imposed by the needs of defense, they accuse you of pacifism, without understanding that violence is the whole essence of authoritarianism, just as the repudiation of violence is the whole essence of anarchism.
		-- Errico Malatesta
%
What matters most is that people, all men, lose their sheepish instincts and habits that the millennial slavery inspired them, and they learn to think and act freely.
		-- Errico Malatesta
%
Others invent more or less complicated system of mutuality. But in the long run it is always the searching for a more secure guarantee of freedom which is the common factor among anarchists, and which divides them into different schools.
		-- Errico Malatesta
%
Individualist anarchism and communist anarchism are the same, or nearly so, in terms of moral motivations and ultimate goals.
		-- Errico Malatesta
%
The "government of all the people", if we have to have government, can at best be only the government of the majority.
		-- Errico Malatesta
%
Government means the right to make the law and to impose it on everyone by force: without a police force there is no government.
		-- Errico Malatesta
%
We are neither for a majority nor for a minority government; neither for democracy not for dictatorship. We are for the abolition of the gendarme. We are for the freedom of all and for free agreement, which will be there for all when no one has the means to force others, and all are involved in the good running of society. We are for anarchy.
		-- Errico Malatesta
%
The subject is not whether we accomplish anarchism today, tomorrow, or within ten centuries, but that we walk towards Anarchism today, tomorrow, and always.
		-- Errico Malatesta
%
Anarchism was born in a moral revolt against social injustice.
		-- Errico Malatesta
%
Anarchy is not perfection, it is not the absolute ideal which like the horizon recedes as fast as we approach it; but it is the way open to all progress and all improvements for the benefit of everybody.
		-- Errico Malatesta
%
If in order to win it were necessary to erect the gallows in the public square, then I would prefer to lose.
		-- Errico Malatesta
%
For us, everything that seeks to destroy economic and political oppression, all that which serves to raise the moral and intellectual level of human beings, to give them the consciousness of their rights and of their forces and to persuade them to do their business by themselves, all that provokes hatred against oppression and love between people, brings us closer to our aim.
		-- Errico Malatesta
%
Obviously, the revolution will produce many misfortunes, many sufferings; but if it produced one hundred times more of them, it would still be a blessing relative to what one endures today.
		-- Errico Malatesta
%
It is from the love of humanity that we are revolutionaries: it is not our fault if history has forced on us this distressing necessity.
		-- Errico Malatesta
%
Hate does not produce love; we will not renew the world by hate.
		-- Errico Malatesta
%
The realisation of the usefulness of cooperation, which should have led to the triumph of solidarity in all human relations, instead gave rise to private property and government, that is to the exploitation of the labor of the whole community by a privileged minority.
		-- Errico Malatesta
%
If today we fall without compromising, we can be sure of victory tomorrow.
		-- Errico Malatesta
%
The real sense of gradualism remains the same: everything in nature and in life changes by degrees, and this is no less true of anarchy. It can only come about little by little. 
		-- Errico Malatesta
%
The solutions to each problem must not only be the most economically viable ones but must respond to the imperatives of justice and liberty and be those most likely to keep open the way to future improvements. If necessary, justice, liberty and solidarity must take priority over economic benefit.
		-- Errico Malatesta
%
There is no need to think in terms of destroying everything in the belief that things will look after themselves.
		-- Errico Malatesta
%
It is the task of all comrades to think, study and prepare -- and to do so with all speed and thoroughly because the times are "dynamic" and we must be ready for what might happen.
		-- Errico Malatesta
%
We must fight authority and privilege, while taking advantage from the benefits that civilisation has conferred. We must not destroy anything that satisfies human need however badly -- until we have something better to put in its place.
		-- Errico Malatesta
%
The task of the conscious minority is to profit from every situation to change the environment in a way that will make possible the education of the whole people.
		-- Errico Malatesta
%
The anarchist ideals are far from being in contradiction, as the "scientific socialists" claim, to the laws of evolution as proved by science; they are a conception which fits these laws perfectly; they are the experimental system brought from the field of research to that of social realisation.
		-- Errico Malatesta
%
If you consider these worthy electors as unable to look after their own interests themselves, how is it that they know how to choose for themselves the shepherds who must guide them? And how will they be able to solve this problem of social alchemy, of producing the election of a genius from the votes of a mass of fools?
		-- Errico Malatesta
%
What we want, therefore, is the complete destruction of the domination and exploitation of man by man; we want men united as brothers by a conscious and desired solidarity, all cooperating voluntarily for the well-being of all; we want society to be constituted for the purpose of supplying everybody with the means for achieving the maximum well-being, the maximum possible moral and spiritual development; we want bread, freedom, love, and science -- for everybody.
		-- Errico Malatesta
%
