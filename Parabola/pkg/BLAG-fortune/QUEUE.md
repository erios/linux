Queue - a list of people to be added
====================================
Note that this list is subject to change. If you know an anarchist you want added to this list, or someone you see who does not belong here, please make an issue for it. I want a wide range of anarchists, including those who disagree with each other and are in direct conflict with each other.

Because the French anarchist fortune has a disproportionate number of French thinkers and artists, I am trying to balance the ratio with Spanish, Italian, American, English, German, Eastern European, and Russian anarchists. I am also going to look for anarchistic thinking in East Asia, the Middle East, and Africa.

The miscellaneous file allows for non-anarchists who had some ideas which are in the spirit of anarchism, for example Bertrand Russell and, er, J. R. R. Tolkien. However, explicit anarchists get priority.

In progress:
------------
- None

Next release:
-------------
- Banksy
- Malcolm X
- George Carlin
- Bob Black
- Nestor Makhno
- Noam Chomsky
- Voltairine de Cleyre
- Lysander Spooner
- Haymarket anarchists
- Jo Labadie
- Moxie Marlinspike
- Kevin Carson

Other well-known anarchists:
----------------------------
- David Graeber
- Peter Arshinov
- Nikolai Berdyaev
- Saul Newman
- Maria Lacerda de Moura
- Francisco Ferrer
- George Woodcock
- Luigi Galleani
- Lucia Sanchez Saornil
- Oscar Wilde
- Hakim Bey
- Elbert Hubbard
- Maria Nikiforova
- L. Susan Brown
- Voline
- Dave Andrews
- Henry Appleton
- Adolph Fischer
- Buenaventura Durruti
- Emile Armand
- Herbert Read
- Ricardo Flores Magon
- Ferdinand Domela Nieuwenhuis
- Yakoub Islam
- Ivan Illich
- Shawn P. Wilbur
- Zaher Baher
- Victor Serge

Other anarchist resources:
--------------------------
- libcom.org
- CrimethInc.
- Invisible Committee
- C4SS

To add to a misc. file:
-----------------------
- Zeno of Citium
- Laozi
- Diggers pamphlet
- Famous Quakers
- Rabindranath Tagore
- J. R. R. Tolkien
- Mohandas Gandhi
- gmilcomic
